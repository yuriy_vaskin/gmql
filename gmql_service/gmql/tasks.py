from __future__ import absolute_import
from datetime import datetime, timedelta
from billiard import SoftTimeLimitExceeded
from gmql.utils.GMQLArchive import GMQLArchive, GMQLArchiveCSVWriter, GMQLArchiveBEDWriter, GMQLArchiveIGBWriter
from gmql.utils.GenomeSpace import GenomeSpaceWriter
import os

from celery import shared_task
from celery.utils.log import get_task_logger
from django.conf import settings

from gmql.models import Task, GMQLFile, Query, TaskHistoryRecord
from gmql.utils.gmql_integration import GMQLRequester

logger = get_task_logger(__name__)


@shared_task
def process_task(gmql_task, files, requester=None):
    try:
        logger.info('Task is processing "%s" ' % gmql_task)
        ds_files = {}
        for par_name in files.keys():
            file_list = files.getlist(par_name)
            for u_file in file_list:
                logger.info('File uploading "%s" ' % u_file)
                gmql_task.set_status_msg(Task.UPLOADING_FILE % u_file)
                try:
                    gmql_file = GMQLFile(task=gmql_task, file_type=GMQLFile.UPLOAD)
                    gmql_file.save()
                    gmql_file.file.save(str(u_file.name), u_file)
                    gmql_file.write_metadata_file()
                    cleaned_name = par_name.partition('-')[0]
                    if cleaned_name not in ds_files.keys():
                        ds_files[cleaned_name] = []
                    ds_files[cleaned_name].append(gmql_file)

                except Exception as exc:
                    gmql_task.last_error = ("Cannot upload file: %s. Internal error: %s" % (u_file, exc))
                    logger.info('GMQL error %s' % gmql_task.last_error)
                    logger.info('GMQL error msg %s' % exc)
                    logger.exception("File uploader")
                    raise exc

        if not requester:
            requester = GMQLRequester()
        try:
            logger.info("Uploading files for GMQL")

            for par_name in ds_files.keys():
                file_paths = [gmql_file.file.path for gmql_file in ds_files[par_name]]
                file_paths = file_paths + [gmql_file.get_metadata_filepath() for gmql_file in ds_files[par_name]]
                logger.info("Uploading files for GMQL %s" % file_paths)
                gmql_task.set_status_msg(Task.SENDING_FILE % par_name)
                requester.upload_dataset(file_paths, par_name + "_" + gmql_task.get_name())
            parameters = gmql_task.get_parameters()
            for par in parameters.keys():
                if par in ds_files.keys():
                    parameters[par] = par + "_" + gmql_task.get_name()
            script = gmql_task.get_script_with_parameters(parameters)
            logger.info('Launching GMQL query %s' % script)
            gmql_task.set_status_msg(Task.LAUNCHING)
            requester.run_query(script, gmql_task.get_name())

            logger.info('Executing GMQL query %s' % script)
            gmql_task.set_status_msg(Task.EXECUTING)
            requester.wait_finished()
            logger.info('Query is finished, downloading the result %s' % script)
            gmql_task.set_status_msg(Task.GETTING_RESULT)
            outpath = os.path.join(settings.MEDIA_ROOT, gmql_task.get_output_dataset_folder())
            downloaded_paths = requester.download_result(outpath)


            for f in downloaded_paths:
                gmql_file = GMQLFile(task=gmql_task, file_type=GMQLFile.DOWNLOAD)
                gmql_file.file.name = f
                gmql_file.save()

                gmql_task.set_status_msg(Task.CHARTS)

                processed = process_file(f, outpath, gmql_task.id, gmql_task.query.query)
                for (t, label, f_path) in processed:
                    gmql_file = GMQLFile(task=gmql_task, file_type=t, label=label)
                    gmql_file.file.name = f_path
                    gmql_file.save()

        except Exception as exc:
            gmql_task.last_error = (
                "Cannot execute GMQL script: %s. Internal error: %s" % (gmql_task.get_script_with_parameters(), exc))
            logger.info('GMQL error %s' % gmql_task.last_error)
            logger.info('GMQL error msg %s' % exc)
            logger.exception("GMQL")
            requester.clear()
            raise exc

        logger.info('The result is downloaded %s' % script)
        gmql_task.status = Task.SUCCESS
        gmql_task.set_status_msg(Task.FINISH)
        requester.clear()

    except SoftTimeLimitExceeded as exc:
        gmql_task.last_error = ("Task %s exceeded the execution time limit. Please, try running it on a smaller input or contact admins of the site " % gmql_task)
        gmql_task.status = Task.ERROR
        requester.clear()

    except Exception as exc:
        logger.info('GMQL error %s' % gmql_task.last_error)
        logger.info('GMQL error msg %s' % exc)
        gmql_task.status = Task.ERROR

    logger.info('Saving the task status')
    query = Query.objects.get(pk=gmql_task.query_id)
    query.increase_used()
    gmql_task.save()
    return 'Task is processing "%s" ' % gmql_task

@shared_task
def clean_task():
    tasks_all = Task.objects.all()
    date_now = datetime.now()

    tasks_to_delete = [
        t for t in tasks_all
        if t.date > date_now.date() or t.date + timedelta(days=settings.MAX_TASK_STORAGE_TIME_DAYS) < date_now.date()
    ]

    for task in tasks_to_delete:
        history = TaskHistoryRecord(user=task.user)
        history.set_msg(task)
        history.save()

        task.delete()


def process_file(file, outpath, task_id, query):
    with GMQLArchive(path=file) as archive:
            archive.register_observer(GMQLArchiveCSVWriter(outpath))
            archive.register_observer(GMQLArchiveBEDWriter(outpath))
            archive.register_observer(GMQLArchiveIGBWriter(outpath))
            archive.register_observer(GenomeSpaceWriter(outpath, task_id, query))

            archive.process()

            return archive.outputs()
