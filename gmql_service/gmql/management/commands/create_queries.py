from django.conf import settings
from gmql.models import Query, Parameter, test_file_path
from django.core.management.base import BaseCommand


##QUERY LIST

##1
#Name: Sort files
#Description: Sorts an input file(s) by chromosome and by start position
#s=SELECT(*) {file}; MATERIALIZE s;

##2
#Name: Intersect regions
#Description: Outputs overlapping regions found in ALL positions of input files.
#I1=SELECT(*) {file}; intersect=COVER(ALL,ALL) I1; MATERIALIZE intersect;

##3
#Name: Annotate regions
#Description: Annotates input file(s) with the number of observed overlaps from other files. This way one can check how one feature coincides with other features
#A1=SELECT(*) {file1}; A2=SELECT(*) {file2}; annotate=MAP(count AS COUNT) A1 A2; MATERIALIZE annotate;

##4
#Name: Get ENCODE ChIP-Seq files by cell line and antibody (hg19)
#Description: Returns ENCODE broadPeak samples of ChIP-Seq experiments of given cell line and antibody
#cell=SELECT( dataType == 'ChipSeq' AND cell == '{cell_name}' AND antibody == '{antibody_name}' ) ENCODE_BROADPEAK_HG19; MATERIALIZE cell;
#Example cell: K562
#Example antibody: CBX2

##5
#Name: Intersect ENCODE experiments with regions (hg19)
#Description: Returns ENCODE broadPeak samples of a given cell line that intersect regions in input file(s)
#custom=SELECT(*) {file}; cell=SELECT( dataType == 'ChipSeq' AND cell == '{cell_name}' ) ENCODE_BROADPEAK_HG19; joinResult=JOIN(distance < 0, right) custom cell; MATERIALIZE joinResult;
#Example TF: K562

##6
#Name: Get ENCODE ChIP-Seq experiment by transcription factor (hg19)
#Description: Returns ENCODE broadPeak samples of ChIP-Seq experiments of a given transcription factor
#ab=SELECT( dataType == 'ChipSeq' AND antibody_target == '{antibody_name}' ) ENCODE_BROADPEAK_HG19; MATERIALIZE ab;
#Example TF: REST

##7
#Name: Intersect ENCODE experiments by transcription factors with regions (hg19)
#Description: Intersect ENCODE ChIP-Seq experiment of a given antibody with regions in a file (hg19)
#custom=SELECT(*) {file}; ab=SELECT( dataType == 'ChipSeq' AND antibody_target == '{antibody_name}' ) ENCODE_BROADPEAK_HG19; joinResult=JOIN(distance < 0, right) custom ab; MATERIALIZE joinResult;
#Example TF: REST

##9
#Name: Map features with score aggregation
#Description: Maps overlapping features of m2 onto m1 applying aggregation of the score of those features.
#M1=SELECT(*) {m1}; M2=SELECT(*) {m2}; mapResult=MAP(sum AS SUM(score)) M1 M2; MATERIALIZE mapResult;


##10
#Name: Promoter flanking intervals
#Description: For each input interval creates a promoter flanking interval l bases before the region start.
#F1=SELECT(*) {file}; FW1=PROJECT(*; start = start-{l}, stop=start) F1; MATERIALIZE FW1;
#Example l: 5



##11
#Name: Slop intervals
#Description: Increases the size of each region by a defined number of bases.
#S1=SELECT(*) {file}; SW1=PROJECT(*; start = start-{l}, stop=stop+{r}) S1; MATERIALIZE SW1;
#Example l: 5
#Example r: 5



##12
#Name: Search intersection with elongated window
#Description: Adds given number of bases to the first intervals and then intersects resulting set with the second intervals.
#I1=SELECT(*) {file1}; I2=SELECT(*) {file2}; IW1=PROJECT(*; start = start-{w}, stop=stop+{w}) I1; U=UNION IW1 I2; window=COVER(ALL,ALL) U; MATERIALIZE window;
#Example l: 5
#Example r: 5
from gmql.models import Query, Parameter


class Command(BaseCommand):
    args = ''
    help = 'Create default queries. If an object already there, it is not changed'

    def _is_query_exists(self, name, query):
        return Query.objects.filter(name=name).exists() or Query.objects.filter(query=query).exists()

    def _add_query(self, query_dict, parameters):
        if not self._is_query_exists(query_dict["name"], query_dict["query"]):
            q = Query(**query_dict)
            q.save()
            for p_dict in parameters:
                p = Parameter(**p_dict)
                p.query = q
                p.save()
        else:
            print "Query %s exists" % query_dict["name"]

    def handle(self, *args, **options):
        self._create_queries()

    def _create_queries(self):
         ##0
#        q_dict = {"name": "Sort files TEST",
#                  "description": "Sorts an input file(s) by chromosome and by start position.",
#                  "query" : "sor=SELECT(*) {file}; MATERIALIZE sor;",
#        }
#        parameters = [
#             {"name": "file",
#             "description": "Input file(s)",
#             "parameter_type": Parameter.FILE,
#             "default_value": test_file_path("input/1.bed"),
#            },
#        ]
#        self._add_query(q_dict, parameters)


        ##2
        q_dict = {"name": "Intersect regions",
                  "description": "Outputs overlapping regions found in ALL positions of input files.",
                  "query" : "I1=SELECT(*) {file}; intersect=COVER(ALL,ALL) I1; MATERIALIZE intersect;",
        }
        parameters = [
             {"name": "file",
             "description": "Input file(s)",
             "parameter_type": Parameter.FILE,
             "default_value": ",".join([test_file_path("input/i1.bed"), test_file_path("input/i2.bed")]),
            },
        ]
        self._add_query(q_dict, parameters)


        ##3
        q_dict = {"name": "Annotate regions",
                  "description": "Annotates input file(s) with the number of observed overlaps from other files. This way one can check how one feature coincides with other features.",
                  "query" : "A1=SELECT(*) {file1}; A2=SELECT(*) {file2}; annotate=MAP(count AS COUNT) A1 A2; MATERIALIZE annotate;",
        }
        parameters = [
            {"name": "file1",
             "description": "Reference file(s)",
             "parameter_type": Parameter.FILE,
             "default_value": test_file_path("input/a1.bed"),
            },

            {"name": "file2",
             "description": "File(s) to map",
             "parameter_type": Parameter.FILE,
             "default_value": ",".join([test_file_path("input/a2.bed"), test_file_path("input/a3.bed")]),
            },
        ]
        self._add_query(q_dict, parameters)

        ##4
        q_dict = {"name": "Get ENCODE ChIP-Seq files by cell line and antibody (hg19)",
                  "description": "Returns ENCODE broadPeak samples of ChIP-Seq experiments of given cell line and antibody",
                  "query" : "encode_broadpeaks=SELECT( dataType == 'ChipSeq' AND cell == '{cell_name}' AND antibody == '{antibody_name}' ) ENCODE_BROADPEAK_HG19; MATERIALIZE encode_broadpeaks;",
        }
        parameters = [
            {"name": "cell_name",
             "description": "Example cell: K562",
             "parameter_type": Parameter.TEXT,
             "default_value": "K562",

            },

            {"name": "antibody_name",
             "description": "Example antibody: CBX2",
             "parameter_type": Parameter.TEXT,
             "default_value": "CBX2",
            },
        ]
        self._add_query(q_dict, parameters)


        ##5
        q_dict = {"name": "Intersect ENCODE experiments with regions (hg19)",
                  "description": "Returns ENCODE broadPeak samples of a given cell line that intersect regions in input file(s)",
                  "query" : "custom=SELECT(*) {file}; encode_broadpeaks=SELECT( dataType == 'ChipSeq' AND cell == '{cell_name}' ) ENCODE_BROADPEAK_HG19; joinResult=JOIN(distance < 0, right) custom encode_broadpeaks; MATERIALIZE joinResult;",
        }
        parameters = [
            {"name": "cell_name",
             "description": "Example TF: K562",
             "parameter_type": Parameter.TEXT,
             "default_value": "K562",
            },
             {"name": "file",
             "description": "Input file(s)",
             "parameter_type": Parameter.FILE,
             "default_value": test_file_path("input/encode_intersect.bed"),
            },
        ]
        self._add_query(q_dict, parameters)

        ##6
        q_dict = {"name": "Get ENCODE ChIP-Seq experiment by transcription factor (hg19)",
                  "description": "Returns ENCODE broadPeak samples of ChIP-Seq experiments of a given transcription factor",
                  "query" : "ab=SELECT( dataType == 'ChipSeq' AND antibody_target == '{antibody_name}' ) ENCODE_BROADPEAK_HG19; MATERIALIZE ab;",
        }
        parameters = [
            {"name": "antibody_name",
             "description": "Example TF: REST",
             "parameter_type": Parameter.TEXT,
             "default_value": "REST",
            },
        ]
        self._add_query(q_dict, parameters)


        ##7
        q_dict = {"name": "Intersect ENCODE experiments by transcription factors with regions (hg19)",
                  "description": "Intersect ENCODE ChIP-Seq experiment of a given antibody with regions in a file (hg19)",
                  "query" : "custom=SELECT(*) {file}; ab=SELECT( dataType == 'ChipSeq' AND antibody_target == '{antibody_name}' ) ENCODE_BROADPEAK_HG19; joinResult=JOIN(distance < 0, right) custom ab; MATERIALIZE joinResult;",
        }
        parameters = [
            {"name": "file",
             "description": "Input file(s)",
             "parameter_type": Parameter.FILE,
             "default_value": test_file_path("input/encode_intersect.bed"),
            },

            {"name": "antibody_name",
             "description": "Example TF: REST",
             "parameter_type": Parameter.TEXT,
             "default_value": "REST",
            },
        ]
        self._add_query(q_dict, parameters)


        ##9
        q_dict = {"name": "Map features with score aggregation",
                  "description": "Maps overlapping features of m2 onto m1 applying aggregation of the score of those features.",
                  "query" : "M1=SELECT(*) {m1}; M2=SELECT(*) {m2}; mapResult=MAP(sum AS SUM(score)) M1 M2; MATERIALIZE mapResult;",
        }
        parameters = [
            {"name": "m1",
             "description": "Reference file(s)",
             "parameter_type": Parameter.FILE,
             "default_value": test_file_path("input/map1.bed"),
            },

            {"name": "m2",
             "description": "File(s) to map",
             "parameter_type": Parameter.FILE,
             "default_value": test_file_path("input/map2.bed"),
            },
        ]
        self._add_query(q_dict, parameters)


        ##10
        q_dict = {"name": "Promoter flanking intervals",
                  "description": "For each input interval creates a promoter flanking interval l bases before the region start.",
                  "query" : "F1=SELECT(*) {file}; FW1=PROJECT(*; start = start-{l}, stop=start) F1; MATERIALIZE FW1;",
        }
        parameters = [
            {"name": "file",
             "description": "Input file(s)",
             "parameter_type": Parameter.FILE,
             "default_value": test_file_path("input/f1.bed"),
            },

            {"name": "l",
             "description": "Example l: 5",
             "parameter_type": Parameter.TEXT,
             "default_value": "5",
            },
        ]
        self._add_query(q_dict, parameters)



        ##11
        q_dict = {"name": "Slop intervals",
                  "description": "Increases the size of each region by a defined number of bases.",
                  "query" : "S1=SELECT(*) {file}; SW1=PROJECT(*; start = start-{l}, stop=stop+{r}) S1; MATERIALIZE SW1;",
        }
        parameters = [
            {"name": "file",
             "description": "Input file(s)",
             "parameter_type": Parameter.FILE,
             "default_value": test_file_path("input/s1.bed"),
            },

            {"name": "l",
             "description": "Example l: 5",
             "parameter_type": Parameter.TEXT,
             "default_value": "5",
            },
            {"name": "r",
             "description": "Example r: 5",
             "parameter_type": Parameter.TEXT,
             "default_value": "5",
            },
        ]
        self._add_query(q_dict, parameters)



        ##12
        q_dict = {"name": "Search intersection with elongated window",
                  "description": "Adds given number of bases to the first intervals and then intersects resulting set with the second intervals.",
                  "query" : "I1=SELECT(*) {file1}; I2=SELECT(*) {file2}; IW1=PROJECT(*; start = start-{w}, stop=stop+{w}) I1; U=UNION IW1 I2; window=COVER(ALL,ALL) U; MATERIALIZE window;",
        }
        parameters = [
            {"name": "file1",
             "description": "Elongated file(s)",
             "parameter_type": Parameter.FILE,
             "default_value": test_file_path("input/w1.bed"),
            },

            {"name": "file2",
             "description": "File(s) to intersect with",
             "parameter_type": Parameter.FILE,
             "default_value": test_file_path("input/w2.bed"),
            },

            {"name": "w",
             "description": "Example w: 5000",
             "parameter_type": Parameter.TEXT,
             "default_value": "5000",
            },
        ]
        self._add_query(q_dict, parameters)
