from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings

from gmql import views


urlpatterns = patterns('',
                       url(r'^$', views.index, name='index'),
                       url(r'^queries/$', views.queries, name='queries'),
                       url(r'^queries/(?P<filter>\w+)/$', views.queries, name='queries'),
                       url(r'^custom/$', views.custom, name='custom'),
                       url(r'^about/$', views.about, name='about'),
                       url(r'^documentation/$', views.documentation, name='documentation'),
                       url(r'^licence/$', views.licence, name='licence'),
                       url(r'^metadata/(?P<meta_name>\w+)/$', views.metadata, name='metadata'),
                       url(r'^documentation/full_doc$', views.full_doc, name='full_doc'),
                       url(r'^query/(?P<query_id>\d+)/$', views.query, name='query'),
                       url(r'^query/(?P<query_id>\d+)/delete/$', views.delete_query, name='delete_query'),
                       url(r'^task/(?P<pk>\d+)/$', views.TaskView.as_view(), name='task'),
                       url(r'^task/(?P<pk>\d+)/download/(?P<file_id>\d+)/$', views.file_download, name='file_download'),

)

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
                            (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
                                'document_root': settings.MEDIA_ROOT}))
