import json
from codemirror import CodeMirrorTextarea
from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils.datastructures import MultiValueDict
from gmql.utils.ChartCalculators import ChartRegionDistribution, ChartChromosomeDistribution, TableMetadataTerms, \
    ChartMetadataTermsDistribution, ChartHeatmapMapOperation
from gmql.utils.SelectionHint import SelectStatement
from gmql.utils.SpecialMeta import SpecialMeta, encode_meta
from gmql.utils.TagsRegistry import Tags
import os
import hashlib

from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.forms import formset_factory
import magic
from django.contrib.auth.models import User
from django.db import models
from allauth.account.models import EmailAddress
from allauth.socialaccount.models import SocialAccount

from gmql.utils import gmql_parser
from selectable import registry
from selectable.base import LookupBase
from selectable.forms.widgets import AutoComboboxWidget, AutoComboboxSelectWidget


def test_file_path(path):
    return os.path.join(settings.BASE_DIR, 'gmql/tests/gmql/', path)

class QueryQuerySet(models.QuerySet):
    def globals_and_customs(self, user=None):
        all_objects = self.all()
        if not user:
            custom_ids = []
        elif user.is_superuser:
            custom_ids = [o.id for o in all_objects if not o.is_global]
        else:
            custom_ids = [o.id for o in all_objects if not o.is_global and o.user == user]
        ids = [o.id for o in all_objects if o.is_global] + custom_ids
        return all_objects.filter(id__in=ids)

    def globals(self):
        all_objects = self.all()
        global_ids = [o.id for o in all_objects if o.is_global]
        return all_objects.filter(id__in=global_ids)


    def customs(self, user=None):
        all_objects = self.all()
        if not user:
            custom_ids = []
        elif user.is_superuser:
            custom_ids = [o.id for o in all_objects if not o.is_global]
        else:
            custom_ids = [o.id for o in all_objects if not o.is_global and o.user == user]
        return all_objects.filter(id__in=custom_ids)


    def tags(self, tags=None):
        all_objects = self.all()
        if not tags:
            custom_ids = [o.id for o in all_objects]
        else:
            if not isinstance(tags, list):
                tags = [tags]
            custom_ids = [o.id for o in all_objects if o.has_tags(tags)]
        return all_objects.filter(id__in=custom_ids)


    def search(self, search_terms=None):
        all_objects = self.all()
        if not search_terms:
            custom_ids = [o.id for o in all_objects]
        else:
            prepared_terms = [search_term.strip() for search_term in search_terms.split(' ') if search_term.strip()]
            custom_ids = [o.id for o in all_objects if o.search(prepared_terms)]
        return all_objects.filter(id__in=custom_ids)

class Query(models.Model):
    objects = QueryQuerySet.as_manager()

    user = models.ForeignKey(User, null=True)
    name = models.CharField(max_length=400)
    description = models.CharField(max_length=2000)
    query = models.CharField(max_length=20000)
    details = models.CharField(max_length=20000, null=True, blank=True)

    used = models.IntegerField(default=0, null=False)

    def __unicode__(self):
        return self.name

    def set_example_data(self):
        self.name = 'Put the name of the query here'
        self.description = 'Put the description of the query here'
        self.query = "encode_broadpeaks=SELECT( dataType == 'ChipSeq' AND cell == '{cell}' AND antibody == '{antibody}' ) ENCODE_BROADPEAK_HG19; \n" \
                     "MATERIALIZE encode_broadpeaks;"


    def increase_used(self):
        self.used += 1
        self.save()


    #HACK for visualization
    def is_hg19(self):
        return self.search(['hg19'])


    def has_tags(self, tags):
        if set(Tags.find_all_tags(self.query)) & set(tags):
            return True
        return False


    def search(self, search_terms):
        wanted = (( term.lower() in self.query.lower() or term.lower() in self.name.lower() or term.lower() in self.description.lower()) for term in search_terms)
        return any(wanted)


    @property
    def is_global(self):
        try:
            if not self.user:
                return True
            user = User.objects.get(pk=self.user.pk)
            return self.user.is_superuser
        except User.DoesNotExist:  # catch the DoesNotExist error
            return True


    def has_defaults(self):
        params = Parameter.objects.filter(query=self.pk)
        return all(p.has_defaults() for p in params)

    def has_file_input(self):
        params = Parameter.objects.filter(query=self.pk)
        return any(p.parameter_type == Parameter.FILE for p in params)


    def extract_selection_statements(self, parameters):
        statements = SelectStatement.get_statements(self.query, parameters)
        [s.retrieve_count() for s in statements]
        return statements

    def has_selection(self):
        return len(SelectStatement.get_statements(self.query, {})) != 0


class Parameter(models.Model):
    query = models.ForeignKey(Query)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)

    TEXT = 'Text'
    FILE = 'File'
    PARAMETER_TYPES = (
        (TEXT, 'General text'),
        (FILE, 'Input file'),
    )
    parameter_type = models.CharField(max_length=10, choices=PARAMETER_TYPES, default=TEXT)

    default_value = models.CharField(max_length=100, null=True, blank=True)

    def __unicode__(self):
        return self.name


    def has_defaults(self):
        if self.default_value:
            return True
        return False


    def get_default_value(self):
        if not self.has_defaults():
            return ""
        if self.parameter_type == Parameter.TEXT:
            return self.default_value
        if self.parameter_type == Parameter.FILE:
            return ",".join(self.get_default_file_names())
        return ""

    def get_default_file_names(self):
        if self.has_defaults() and self.parameter_type == Parameter.FILE:
            return [path[len(test_file_path("")):] for path in self.default_value.split(',')]
        return []


class Task(models.Model):
    user = models.ForeignKey(User, null=True)
    date = models.DateField(auto_now_add=True)
    query = models.ForeignKey(Query)
    parameter_values = models.CharField(max_length=400)
    last_error = models.CharField(max_length=400)
    status_message = models.CharField(max_length=1000, null=True, blank=True)

    RUNNING = 'Running'
    ERROR = 'Error'
    SUCCESS = 'Success'
    STATUS_TYPES = (
        (RUNNING, 'Running'),
        (ERROR, 'Error'),
        (SUCCESS, 'Success'),
    )
    status = models.CharField(max_length=10, choices=STATUS_TYPES, default=RUNNING)

    # TODO cancellation
    def __unicode__(self):
        return self.query.name

    def set_parameters(self, parameters_dict):
        self.parameter_values = json.dumps(parameters_dict)

    def get_parameters(self):
        return json.loads(self.parameter_values)

    def get_script_with_parameters(self, parameters=None):
        if parameters is None:
            parameters = self.get_parameters()
        script = gmql_parser.set_gqml_parameters(Query.objects.get(pk=self.query_id).query, parameters)
        return script

    def get_input_dataset_folder(self):
        return os.path.join(str(self.pk), 'input')

    def get_output_dataset_folder(self):
        return os.path.join(str(self.pk), 'output')

    def get_name(self):
        return str(self.pk)

    def get_display_name(self):
        return '%s_%s' % (self.query.name, self.date)


    PREPARED = 'Preparing to run a query'
    UPLOADING_FILE = 'Uploading file %s'
    SENDING_FILE = 'Sending file %s to the GMQL core'
    LAUNCHING = 'Launching GMQL query'
    EXECUTING = 'Executing GMQL query'
    GETTING_RESULT = 'Getting the result of the query'
    CHARTS = 'The result is available for download. Additionally, converting the format and calculating charts'
    FINISH = 'The query is successfully finished'

    def report_status(self):
        if self.status != Task.RUNNING or not self.status_message:
            return None
        return self.status_message

    def set_status_msg(self, msg):
        self.status_message = msg
        self.save()


class TaskHistoryRecord(models.Model):
    user = models.ForeignKey(User, null=True)
    msg = models.CharField(max_length=1000, blank=True, null=True)

    def __unicode__(self):
        return self.msg

    def set_msg(self, task):
        self.msg = "Date: %s; Query: %s; Parameters: %s; Status: %s;" % (task.date, task.query, task.parameter_values, task.status)
        if task.status == Task.ERROR:
            self.msg += " Error: %s;" % task.last_error


def input_file_name(instance, filename):
    return '/'.join([instance.task.get_input_dataset_folder(), str(filename)])


class ChartCalculatorRunner(object):
    calculators = []

    def __init__(self, arch_genome_space, task_id, query):
        self.arch_genome_space = arch_genome_space
        self.task_id = task_id
        self.query = query

    def run(self):
        for archive_name in self.arch_genome_space:
            for calculator in ChartCalculatorRunner.calculators:
                if not calculator.test(self.query):
                    continue
                try:
                    data = calculator.run(self.arch_genome_space[archive_name])
                except Exception as ex:
                    print ex
                    data = ''
                if data:
                    chart = Chart.objects.create(name=calculator.name, archive_name=archive_name, task_id=self.task_id, is_table=calculator.table, has_chromosomes=calculator.has_chromosomes)
                    chart.data = data

    @staticmethod
    def add_calculator(calculator):
        ChartCalculatorRunner.calculators.append(calculator)

    @staticmethod
    def connect_data(name, data, chromosome=None):
        js = ''
        for calculator in ChartCalculatorRunner.calculators:
            if calculator.name == name:
                js = calculator.html(data, chromosome)
                break
        return js



ChartCalculatorRunner.add_calculator(TableMetadataTerms())
ChartCalculatorRunner.add_calculator(ChartRegionDistribution())
ChartCalculatorRunner.add_calculator(ChartChromosomeDistribution())
ChartCalculatorRunner.add_calculator(ChartMetadataTermsDistribution())
ChartCalculatorRunner.add_calculator(ChartHeatmapMapOperation())



class Chart(models.Model):
    task = models.ForeignKey(Task)
    name = models.CharField(max_length=20)
    archive_name = models.CharField(max_length=20)
    data_serialized = models.CharField(max_length=999999999, null=True)
    is_table = models.BooleanField(default=False)
    has_chromosomes = models.BooleanField(default=False)


    def chart_data(self, chromosome=None):
        js = ChartCalculatorRunner.connect_data(self.name, self.data, chromosome=chromosome)
        return js

    @property
    def data(self):
        return json.loads(self.data_serialized)

    @data.setter
    def data(self, data_dict):
        self.data_serialized = json.dumps(data_dict)
        self.save()


    def container_name(self):
        return ('%s-%s-%s-%s' % (self.archive_name, self.name, self.task_id, self.pk)).replace('.', '-')


    def __str__(self):
        return '%s-%s' % (self.archive_name, self.name)



class GMQLFile(models.Model):
    task = models.ForeignKey(Task)
    file = models.FileField(upload_to=input_file_name, null=True)
    meta_file = models.FileField(upload_to=input_file_name, null=True)
    label = models.CharField(max_length=20, default='default', blank=True)

    DOWNLOAD = 'Download'
    UPLOAD = 'Upload'
    BROWSE = 'Browse'
    TYPES = (
        (DOWNLOAD, 'DOWNLOAD'),
        (UPLOAD, 'UPLOAD'),
        (BROWSE, 'BROWSE'),
    )
    file_type = models.CharField(max_length=10, choices=TYPES, default=UPLOAD)

    def __unicode__(self):
        return self.file.name.split('/')[-1]

    def get_metadata_filepath(self):
        return self.file.path + '.meta'


    def write_metadata_file(self):
        with open(self.get_metadata_filepath(), 'w') as meta_file:
            meta_file.write("name\t%s\n" % self.file.name.split('/')[-1])
            self.meta_file.name = self.get_metadata_filepath()
            self.save()


    @property
    def download_path(self):
        # HACK for some reason self.file.path doesn't work in production
        path = str(self.file)
        return GMQLFile.file_download_path(path)

    @staticmethod
    def file_download_path(path):
        if settings.MEDIA_URL in path:
            path = path[path.rfind(settings.MEDIA_URL):]
            path = path[len(settings.MEDIA_URL):]

        return path

    def igb_link(self):
        return 'http://localhost:7085/UnibrowControl?scriptfile=%s' %(settings.SITE_URL + settings.MEDIA_URL + self.download_path)


class LookupItem:
    def __init__(self, category, value):
        self.category = category
        self.value = value

    def __unicode__(self):
        return self.value


class ValueLookup(LookupBase):
    def _create_lookup_items(self, request):
        res = []

        parameter = Parameter.objects.get(pk=request.GET['id'])
        query = Query.objects.get(pk=parameter.query_id)

        #defualts
        if parameter.has_defaults():
            [res.append(LookupItem("Default", d.strip())) for d in parameter.get_default_value().split(',') if d.strip()]

        #guess genome
        sms = SpecialMeta.guess_metas(query.query)

        #data of that term
        for sm in sms:
            if parameter.name.lower() in [k.lower() for k in sm.keys()]:
                [res.append(LookupItem('%s-%s' % (sm.name, parameter.name), d)) for d in sm.values(parameter.name)]

        return res


    def get_query(self, request, term):
        data = self._create_lookup_items(request)
        return filter(lambda x: x.value.lower().startswith(term.lower()), data)


    def get_item_label(self, item):
        if isinstance(item, LookupItem):
            return "%s: %s" % (item.category, item.value)
        return super(ValueLookup, self).get_item_label(item)

registry.registry.register(ValueLookup)



class QuerySearchForm(forms.Form):
    search_value = forms.CharField(max_length=200, required=False, widget=forms.TextInput(attrs={'placeholder': 'Search...'}))

codemirror_widget = CodeMirrorTextarea(mode="gmql", config={ 'fixedGutter': True }, attrs={'cols': 30, 'rows': 5})


class CustomQueryModelForm(forms.ModelForm):
    def clean_query(self):
        data = self.cleaned_data['query']
        parameters = gmql_parser.parse_gqml_parameters(data)
        if len(parameters) <= 0:
            raise forms.ValidationError(
                "The query has no parameters. Please, use {parameter_name} form to extract parameters.")
        return data

    class Meta:
        model = Query
        exclude = ('used', 'user', 'details')
        widgets = {
            'query': codemirror_widget,
        }


class CustomParameterModelForm(forms.ModelForm):
    class Meta:
        model = Parameter
        exclude = ('query',)

    def __init__(self, *args, **kwargs):
        super(CustomParameterModelForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['readonly'] = True


    def clean_default_value(self):
        default_val = self.cleaned_data['default_value']
        type = self.cleaned_data['parameter_type']
        if default_val and type == Parameter.FILE:
            raise forms.ValidationError(
                "A default file cannot be set.")
        return default_val


class SetParameterModelFormBase(forms.ModelForm):
    class Meta:
        model = Parameter
        exclude = ('query', 'parameter_type', 'default_value', )

    def __init__(self, *args, **kwargs):
        super(SetParameterModelFormBase, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['readonly'] = True
        self.fields['description'].widget.attrs['readonly'] = True


    def get_parameters_dict(self, parameters):
        parameters_dict = {}
        if self.is_valid():
            name = self.cleaned_data['name']
            if len([p for p in parameters if p['parameter_type'] == Parameter.FILE and name == p['name']]) != 0:
                value = ','.join([f.name for f in self.cleaned_data['value']])
            else:
                value = self.cleaned_data['value']
            parameters_dict[name] = value
        return parameters_dict



class MultiFileInput(forms.FileInput):
    def render(self, name, value, attrs=None):
        if not attrs: attrs = {}
        attrs['multiple'] = 'multiple'
        return super(MultiFileInput, self).render(name, None, attrs=attrs)

    def value_from_datadict(self, data, files, name):
        if hasattr(files, 'getlist'):
            return files.getlist(name)
        else:
            return [files.get(name)]


class MultiFileField(forms.FileField):
    widget = MultiFileInput
    default_error_messages = {
        'min_num': u"Ensure at least %(min_num)s files are uploaded (received %(num_files)s).",
        'max_num': u"Ensure at most %(max_num)s files are uploaded (received %(num_files)s).",
        'file_size': u"File: %(uploaded_file_name)s, exceeded maximum upload size.",
        'mime': u"File: %(uploaded_file_name)s, has unsupported MIME type %(mime_type)s. Use text/plain only"
    }

    def __init__(self, *args, **kwargs):
        self.min_num = kwargs.pop('min_num', 0)
        self.max_num = kwargs.pop('max_num', None)
        self.maximum_file_size = kwargs.pop('maximum_file_size', None)
        super(MultiFileField, self).__init__(*args, **kwargs)

    def to_python(self, data):
        ret = []
        for item in data:
            ret.append(super(MultiFileField, self).to_python(item))
        return ret

    def validate(self, data):
        super(MultiFileField, self).validate(data)
        num_files = len(data)
        if len(data) and not data[0]:
            num_files = 0
        if num_files < self.min_num:
            raise ValidationError(self.error_messages['min_num'] % {'min_num': self.min_num, 'num_files': num_files})
        elif self.max_num and num_files > self.max_num:
            raise ValidationError(self.error_messages['max_num'] % {'max_num': self.max_num, 'num_files': num_files})
        for uploaded_file in data:
            if uploaded_file.size > self.maximum_file_size:
                raise ValidationError(self.error_messages['file_size'] % {'uploaded_file_name': uploaded_file.name})
        for uploaded_file in data:
            mime = magic.from_buffer(uploaded_file.read(1024), mime=True)
            if mime != "text/plain":
                raise ValidationError(
                    self.error_messages['mime'] % {'uploaded_file_name': uploaded_file.name, 'mime_type': mime})


class SetParameterModelFormFile(SetParameterModelFormBase):
    value = MultiFileField(max_num=settings.MAX_UPLOAD_FILES, min_num=1, maximum_file_size=settings.MAX_UPLOAD_FILE_SIZE)


class SetParameterModelFormText(SetParameterModelFormBase):
    def __init__(self, *args, **kwargs):
        super(SetParameterModelFormText, self).__init__(*args, **kwargs)
        initials = dict(kwargs['initial'])
        if 'default_value' in initials:
            initials.pop('default_value')
        self.fields['value'] = forms.CharField(widget=AutoComboboxWidget(ValueLookup, limit=5, query_params=initials, allow_new=True))




def add_default_files(parameters, files=None):
    if not files: files = MultiValueDict()
    for i in range(0, len(parameters)):
        if parameters[i]['parameter_type'] == Parameter.FILE:
                file_list = []
                for f in parameters[i]['default_value'].split(','):
                    try:
                        upload_file = open(f, 'rb')
                    except IOError:
                        continue
                    file_list.append(SimpleUploadedFile(upload_file.name, upload_file.read()))
                files.setlist('%s-value' % parameters[i]['name'], file_list)
    return files


def is_combo(parameter):
    if parameter['parameter_type'] == Parameter.TEXT and 'default_value' in parameter and parameter['default_value']:
        cleaned = [d.strip() for d in parameter['default_value'].split(',') if d.strip()]
        if len(cleaned) > 1:
            return True
    return False

def generate_forms_list(parameters, data=None, files=None, use_default=False):
    result = []
    if use_default:
        files = add_default_files(parameters, files)

    for i in range(0, len(parameters)):
        if use_default:
            if not is_combo(parameters[i]):
                data.setlist("%s-value" % parameters[i]['name'], [parameters[i]['default_value']])
        form = get_form_by_type(parameters[i]['parameter_type'], parameters[i]['name'], parameters[i], data, files)
        result.append(form)
    return result


def get_form_by_type(t, prefix, initials, data, files):
    if t == Parameter.FILE:
        return SetParameterModelFormFile(data=data, files=files, prefix=prefix, initial=initials)
    return SetParameterModelFormText(data=data, prefix=prefix, initial=initials)


ParametersFormset = formset_factory(CustomParameterModelForm, extra=0)


def image(user):
    fb_uid = SocialAccount.objects.filter(user_id=user.id, provider='facebook')

    if len(fb_uid):
        return "http://graph.facebook.com/{}/picture?width=50&height=50".format(fb_uid[0].uid)

    return "http://www.gravatar.com/avatar/{}?s=50".format(
        hashlib.md5(user.email).hexdigest())

User.image = property(image)
