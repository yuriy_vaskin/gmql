// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: http://codemirror.net/LICENSE

(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})(function(CodeMirror) {
"use strict";

CodeMirror.defineMode('gmql', function() {

  var words = {};
  function define(style, string) {
    var split = string.split(' ');
    for(var i = 0; i < split.length; i++) {
      words[split[i]] = style;
    }
  };

  // Atoms
  define('atom', 'TRUE FALSE');

  // Keywords
  define('keyword', 'SELECT PROJECT PROJECT MAP MAP-STRANDED JOIN ' +
   'JOIN-STRANDED UNION COVER COVER-FLAT SUMMIT SUMMIT-FLAT DIFFERENCE ' +
   'DIFFERENCE-STRANDED MATERIALIZE MERGE OR AND NOT');


  // Commands
  define('builtin', 'ORDER TOP TOPG DESC JACCARD GROUP BY ' +
                'ANY ALL LEFT RIGHT LEFT-DISTINCT RIGHT-DISTINCT ' +
                'PROJECT-LEFT PROJECT-RIGHT PROJECT-LEFT-DISTINCT ' +
                'PROJECT-RIGHT-DISTINCT PLUS MINUS STAR DIV ASSIGN ' +
                'AS EQ NOT-EQ LOWER LOWER-EQ GREATER GREATER-EQ ' +
                'INTERSECTION CONJUNCTION L-PAR R-PAR R-ARROW ' +
                'DISTANCE MIN-DISTANCE FIRST AFTER UPSTREAM-DISTANCE ' +
                'DOWNSTREAM-DISTANCE AGGREGATE START STOP STD MEDIAN CHR ' +
                'STRAND SEMI-COLON COMMA DOT EOF');

  function tokenBase(stream, state) {
    if (stream.eatSpace()) return null;

    var sol = stream.sol();
    var ch = stream.next();

    if (ch === '\\') {
      stream.next();
      return null;
    }
    if (ch === '{') {
      state.tokens.unshift(tokenBrack(ch));
      return tokenize(stream, state);
    }
    if (ch === '\'' || ch === '"' || ch === '`') {
        state.tokens.unshift(tokenString(ch));
        return tokenize(stream, state);
    }
    if (ch === '#') {
      if (sol && stream.eat('!')) {
        stream.skipToEnd();
        return 'meta'; // 'comment'?
      }
      stream.skipToEnd();
      return 'comment';
    }
    if (ch === '+' || ch === '=' || ch === '==' || ch === '-') {
      return 'operator';
    }
    if (/\d/.test(ch)) {
      stream.eatWhile(/\d/);
      if(stream.eol() || !/\w/.test(stream.peek())) {
        return 'number';
      }
    }
    stream.eatWhile(/[\w-]/);
    var cur = stream.current();
    if (stream.peek() === '=' && /\w+/.test(cur)) return 'def';
    return words.hasOwnProperty(cur) ? words[cur] : null;
  }

  function tokenString(quote) {
    return function(stream, state) {
      var next, end = false, escaped = false, defin = false;
      while ((next = stream.next()) != null) {
        if (next === quote && !escaped) {
          end = true;
          break;
        }
        if (next === '{' || next === '}'){
          defin = true;
        }
        if (next === '$' && !escaped && quote !== '\'') {
          escaped = true;
          stream.backUp(1);
          state.tokens.unshift(tokenDollar);
          break;
        }
        escaped = !escaped && next === '\\';
      }
      if (end || !escaped) {
        state.tokens.shift();
      }
      if (defin){
        return 'def'
      }else{
        return (quote === '`' || quote === ')' ? 'quote' : 'string');
      }
    };
  };

  function tokenBrack(quote) {
    return function(stream, state) {
      var next, end = false;
      while ((next = stream.next()) != null) {
        if (next === '}' ) {
          end = true;
          break;
        }
      }
        
      if (end) {
        state.tokens.shift();
      }
      return 'def';
    };
  };

  function tokenize(stream, state) {
    return (state.tokens[0] || tokenBase) (stream, state);
  };

  return {
    startState: function() {return {tokens:[]};},
    token: function(stream, state) {
      return tokenize(stream, state);
    },
    lineComment: '#',
    fold: "brace"
  };
});

CodeMirror.defineMIME('text/x-gmql', 'gmql');

});
