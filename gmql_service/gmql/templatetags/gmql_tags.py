from django import template


register = template.Library()

def _create_url(base_url, tag):
    return '<a href="%s/%s/">%s</a>' % (base_url, tag, tag)

@register.filter(is_safe=True)
def tags_list(tag_categories, base_url):
    res = ''
    res += '<ul>'
    res += '<li><a href="%s">All queries</a></li>' % base_url
    for category in tag_categories:
        res += '<p><li>%s</p>' % category
        res += '<ul>'
        for tag in tag_categories[category]:
            res += '<li><a href="%s%s/">%s</a></li>' % (base_url, tag, tag)
        res += '</ul>'
        res += '</li>'
    res += '</ul>'
    return res


@register.filter
def add_class(field, class_name):
    return field.as_widget(attrs={
        "class": " ".join((field.css_classes(), class_name))
    })

@register.filter
def partition_label(files):
    labels = []
    for file in files:
        if file.label not in labels:
            labels.append(file.label)

    file_dict = {}

    for label in labels:
        for file in files:
            if file.label != label:
                continue
            if str(file) not in file_dict:
                file_dict[str(file)] = []
            file_dict[str(file)] = file_dict[str(file)] + [file]

    return [{'labels': labels, 'name' : file_name, 'files': file_dict[file_name]} for file_name in file_dict]

