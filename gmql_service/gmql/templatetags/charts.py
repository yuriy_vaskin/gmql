from django import template

register = template.Library()

@register.filter
def partition_charts(charts):
    chart_dict = {}
    for chart in charts:
        if chart.archive_name not in chart_dict:
            chart_dict[chart.archive_name] = []

    for archive_name in chart_dict:
        for chart in charts:
            if chart.archive_name != archive_name:
                continue
            chart_dict[archive_name] = chart_dict[archive_name] + [chart]

    return [{'archive_name': archive_name, 'charts': chart_dict[archive_name]} for archive_name in chart_dict]


@register.filter(is_safe=True)
def chart_data(chart):
    d = ''
    try:
        if not chart.is_table:
            if chart.has_chromosomes:
                for chromosome in chromosomes:
                    internal_data = chart.chart_data(chromosome)
                    if not internal_data:
                        continue
                    d += ''' <script type="text/javascript"> $(function () { $('#%s_%s').highcharts({ %s }); });</script>''' % (
                        chart.container_name(), chromosome, chart.chart_data(chromosome))
                d += '''<script type="text/javascript">$(function () {                $('#chromosomeselector').change(function(){        var val = '.'+$('#chromosomeselector').val();  $('.chart_selectable').hide();  $(val).show(); });});</script>'''
                d += '''<script type="text/javascript">$(function () {                $(document).ready(function(){        var val = '.'+$('#chromosomeselector').val();  $('.chart_selectable').hide();  $(val).show(); });});</script>'''

            else:
                d = ''' <script type="text/javascript"> $(function () { $('#%s').highcharts({ %s }); });</script>''' % (
                    chart.container_name(), chart.chart_data())
    except Exception as e:
        print e
        d = ''
    return d


chromosomes = ['chr1', 'chr2', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chr10', 'chr11', 'chr12',
               'chr13',
               'chr14', 'chr15', 'chr16', 'chr17', 'chr18', 'chr19', 'chr20', 'chr21', 'chr22', 'chrX', 'chrY', 'chrM']


@register.filter(is_safe=True)
def chart_container(chart):
    html = ''
    try:
        if not chart.is_table:
            if chart.has_chromosomes:
                html += ' <select id="chromosomeselector" class="form-control input-sm">'
                for chromosome in chromosomes:
                    html += '<option value=%s>%s</option>' % (chromosome, chromosome)
                html += '</select>'
                for chromosome in chromosomes:
                    html += '<div class="chart chart_selectable %s" id="%s_%s"></div>' % (
                            chromosome, chart.container_name(), chromosome)
            else:
                html = '<div class="chart" id="%s"></div>' % chart.container_name()
        else:
            html = '<div class="table-scrollable"><table class="table table-hover chart-table">%s</table></div>' % chart.chart_data()
    except Exception as e:
        print e
        html = ''
    return html
