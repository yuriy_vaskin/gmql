{% extends "gmql/base.html" %}
{% load staticfiles %}
{% block extrahead %}
    <link href="{% static 'gmql/bootstrap/css/queries.css' %}" rel="stylesheet">
{% endblock %}
{% block gmql_content %}
    <div class="container">
        <div class="col-lg-9">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Documentation</h3></div>
                <div class="panel-body">
                <div class="doc-section">
                    <h1 id="service" class="page-header">GMQL Service</h1>
                    <p class="lead"><b>GMQL Service</b> is a collection of click-to-run bioinformatics services. The services operate at the level of regions, providing operations of rapid intersection, selection, join, etc. Each service is based on GenoMetric Query Language - a high performance generic environment for running genomic queries. GMQL is a language to formulate genomic queries, that is executed over the <a href="http://hadoop.apache.org/">Hadoop</a> high-performance environment. The services wrap selected GMQL queries providing a simple way of running them with user-defined parameters and files. Integrated services work with user data as well as with publicly available repositories (<a href="https://www.encodeproject.org/">ENCODE</a>, <a href="http://www.roadmapepigenomics.org/">Roadmap Epigenomics</a>, etc.). </p>
                    <div class="bs-callout bs-callout-info">
                        <h4>Explore the services</h4>
                        The best way to explore the GMQL services is to check the list of available tools, pick one and run it. For instance, you can get all ENCODE ChIP-Seq peaks (separated into samples), that intersect peaks of your own ChIP-Seq experiment.
                    </div>
                    <h2 id="gmql-video">GMQL video</h2>
                    <p>The video gives an overview of features of GMQL Services.</p>
                    <div align="center" class="embed-responsive embed-responsive-16by9">
                        <video autoplay loop class="embed-responsive-item">
                            <source src="{%  static 'gmql/video/gmql-demo2.mp4' %}" type=video/mp4>
                        </video>
                    </div>
                </div>

                <div class="doc-section">
                    <h1 id="gmql" class="page-header">GenoMetric Query Language (GMQL)</h1>
                    <h2 id="gmql-intro">GMQL introduction</h2>
                    <p>GMQL (GenoMetric Query Language) raises the level of data abstraction with regard to current bioinformatics query languages, as it allows query formulation by means of powerful and at the same time rather simple operators; thus, the language supports knowledge discovery along a variety of dimensions. The name genometric indicates that an important aspect of the query language is to deal with genomic distances, which are measured in terms of bases (nucleotides) between regions of the genome; as we assume that genomic data are aligned to known references, such distances are computed as simple arithmetic operations between region coordinates. In the long range, the main difficulty in dealing with NGS data will be scalability to thousands or even millions of experiments; therefore, the design of GMQL is inspired by <a href="https://pig.apache.org/docs/r0.7.0/piglatin_ref2.html">Pig Latin</a>, a high-level, declarative language that can be executed over an Hadoop cloud computing architecture. We focus on providing a model and query language for data interoperability at large scale, across experiments; interoperability is guaranteed by the use of a type system to express region data, so that compatible types support well-defined operations between them, and on arbitrary attribute-value pairs for metadata.</p>
                    <h2 id="gmql-full">GMQL full specification</h2>
                    <p>GMQL constitutes the computational core of the services. There is a possibility to write custom operations and create services on-the-fly. The full specification of the language is available here:</p>
                    <ul>
                        <li><a href="{% url 'gmql:full_doc' %}">Full GMQL Documentation</a></li>
                    </ul>
                </div>

                <div class="doc-section">
                    <h1 id="run" class="page-header">Running services</h1>
                    <h2 id="run-list">Tools list</h2>
                    <p>All available services are listed on the page of the tools. Click on a tool to set the parameters and run it. Tools that are created by an administrator appear on the top of the page. Custom user's tools appear at the bottom of the page (in a separate list). The badge <span class="badge badge-important">5</span> indicates how many times the tool has been run. Tools are sorted by the number of runs. The badge <span class="badge badge-important"><i class="fa fa-upload"></i></span> indicates that query requires data uploaded by a user.</p>
                    <div class="bs-callout bs-callout-info">
                        <h4>The search box</h4>
                        The search words are checked in names, descriptions and queries of services.
                    </div>
                    <div class="bs-callout bs-callout-success">
                        <h4>Multiple keyword search</h4>
                        Use space separated keywords to filter tools that contain at least on of the keywords.
                    </div>
                    <div class="bs-callout bs-callout-info">
                        <h4>Filtering</h4>
                        Category selector filters tools by GMQL operators, data sources, etc.
                    </div>
                    <h2 id="run-run">Run a tool</h2>
                    <p>Basically, to run a tools you need to specify input parameters, click the  <button type="submit" class="btn btn-primary btn-sm" name="run"><span class="glyphicon glyphicon-ok"></span>Run</button> button, wait until the execution is finished and download the result.
                        It is possible to see default parameters of a query with the <button type="submit" class="btn btn-info btn-sm" name="run">See defaults</button> button or run the query with the default parameters with the <button type="submit" class="btn btn-info btn-sm" name="run">Try with defaults</button>. If no default parameters are available, the buttons are hidden.
                        The <button type="submit" class="btn btn-primary btn-sm" name="run">Hint</button> button lets you check how many samples will be selected with the select statements of the query and given parameters.
                        You can see the list of all available tasks in you profile page (click the icon in the top right corner), so you can run as many tasks as you want in parallel. Only you have an access to the tasks that you run and the output data that the tasks produce.
                    <div class="bs-callout bs-callout-warning">
                        <h4>Input file format</h4>
                        Input files must be in <a href="http://genome.ucsc.edu/FAQ/FAQformat.html#format1">BED-like format</a> (BED, broadPeak, narrowPeask, etc.).
                    </div>
                    <div class="bs-callout bs-callout-info">
                        <h4>Run with defaults</h4>
                        Some of the tools have default input parameters. If so you will see additional button in the parameters box. You have an option to see default parameters (you won't see default files though) or to run a tool with default parameters.
                    </div>
                    <div class="bs-callout bs-callout-info">
                        <h4>Tool description</h4>
                        Each tools has a description. Alternately, you may want to have a look at the GMQL query invoked by the tool.
                    </div>
                    <div class="bs-callout bs-callout-danger">
                        <h4>Matching parameters</h4>
                        Text parameters should exactly match terms in the database (so GATA1, gata1, gata are three different terms). Please, use auto-completion suggestions when it is possible to avoid miss-spelling.
                    </div>
                    <div class="bs-callout bs-callout-danger">
                        <h4>Limitations</h4>
                        Note the following limitations o each task: uploadable file limit is 10, maximum uploadable file size is 100Mb, maximum task execution time is 60 minutes, the output is stored for 1 day. This limitations is a subject to change.
                    </div>

                    <h2 id="run-out">Output states</h2>
                    <p>Each task has two output states: success and error. In the success state downloadable output data (if any) appears on the page of the task. In the error state the error message is printed on the page of the task.</p>
                    <div class="bs-callout bs-callout-danger">
                        <h4>Error reporting</h4>
                        If the task is finished with an error it assumes not only an execution error. It also could be a bad parameter error, or no result error, etc.
                    </div>
                    <div class="bs-callout bs-callout-danger">
                        <h4>Output availability</h4>
                        You data will be available not more that one day after task invocation. Then the associated output and the data will be deleted.
                    </div>

                    <h2 id="run-igb">IGB visualization</h2>
                    <p>Resulting regions can be visualized in <a href="http://bioviz.org/igb/index.html">IGB (Integrated Genome Browser)</a>. GMQL services use IGB scripting language and web connection of IGB. To see your tracks, launch IGB first and click on the visualization link after. It will download your results as IGB annotation tracks.</p>
                    <div class="bs-callout bs-callout-danger">
                        <h4>hg19 only</h4>
                        IGB visualization availalbe for human genome (hg19) only.
                    </div>

                    <h2 id="run-formats">Output formats</h2>
                    <p>The result of a calculation is available in three formats: default, CSV and BED.</p>
                    <p>The <b>default</b> format is the original GMQL output with regions in the GTF format (0-based), metadata and technical details of a query. </p>
                    <p>The <b>CSV</b> format has all the regions (1-based) with sample-wide metadata added to each region. This format is the best one for further parsing of GMQL output. </p>
                    <p>The <b>BED</b> format has only all the regions (1-based) with no metadata. </p>

                    <h2 id="run-charts">Charts</h2>
                    <p class="lead">To make sense of the output data GMQL services draws charts based on the result. All the charts can be exported (top-right corner button) as images or CSV tables. Each sample has its color in the chart. To remove the sample from the chart, click on the name of the sample.</p>
                    <h4>Region distribution by chromosomes</h4>
                    <p>For each sample shows how many regions the sample has for each chromosome</p>
                    <p><img class="img-responsive" src="{% static 'gmql/images/charts/chr.png' %}"></p>

                    <h4>Metadata table</h4>
                    <p>Show pre-selected metadata terms for each sample. Colums of the table are samples and metadata terms are the row.</p>
                    <p><img class="img-responsive" src="{% static 'gmql/images/charts/table.png' %}"></p>

                    <h4>Metaterms distribution by samples</h4>
                    <p>For pre-selected metadata terms shows how many time each term occurrence in each sample.</p>
                    <p><img class="img-responsive" src="{% static 'gmql/images/charts/meta.png' %}"></p>

                    <h4>Region distribution by samples</h4>
                    <p>Show how many regions are in each sample.</p>
                    <p><img class="img-responsive" src="{% static 'gmql/images/charts/regions.png' %}"></p>

                    <h4>Heatmap of region presence</h4>
                    <p>With rows as samples and columns as regions the heatmap shows the matrix of presence of the regions. For instance, if the sample1 has the region1, but the sample2 does not have the region2 it is displayed in the heatmap. The chromosome selector is available. The heatmap is drawn only if there is MAP or JOIN opperation in the query.</p>
                    <p><img class="img-responsive" src="{% static 'gmql/images/charts/heatmap.png' %}"></p>


                    <h2 id="run-internal">Internal running process</h2>
                    <p>Services or tools are GMQL queries. Tasks are invocations of GMQL queries with specified parameters. When you run a service, text or number parameters are put inside the associated query. Input files are uploaded to the server, GMQL datasets with the files are created, and then the datasets are put inside the query as parameters. The mapping between parameters is done by parameter name. So a generic query <code>q={p}</code> for a parameter <code>p</code> with the value <code>1</code> ends up as the query <code>q=1</code>. After substitution the GMQL core receives a GMQL query and proceeds with execution. The resulting files or error messages are passed to the task. Associated queries and datasets are deleted from the GMQL core after the execution is finished.</p>

                </div>

                <div class="doc-section">
                    <h1 id="data" class="page-header">Data</h1>
                    <p class="lead">Generic nature of GMQL data model and high-performance execution environment gives a possibility to integrate and query huge data collections. GMQL services provide access to publicly available data collections that were integrated into GMQL data storage. There is a pre-set collection of tools to retrieve samples, intersect samples with user data or even take advantage of intersection data from different repositories. For instance, it s possible to retrieve all ChIP-Seq samples of ENCODE that have certain histone modification (from the <a href="http://www.roadmapepigenomics.org/">Epigenome Roadmap Project</a>) around peaks.</p>
                    <h2 id="data-encode">ENCODE</h2>
                    <p><a href="https://www.encodeproject.org/">The Encyclopedia of DNA Elements (ENCODE)</a> Consortium is an international collaboration of research groups funded by the National Human Genome Research Institute (NHGRI). The goal of ENCODE is to build a comprehensive parts list of functional elements in the human genome, including elements that act at the protein and RNA levels, and regulatory elements that control cells and circumstances in which a gene is active. </p>
                    <h2 id="data-epigenome">Epigenome Roadmap</h2>
                    <p>The <a href="http://www.roadmapepigenomics.org/">NIH Roadmap Epigenomics Mapping Consortium</a> was launched with the goal of producing a public resource of human epigenomic data to catalyze basic biology and disease-oriented research. The Consortium leverages experimental pipelines built around next-generation sequencing technologies to map DNA methylation, histone modifications, chromatin accessibility and small RNA transcripts in stem cells and primary ex vivo tissues selected to represent the normal counterparts of tissues and organ systems frequently involved in human disease.</p>
                 </div>

                 <div class="doc-section">
                    <h1 id="users" class="page-header">Users</h1>
                    <h2 id="users-signup">Sign up</h2>
                    <p>	The signup/login procedure is required to provide data privacy. Users have access only to their tasks, outputs or queries. The predefined collection of queries is available to everybody.
                    <div class="bs-callout bs-callout-info">
                        <h4>Social networks sign up</h4>
                        You can sign up using your Google or Facebook account.
                    </div>

                    <h2 id="users-userspace">User's space</h2>
                    <p>A registered user has access to his profile (the right top corner). The profile stores user information and finished or running tasks. Also custom queries that the user creates are available only to him. They appear in the bottom of the page. User can delete queries that he creates, but cannot delete pre-set services.</p>
                </div>
                <div class="doc-section">
                    <h1 id="custom" class="page-header">Custom tools</h1>
                    <h2 id="custom-create">Create a custom tool</h2>
                    <p>A GMQL service is a generic GMQL query. So buy adding new generic queries it is possible to produce new services that can be used multiple times.</p>
                    <div class="bs-callout bs-callout-warning">
                        <h4>Advanced feature</h4>
                        This is an advanced feature and requires GMQL language knowledge. Alternatively, you can take exiting queries, change them and create your custom services that way.
                    </div>

                    <p>
                    To create a service you need to define a query (with a name and a description), select parameters and their types. A parameter is selected by putting any part of a query into brackets <code>{}</code>.

                    As an example we can take the query of the service that returns ENCODE samples by cell line and antibody.</p>

                    <p><code>cell=SELECT( dataType == 'ChipSeq' AND cell == '{cell}' AND antibody == '{antibody}' ) ENCODE_BROADPEAK_HG19;</code></p>
                    <p><code>MATERIALIZE cell;</code></p>

                    <p>A new query can be created that takes only cell line as a parameter. So it will be a service that returns ENCODE samples by cell line.</p>

                    <p><code>cell=SELECT( dataType == 'ChipSeq' AND cell == '{cell}' ) ENCODE_BROADPEAK_HG19;</code></p>
                    <p><code>MATERIALIZE cell;</code></p>


                    <p>We can use the custom query creation form, define the types of the parameters and produce a new service.</p>
                 </div>
                 <div class="doc-section">
                    <h1 id="api" class="page-header">REST API</h1>
                    <p class="lead">Browsable <a href="{% url 'rest' %}">GMQL REST API</a> is available by <a href="http://www.django-rest-framework.org/">Django REST Framework</a>. Before using the API you can browse it and check the methods for extra documentation.</p>
                    <h2 id="api-methods">API methods</h2>
                    <p> To query the API methods you need to register first and send credentials with an API request.
                    <ul>
                    <li><b>GET queries </b> lists all public and custom queries.</li>
                    <li><b>GET tasks </b> lists tasks with query results that you invoked.</li>
                    <li><b>GET task </b> shows detailed information and output files of an individual task.</li>
                    <li><b>GET/PUT/PATCH query </b> shows detailed information of a query and allow running it with specified parameters.</li>
                    </ul>
                    </p>
                    <h2 id="api-example">API call example</h2>
                    <p>The example below shows how to call the GMQL API from Python code. The call is performed with <a href="http://docs.python-requests.org/en/latest/">requests library</a>.</p>
                     <pre><code>import requests, json, time
LOGIN = 'login'
PASS = 'pass'
#get list of all queries
query = requests.get('http://cru.genomics.iit.it/GMQL/rest/queries/', auth=(LOGIN, PASS)).content

#run a query #6 with specified parameters
resp_patch = requests.patch('http://cru.genomics.iit.it/GMQL/rest/queries/6/', json={"parameter_set" : [{"name":"cell","value":"K562"},{"name":"antibody", "value":"CBX2"}]}, auth=(LOGIN, PASS))

if resp_patch.ok:
	while True:
		#get all the tasks of the user, running and stopped
		resp_tasks = requests.get('http://cru.genomics.iit.it/GMQL/rest/tasks/', auth=(LOGIN, PASS))

		if resp_tasks.ok:
			#get the last task, that correspondes to the query
			last_task = json.loads(resp_tasks.content)[-1]

			if last_task['status'] == 'Running':
				#performance timer
				time.sleep(5)
				continue

			elif last_task['status'] == 'Success':
				#downloadable results
				gmqlfile_set = last_task['gmqlfile_set']

			elif last_task['status'] == 'Error':
				#error message, if any
				last_error = last_task['last_error']
		break
 </code></pre>
                <p>Another example demonstate how to call GMQL API from R code. The call is performed with <a href="http://cran.r-project.org/web/packages/httr/index.html">httr library</a>.</p>
                     <pre><code>library(httr)
query <- GET('http://cru.genomics.iit.it/GMQL/rest/queries/', authenticate("LOGIN", "PASS"))
stop_for_status(query)
data_json<-content(query, "parsed", "application/json")
     </code></pre>

                   <h2 id="api-galaxy">Galaxy integration</h2>
                    <p>The prototype below shows how to integrate GMQL Services as <a href="https://galaxyproject.org/">Galaxy</a> Tools. The call from Galaxy are performed through GMQL Services REST API.</p>
                     <ol>
                         <li>In the Galaxy <code>tools/</code> create directory <code>gmql</code></li>
                         <li>Create a file <code>gmql_chip_cell_antibody.py</code> with the script:
                            <pre><code>#!/usr/bin/env python

import requests, json, time, sys, os

LOGIN = 'login'
PASS = 'pass'

def __main__():
    #get list of all queries
    query = requests.get('http://cru.genomics.iit.it/GMQL/rest/queries/', auth=(LOGIN, PASS)).content

    #run a query #6 with specified parameters
    resp_patch = requests.patch('http://cru.genomics.iit.it/GMQL/rest/queries/6/', json={"parameter_set" : [{"name":"cell","value":sys.argv[1]},{"name":"antibody", "value":sys.argv[2]}]}, auth=(LOGIN, PASS))

    if resp_patch.ok:
        while True:
            #get all the tasks of the user, running and stopped
            resp_tasks = requests.get('http://cru.genomics.iit.it/GMQL/rest/tasks/', auth=(LOGIN, PASS))

            if resp_tasks.ok:
                #get the last task, that correspondes to the query
                last_task = json.loads(resp_tasks.content)[-1]

                if last_task['status'] == 'Running':
                    #performance timer
                    time.sleep(5)
                    continue

                elif last_task['status'] == 'Success':
                    #downloadable results
                    gmqlfile_set = last_task['gmqlfile_set']

                elif last_task['status'] == 'Error':
                    #error message, if any
                    last_error = last_task['last_error']
            break

if __name__ == "__main__" : __main__()


                            </code></pre>
                         </li>
                         <li>Create the tool configuration file <code>chip_cell_antibody.xml</code> with the following XML:
                             <code><pre>&lt;tool id=&quot;gmql_chip_cell_antibody&quot; name=&quot;GMQL Get ChIP-Seq Data&quot;&gt;
	&lt;command interpreter=&quot;pyhton&quot;&gt;gmql_chip_cell_antibody.py $cell $antibody&lt;/command&gt;
&lt;inputs&gt;
&lt;param name=&quot;cell&quot; type=&quot;text&quot; format=&quot;text&quot; label=&quot;Cell line. Example: K562&quot;/&gt;
&lt;param name=&quot;antibody&quot; type=&quot;text&quot; format=&quot;text&quot; label=&quot;Antibody. Example: CBX2&quot;/&gt;
&lt;/inputs&gt;
&lt;help&gt;
Returns ENCODE broadPeak samples of ChIP-Seq experiments of given cell line and antibody

&lt;/help&gt;&lt;/tool&gt;
                         </code></pre></li>
                         <li>Add to <code>tool_conf.xml</code>:

<code><pre>&lt;section name=&quot;GMQL&quot; id=&quot;gmql&quot;&gt;
   &lt;tool file=&quot;gmql/chip_cell_antibody.xml&quot; /&gt;
&lt;/section&gt;
                         </code></pre></li>
                         <li>Restart Galaxy</li>
                         <li>The GMLQ tools should appear in the Galaxy Tools list as follows:
                         <p><img class="img-responsive" src="{% static 'gmql/images/galaxy.png' %}"></p></li>
                     </ol>
                </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="sidebar">
                <ul>
                    <li><a href="#service">GMQL Service</a></li>
                    <ul>
                        <li><a href="#gmql-video">GMQL video</a></li>
                    </ul>

                    <li>
                        <a href="#gmql">GenoMetric Query Language (GMQL)</a>
                        <ul>
                            <li><a href="#gmql-intro">GMQL introduction</a></li>
                            <li><a href="#gmql-full">GMQL full specification</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#run">Running services</a>
                        <ul>
                            <li><a href="#run-list">Tools list</a></li>
                            <li><a href="#run-run">Run a tool</a></li>
                            <li><a href="#run-out">Output states</a></li>
                            <li><a href="#run-igb">IGB visualization</a></li>
                            <li><a href="#run-formats">Output formats</a></li>
                            <li><a href="#run-charts">Charts</a></li>
                            <li><a href="#run-internal">Internal running process</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#data">Data</a>
                        <ul>
                            <li><a href="#data-encode">ENCODE</a></li>
                            <li><a href="#data-epigenome">Epigenome Roadmap</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#users">Users</a>
                        <ul>
                            <li><a href="#users-signup">Sign up</a></li>
                            <li><a href="#users-userspace">User's space</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#custom">Custom tools</a>
                        <ul>
                            <li><a href="#custom-create">Create a custom tool</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#api">REST API</a>
                        <ul>
                            <li><a href="#api-methods">API methods</a></li>
                            <li><a href="#api-example">API call example</a></li>
                            <li><a href="#api-galaxy">Galaxy integration</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Back to top</a>
                    </li>
                </ul>

            </div>
        </div>
    </div>

{% endblock %}
