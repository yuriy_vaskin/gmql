from django import forms
from django.contrib import admin
from django.forms import Textarea

from gmql.models import Query, Parameter, Task, GMQLFile, TaskHistoryRecord


class FileInline(admin.TabularInline):
    model = GMQLFile
    extra = 0
    can_delete = False


class ParameterInline(admin.TabularInline):
    model = Parameter
    extra = 0
    can_delete = False



class QueryForm(forms.ModelForm):
    class Meta:
        model = Query
        widgets = {
            'query': Textarea(attrs={'cols': 30, 'rows': 5}),
        }

class QueryAdmin(admin.ModelAdmin):
    inlines = [ParameterInline]
    form = QueryForm



class TaskAdmin(admin.ModelAdmin):
    inlines = [FileInline]


admin.site.register(Query, QueryAdmin)
admin.site.register(Task, TaskAdmin)
admin.site.register(TaskHistoryRecord)