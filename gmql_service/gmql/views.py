from django.core.exceptions import SuspiciousFileOperation
from django.core.files.storage import FileSystemStorage
from gmql.utils.SpecialMeta import SpecialMeta
from gmql.utils.TagsRegistry import Tags
import os
from allauth.account.adapter import DefaultAccountAdapter
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse, Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.decorators import method_decorator
from django.views.generic import DetailView
from django.conf import settings

from gmql.models import Query, Parameter, CustomQueryModelForm, ParametersFormset, \
    generate_forms_list, Task, GMQLFile, QuerySearchForm, add_default_files, Chart
from gmql.utils import gmql_parser
from gmql.tasks import process_task


def index(request):
    if request.user.is_authenticated():
        return redirect(reverse('gmql:queries'))
    return render(request, 'gmql/index.html')


def about(request):
    return render(request, 'gmql/about.html')

def documentation(request):
    return render(request, 'gmql/documentation.html')


def get_pdf_file_response(file_name):
    with open(os.path.join(settings.MEDIA_ROOT, 'docs', file_name), 'r') as pdf:
        response = HttpResponse(pdf.read(),content_type='application/pdf')
        response['Content-Disposition'] = 'filename=%s' % file_name
        return response


def get_txt_file_response(file_name):
    with open(os.path.join(settings.MEDIA_ROOT, 'dicts', file_name), 'r') as pdf:
        response = HttpResponse(pdf.read(),content_type='text/plain')
        response['Content-Disposition'] = 'filename=%s' % file_name
        return response



def full_doc(request):
    return get_pdf_file_response('GMQL_Complete_Documentation.pdf')


def licence(request):
    return get_pdf_file_response('gpl-2.0.pdf')

def metadata(request, meta_name):
    res_meta = None
    metas = SpecialMeta.special_metas
    for m in metas:
        if m.name == meta_name:
            res_meta = m
            break
    if not res_meta:
        raise Http404('No %s metadata found.' % meta_name)

    return get_txt_file_response(res_meta.file_name)


def queries(request, filter=None):
    mostly_used_query_list = Query.objects.globals().tags(filter).order_by('-used')
    custom_query_list = None
    if request.user.is_authenticated():
        custom_query_list = Query.objects.customs(user=request.user).tags(filter).order_by('-used')

    search_form = QuerySearchForm(request.POST or None)
    if search_form.is_valid():
        mostly_used_query_list = mostly_used_query_list.search(search_form.cleaned_data['search_value'])
        if custom_query_list:
            custom_query_list = custom_query_list.search(search_form.cleaned_data['search_value'])


    context = {'mostly_used_query_list': mostly_used_query_list,
                'custom_query_list': custom_query_list,
                'tag_categories' : Tags.tags_dict(),
                'search_form' : search_form,
                }

    return render(request, 'gmql/queries.html', context)

@login_required()
def profile(request):
    context = {}
    context['custom_query_list'] = Query.objects.customs(user=request.user).order_by('-used')
    context['task_list'] = Task.objects.filter(user=request.user)
    return render(request, 'gmql/profile.html', context)


@login_required()
def delete_query(request, query_id):
    query = get_object_or_404(Query, pk=query_id)
    if query.user == request.user and not query.is_global:
        query.delete()
    return redirect(reverse('gmql:queries'))


# TODO: demo user
@login_required()
def query(request, query_id):
    query = get_object_or_404(Query, pk=query_id)

    if not request.user.is_superuser and query.user and not query.is_global and query.user != request.user:
        raise Http404

    parameters = Parameter.objects.filter(query=query_id)
    parameters_data = list(parameters.values())
    formset = generate_forms_list(parameters_data)

    if request.method == 'POST':
        if 'run' in request.POST or 'try' in request.POST:
            formset = generate_forms_list(parameters_data, request.POST, request.FILES)
            if 'try' in request.POST:
                formset = generate_forms_list(parameters_data, request.POST, request.FILES, use_default=True)
            if all([form.is_valid() for form in formset]):
                parameters_dict = {}
                [parameters_dict.update(f.get_parameters_dict(parameters_data)) for f in formset]
                task = Task(query_id=query_id, user_id=request.user.id)
                task.set_parameters(parameters_dict)
                task.set_status_msg(Task.PREPARED)
                files = request.FILES
                if 'try' in request.POST:
                    files = add_default_files(parameters_data, request.FILES)
                result = process_task.delay(task, files)
                return redirect(reverse('gmql:task', args=[task.id]))
        elif 'see' in request.POST:
            formset = generate_forms_list(parameters_data, request.POST, request.FILES, use_default=True)

        elif 'check' in request.POST:
            formset = generate_forms_list(parameters_data, request.POST, request.FILES)
            if all([form.is_valid() for form in formset]):
                parameters_dict = {}
                [parameters_dict.update(f.get_parameters_dict(parameters_data)) for f in formset]
                statements = query.extract_selection_statements(parameters_dict)
                context = {'query': query,
                            'formset': formset,
                            'select_data': None,
                           'hint_statements': {'checked':True, 'statements': statements}}
                return render(request, 'gmql/query.html', context)


    context = {'query': query,
               'formset': formset}
    return render(request, 'gmql/query.html', context)


class TaskView(DetailView):
    model = Task
    template_name = 'gmql/task.html'

    def get_object(self, queryset=None):
        obj = super(TaskView, self).get_object()
        if not self.request.user.is_superuser and obj.user is not None and obj.user != self.request.user:
            raise Http404
        return obj

    def get_context_data(self, **kwargs):
        context = super(TaskView, self).get_context_data(**kwargs)
        context['file_list'] = GMQLFile.objects.filter(task=self.object.pk, file_type=GMQLFile.DOWNLOAD)

        #TODO add for other genomes
        if self.object.query.is_hg19():
            context['igb_files'] = GMQLFile.objects.filter(task=self.object.pk, file_type=GMQLFile.BROWSE, label='IGB')

        context['charts'] = Chart.objects.filter(task=self.object).order_by('name')
        #context['charts'] = [
        #    Chart(task=self.object, name='chast_name', archive_name='arch_name', pk=1),
        #                     ]

        return context

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TaskView, self).dispatch(*args, **kwargs)


@login_required()
def file_download(request, pk, file_id):
    gmqlfile = GMQLFile.objects.get(id=file_id)
    if not request.user.is_superuser and gmqlfile.task.user and gmqlfile.task.user != request.user:
        raise Http404
    response = HttpResponse()
    response['Content-Disposition'] = "attachment; filename=%s" % (gmqlfile)
    response['X-Accel-Redirect'] = settings.MEDIA_URL + gmqlfile.download_path
    return response


@login_required()
def custom(request):
    if request.method == 'POST':
        if 'submit_example' in request.POST:
            print request.POST
            example_query = Query()
            example_query.set_example_data()
            custom_form = CustomQueryModelForm(prefix='query', instance=example_query)
        else:
            custom_form = CustomQueryModelForm(request.POST, prefix='query')
        parameters_form = None
        context = {'custom_form': custom_form}

        if 'submit_parameters' in request.POST:
            # generate parameters
            if custom_form.is_valid():
                parameters = gmql_parser.parse_gqml_parameters(custom_form.cleaned_data['query'])
                if len(parameters) != 0:
                    #parameters_form
                    initials = [{'name': par, 'description': 'Description of ' + par} for par in parameters]
                    parameters_form = ParametersFormset(initial=initials, )

        elif 'submit_create' in request.POST:
            custom_form = CustomQueryModelForm(request.POST, prefix='query')
            parameters_form = ParametersFormset(request.POST)
            if custom_form.is_valid() and parameters_form.is_valid():
                new_query = Query(**custom_form.cleaned_data)
                new_query.user = request.user
                new_query.save()
                for parameter_form in parameters_form:
                    new_parameter = Parameter(**parameter_form.cleaned_data)
                    new_parameter.query = new_query
                    new_parameter.save()
                return redirect(reverse('gmql:query', args=[new_query.id]))
    else:
        custom_form = CustomQueryModelForm(prefix='query')
        parameters_form = None

    context = {'custom_form': custom_form,
               'special_metas': SpecialMeta.special_metas}
    if parameters_form is not None:
        context['parameters_form'] = parameters_form
    return render(request, 'gmql/custom_form.html', context)


class AccountAdapter(DefaultAccountAdapter):
    def get_login_redirect_url(self, request):
        return reverse('gmql:queries')



#HACK to avoid exeptions on deleting files with bad path
class FileStorage(FileSystemStorage):
    def exists(self, name):
        try:
            return super(FileStorage, self).exists(name)
        except SuspiciousFileOperation:
            return False
