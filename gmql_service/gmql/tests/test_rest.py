from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from gmql.models import Query, Parameter
from rest_framework import status
from rest_framework.test import APITestCase


class QueryTests(APITestCase):
    def setUp(self):
        self.username = 'test'
        if not User.objects.filter(username='test').exists():
            User.objects.create_user(username='test', email='test@test.com', password='test')
        self.client.login(username=self.username, password='test')


    def test_run_query(self):
        q1 = Query(name="Example name", description="Example description", query="q1={q1}; q2={q2}")
        q1.save()
        p1 = Parameter(query=q1, name="q1", description='descrq1')
        p1.save()
        p2 = Parameter(query=q1, name="q2", description='descrq2')
        p2.save()

        url = reverse('query-detail', args=[q1.pk])
        data = {"url":"http://testserver/rest/queries/1/","name":"Example name","description":"Example description","query":"q1={q1}; q2={q2}","parameter_set":[{"name":"q1","description":"descrq1","parameter_type":"Text", "value":"1"},{"name":"q2","description":"descrq2","parameter_type":"Text", "value":"2"}]}
        response = self.client.put(url, data, format='json')

        url = reverse('task-list')
        response = self.client.get(url, format='json')
        self.assertTrue(len(list(response)) != 0)


    def test_run_query_patch(self):
        q1 = Query(name="Example name", description="Example description", query="q1={q1}; q2={q2}")
        q1.save()
        p1 = Parameter(query=q1, name="q1", description='descrq1')
        p1.save()
        p2 = Parameter(query=q1, name="q2", description='descrq2')
        p2.save()

        url = reverse('query-detail', args=[q1.pk])
        data = {"parameter_set":[{"name":"q1","value":"1"},{"name":"q2", "value":"2"}]}
        response = self.client.patch(url, data, format='json')

        url = reverse('task-list')
        response = self.client.get(url, format='json')
        self.assertTrue(len(list(response)) != 0)
