from django.test import TestCase
from django.test.utils import override_settings
from gmql.management.commands import create_queries
from gmql.models import test_file_path, Parameter

from gmql.tasks import *
from gmql.tests.testutils import QueryTester, fileiterator


@override_settings(CELERY_ALWAYS_EAGER=True)
@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class GMQLQueryBaseTest(TestCase):
    def run_query(self):
        try:
            result = process_task.delay(self.tester.task, self.tester.files, self.tester.requester)
            self.assertEqual(self.tester.task.status, Task.SUCCESS)

            files = self.tester.result_files()
            self.assertTrue(len(files) > 0)
            for file in files:
                self.assertTrue(os.path.exists(file.file.path))

        except Exception as er:
            self.fail(er)

    def tearDown(self):
        if self.tester:
            self.tester.clear()


class Sort(GMQLQueryBaseTest):
    def test_run_task(self):
        self.tester = QueryTester(query='s=SELECT(*) {filef}; MATERIALIZE s;',
                                  parameters_dict={"filef": "filef"},
                                  files_dict={"filef": ["input/1.bed"]})

        super(Sort, self).run_query()


class Intersect(GMQLQueryBaseTest):
    def test_run_task(self):
        self.tester = QueryTester(query='I1=SELECT(*) {file}; intersect=COVER(ALL,ALL) I1; MATERIALIZE intersect;',
                                  parameters_dict={"file": "file"},
                                  files_dict={"file": ["input/i1.bed", "input/i2.bed"]})

        super(Intersect, self).run_query()


class Annotate(GMQLQueryBaseTest):
    def test_run_task(self):
        self.tester = QueryTester(
            query='A1=SELECT(*) {file1}; A2=SELECT(*) {file2}; annotate=MAP(count AS COUNT) A1 A2; MATERIALIZE annotate;',
            parameters_dict={"file1": "file1", "file2": "file2"},
            files_dict={"file1": ["input/a1.bed"], "file2": ["input/a2.bed", "input/a3.bed"]})

        super(Annotate, self).run_query()


class Map(GMQLQueryBaseTest):
    def test_run_task(self):
        self.tester = QueryTester(
            query='M1=SELECT(*) {m1}; M2=SELECT(*) {m2}; mapResult=MAP(sum AS SUM(score)) M1 M2; MATERIALIZE mapResult;',
            parameters_dict={"m1": "m1", "m2": "m2"},
            files_dict={"m1": ["input/map1.bed"], "m2": ["input/map2.bed"]})

        super(Map, self).run_query()


class CellAndAntybodyName(GMQLQueryBaseTest):
    def test_run_task(self):
        self.tester = QueryTester(
            query="cell=SELECT( dataType == 'ChipSeq' AND cell == '{cell_name}' AND antibody == '{antibody_name}' ) ENCODE_BROADPEAK_HG19; MATERIALIZE cell;",
            parameters_dict={"cell_name": "K562", "antibody_name": "CBX2"}, )

        super(CellAndAntybodyName, self).run_query()


class CustomCellName(GMQLQueryBaseTest):
    def test_run_task(self):
        self.tester = QueryTester(
            query="custom=SELECT(*) {file}; cell=SELECT( dataType == 'ChipSeq' AND cell == '{cell_name}' ) ENCODE_BROADPEAK_HG19; joinResult=JOIN(distance < 0, right) custom cell; MATERIALIZE joinResult;",
            parameters_dict={"file": "file", "cell_name": "K562"},
            files_dict={"file": ["input/encode_intersect.bed"]})

        super(CustomCellName, self).run_query()


class AntybodyName(GMQLQueryBaseTest):
    def test_run_task(self):
        self.tester = QueryTester(
            query="ab=SELECT( dataType == 'ChipSeq' AND antibody_target == '{antibody_name}' ) ENCODE_BROADPEAK_HG19; MATERIALIZE ab;",
            parameters_dict={"antibody_name": "REST"}, )

        super(AntybodyName, self).run_query()


class CustomAntybodyName(GMQLQueryBaseTest):
    def test_run_task(self):
        self.tester = QueryTester(
            query="custom=SELECT(*) {file}; ab=SELECT( dataType == 'ChipSeq' AND antibody_target == '{antibody_name}' ) ENCODE_BROADPEAK_HG19; joinResult=JOIN(distance < 0, right) custom ab; MATERIALIZE joinResult;",
            parameters_dict={"file": "file", "antibody_name": "REST"},
            files_dict={"file": ["input/encode_intersect.bed"]})

        super(CustomAntybodyName, self).run_query()


class PromoterFlank(GMQLQueryBaseTest):
    def test_run_task(self):
        self.tester = QueryTester(
            query="F1=SELECT(*) {file}; FW1=PROJECT(*; start = start-{l}, stop=start) F1; MATERIALIZE FW1;",
            parameters_dict={"file": "file", "l": "5", },
            files_dict={"file": ["input/f1.bed"]})

        super(PromoterFlank, self).run_query()


class Slop(GMQLQueryBaseTest):
    def test_run_task(self):
        self.tester = QueryTester(
            query="S1=SELECT(*) {file}; SW1=PROJECT(*; start = start-{l}, stop=stop+{r}) S1; MATERIALIZE SW1;",
            parameters_dict={"file": "file", "l": "100", "r": "100"},
            files_dict={"file": ["input/s1.bed"]})

        super(Slop, self).run_query()


class Window(GMQLQueryBaseTest):
    def test_run_task(self):
        self.tester = QueryTester(
            query="I1=SELECT(*) {file1}; I2=SELECT(*) {file2}; IW1=PROJECT(*; start = start-{w}, stop=stop+{w}) I1; U=UNION IW1 I2; window=COVER(ALL,ALL) U; MATERIALIZE window;",
            parameters_dict={"file1": "file1", "file2": "file2", "w": "5000", },
            files_dict={"file1": ["input/w1.bed"],
                        "file2": ["input/w2.bed"]})

        super(Window, self).run_query()



@override_settings(CELERY_ALWAYS_EAGER=True)
@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class QueryFromCommandTest(GMQLQueryBaseTest):
    def test_commands(self):
        command = create_queries.Command()
        command.handle()

        for q in Query.objects.all():
            if q.has_defaults():
                parameters_dict = {p.name: p.get_default_value() for p in Parameter.objects.filter(query=q.pk)}
                file_dict = {p.name: p.get_default_file_names() for p in Parameter.objects.filter(query=q.pk) if
                             p.get_default_file_names()}
                self.tester = QueryTester(query=q.query,
                                          parameters_dict=parameters_dict,
                                          files_dict=file_dict)
                super(QueryFromCommandTest, self).run_query()
                self.tester.clear()


"""
#intersect with genes
#https://www.biostars.org/p/18112/#18114
@override_settings(CELERY_ALWAYS_EAGER=True)
@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class GMQLBaseQueryTaskTest(TestCase):
    def setUp(self):
        self.requester = GMQLRequester()
        q1 = Query(name="join_query", description="description",
                   query="custom=SELECT(*) {test_j1};"
                         "cell=SELECT( dataType == 'ChipSeq' AND cell == '{cell_name}' ) ENCODE_BROADPEAK_HG19;"
                         "joinResult=JOIN(distance < 0, right) custom cell;"
                         "MATERIALIZE joinResult;"
        )
        q1.save()
        parameters = {"test_j1": "file1","cell_name": "K562"}
        self.task = Task(query_id=q1.pk)
        self.task.set_parameters(parameters)
        self.task.save()

    def test_run_task_and_download_result(self):
        try:
            upload_file1 = open(test_file_path('input/encode_intersect.bed'), 'rb')
            files = MultiValueDict({'test_j1-value': [SimpleUploadedFile(upload_file1.name, upload_file1.read())],
            })
            result = process_task.delay(self.task, files)
            print self.task.last_error
            self.assertEqual(self.task.status, Task.SUCCESS)

            files = GMQLFile.objects.filter(task=self.task, file_type=GMQLFile.DOWNLOAD)
            self.assertTrue(len(files) > 0)
            for file in files:
                self.assertTrue(os.path.exists(file.file.path))

        except Exception as er:
            self.fail(er)

    def tearDown(self):
        all_files = GMQLFile.objects.filter(task=self.task)
        #for file in all_files:
        #    file.delete()
        #self.requester.clear()



#mutations and encode regions

#https://www.biostars.org/p/8807/#8809
#promoters

"""


class DBMap(GMQLQueryBaseTest):
    def test_run_task(self):
        #for each sample in DB returns a sample with regions from CUSTOM, with a number of intersected features in a sample in DB
        #add project to remove regions that do not have intersections
        self.tester = QueryTester(
            query="db=SELECT(*) {file1}; custom=SELECT(*) {file2}; db_map=MAP(db_intersect AS COUNT) custom db; MATERIALIZE db_map;",
            parameters_dict={"file1": "file1", "file2": "file2"},
            files_dict={"file1": ["input/db1.bed", "input/db2.bed"],
                        "file2": ["input/db_sample.bed"]},
            clear_files=False)

        super(DBMap, self).run_query()


class DBCover(GMQLQueryBaseTest):
    def test_run_task(self):
        ##return regions that exist in ALL samples in db and custom. Changes regions
        self.tester = QueryTester(
            query="db=SELECT(*) {file1}; custom=SELECT(*) {file2}; u=UNION custom db; db_cover=COVER(ALL, ALL) u; MATERIALIZE db_cover;",
            parameters_dict={"file1": "file1", "file2": "file2"},
            files_dict={"file1": ["input/db1.bed", "input/db2.bed"],
                        "file2": ["input/db_sample.bed"]},
            clear_files=False)

        super(DBCover, self).run_query()

class DBJoin(GMQLQueryBaseTest):
    def test_run_task(self):
        ##for each sample in DB. return DB regions that intersect custom regions. Do not change regions. Left right can be used to return custom/db regions
        self.tester = QueryTester(
            query="db=SELECT(*) {file1}; custom=SELECT(*) {file2}; db_join=JOIN(distance<0, right) custom db; MATERIALIZE db_join;",
            parameters_dict={"file1": "file1", "file2": "file2"},
            files_dict={"file1": ["input/db1.bed", "input/db2.bed"],
                        "file2": ["input/db_sample.bed"]},
            clear_files=False)

        super(DBJoin, self).run_query()
