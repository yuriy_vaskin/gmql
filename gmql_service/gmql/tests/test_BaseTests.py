import shutil
import time
from django.contrib.auth.models import User
from django.core.files import File
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.urlresolvers import reverse, resolve
from django.forms import formset_factory
from django.test import TestCase, Client
from django.test.utils import override_settings
from django.utils.datastructures import MultiValueDict
from gmql.utils.ChartCalculators import ChartChromosomeDistribution, TableMetadataTerms, ChartHeatmapMapOperation
from gmql.utils.GMQLArchive import GMQLArchive, GMQLArchiveWriter, GMQLArchiveBEDWriter, GMQLArchiveCSVWriter, \
    GMQLArchiveIGBWriter
from gmql.utils.GenomeSpace import GenomeSpace, GenomeSpaceRegion, GenomeSpaceSample, GenomeSpaceWriter
from gmql.utils.SelectionHint import SelectStatement, SelectionTerm
from gmql.utils.SpecialMeta import encode_meta, SpecialMeta
from gmql.utils.TagsRegistry import Tags
from rebar.testing import flatten_to_dict


from gmql.models import CustomQueryModelForm, CustomParameterModelForm, Parameter, generate_forms_list, test_file_path, \
    Chart, ChartCalculatorRunner, TaskHistoryRecord
from gmql.tests.testutils import data_with_prefix, create_query_from_example, get_example_response, \
    LoginedClientTestCase
from gmql.utils import gmql_parser
from gmql.utils import FormatGuess
from gmql.views import index, custom, queries, about, query
from gmql.tasks import *


class URLSResolvableTests(TestCase):
    def test_resolves_to_index_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_resolves_to_custom_page_view(self):
        found = resolve('/custom/')
        self.assertEqual(found.func, custom)

    def test_resolves_to_about_page_view(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)

    def test_resolves_to_queries_page_view(self):
        found = resolve('/queries/')
        self.assertEqual(found.func, queries)

    def test_resolves_to_task_page_view(self):
        found = resolve('/task/1/')
        self.assertEqual(found.url_name, 'task')


class GMQLParserTests(TestCase):
    def test_get_parameters_1(self):
        parameters = gmql_parser.parse_gqml_parameters("a={val}; b={val1}")
        self.assertEqual(parameters, ['val', 'val1'])

    def test_get_parameters_2(self):
        parameters = gmql_parser.parse_gqml_parameters("a={val}; b={val}")
        self.assertEqual(parameters, ['val'])

    def test_get_parameters_3(self):
        parameters = gmql_parser.parse_gqml_parameters("")
        self.assertEqual(parameters, [])

    def test_get_parameters_4(self):
        parameters = gmql_parser.parse_gqml_parameters("1")
        self.assertEqual(parameters, [])

    def test_get_parameters_5(self):
        parameters = gmql_parser.parse_gqml_parameters("{}")
        self.assertEqual(parameters, [])


class GMQLParameterSetterTests(TestCase):
    def test_set_parameters_1(self):
        query = 'a={par}'
        parameters = {'par': 'val'}
        preset_query = gmql_parser.set_gqml_parameters(query, parameters)
        self.assertEqual(preset_query, 'a=val')

    def test_set_parameters_2(self):
        query = 'a={par} and b={par1}'
        parameters = {'par': 'val', 'par1': 'val1'}
        preset_query = gmql_parser.set_gqml_parameters(query, parameters)
        self.assertEqual(preset_query, 'a=val and b=val1')

    def test_set_parameters_3(self):
        query = 'a={par} and b={par}'
        parameters = {'par': 'val'}
        preset_query = gmql_parser.set_gqml_parameters(query, parameters)
        self.assertEqual(preset_query, 'a=val and b=val')


class GMQLCustomFormGeneratorTests(TestCase):
    def test_query_form_valid_1(self):
        query = Query(name='name1', description='description1', query='q={1}')
        data = {'name': query.name, 'description': query.description, 'query': query.query}
        form = CustomQueryModelForm(data=data)
        self.assertTrue(form.is_valid())

    def test_query_form_example_valid(self):
        query = Query()
        query.set_example_data()
        data = {'name': query.name, 'description': query.description, 'query': query.query}
        form = CustomQueryModelForm(data=data)
        self.assertTrue(form.is_valid())

    def test_query_form_invalid_no_query_parameter(self):
        query = Query(name='name1', description='description1', query='q={}')
        data = {'name': query.name, 'description': query.description, 'query': query.query}
        form = CustomQueryModelForm(data=data)
        self.assertFalse(form.is_valid())

    def test_query_form_invalid_no_required_parameter(self):
        query = Query(name='', description='description1', query='q={1}')
        data = {'name': query.name, 'description': query.description, 'query': query.query}
        form = CustomQueryModelForm(data=data)
        self.assertFalse(form.is_valid())

    def test_parameters_form_valid(self):
        ParamtersFormset = formset_factory(CustomParameterModelForm, extra=0)
        query = 'q={1}'
        parameters = gmql_parser.parse_gqml_parameters(query)
        initials = [{'name': par, 'description': 'Description of ' + par} for par in parameters]
        parameters_form = ParamtersFormset(initial=initials)

        form_data = flatten_to_dict(parameters_form)
        bound_form = ParamtersFormset(data=form_data)
        self.assertTrue(bound_form.is_valid())


    def test_parameters_form_invalid_with_default_files(self):
        ParamtersFormset = formset_factory(CustomParameterModelForm, extra=0)
        query = 'q={1}'
        parameters = gmql_parser.parse_gqml_parameters(query)
        initials = [{'name': par, 'description': 'Description of ' + par, "default_value" : "asdf", "parameter_type": Parameter.FILE} for par in parameters]
        parameters_form = ParamtersFormset(initial=initials)

        form_data = flatten_to_dict(parameters_form)
        bound_form = ParamtersFormset(data=form_data)
        self.assertFalse(bound_form.is_valid())

    def test_example_valid(self):
        example_query = Query()
        example_query.set_example_data()
        custom_form = CustomQueryModelForm(prefix='query', instance=example_query)
        form_data = flatten_to_dict(custom_form)
        bound_form = CustomQueryModelForm(prefix='query', data=form_data)
        self.assertTrue(bound_form.is_valid())


class CustomQueryRequests(LoginedClientTestCase):
    def test_custom_query_accessible(self):
        response = self.client.get(reverse('gmql:custom'))
        self.assertEqual(response.status_code, 200)

    def test_custom_query_example_substitutes(self):
        example_query = Query()
        example_query.set_example_data()
        custom_form = CustomQueryModelForm(prefix='query', instance=example_query)
        example_data = flatten_to_dict(custom_form)


        data = {'submit_example': ''}
        response = self.client.post(reverse('gmql:custom'), data)
        form = response.context['custom_form']
        form_data = flatten_to_dict(form)

        self.assertEqual(form_data, example_data)

    def test_parameters_generated_from_example(self):
        response = get_example_response(self.client)

        parameters_formset = response.context['parameters_form']
        parameters_data = flatten_to_dict(parameters_formset)

        query = Query()
        query.set_example_data()
        parameters = gmql_parser.parse_gqml_parameters(query.query)
        self.assertEqual(len(parameters), len(parameters_formset))

        for p in parameters:
            self.assertIn(p, parameters_data.values())

    def test_query_created_from_example(self):
        response = create_query_from_example(self.client)


        query = Query()
        query.set_example_data()

        created_queries = Query.objects.filter(name=query.name)
        self.assertGreater(len(created_queries), 0)

    def test_query_created_from_example_with_defaults(self):
        response = create_query_from_example(self.client, default=True)

        query = Query()
        query.set_example_data()

        created_queries = Query.objects.filter(name=query.name)
        self.assertGreater(len(created_queries), 0)
        self.assertTrue(created_queries[0].has_defaults())


    def test_query_created_has_file_input(self):
        parameters = {"name": "n", "description": "d", "parameter_type": Parameter.FILE}
        q = Query(name="q", description="d", query="p={v};")
        q.save()
        p = Parameter(query=q, name=parameters["name"], description=parameters["description"],
                      parameter_type=parameters["parameter_type"])
        p.save()

        created_queries = Query.objects.filter(name=q.name)
        self.assertGreater(len(created_queries), 0)
        self.assertTrue(created_queries[0].has_file_input())


    def test_query_created_has_no_file_input(self):
        parameters = {"name": "n", "description": "d", "parameter_type": Parameter.TEXT}
        q = Query(name="q", description="d", query="p={v};")
        q.save()
        p = Parameter(query=q, name=parameters["name"], description=parameters["description"],
                      parameter_type=parameters["parameter_type"])
        p.save()

        created_queries = Query.objects.filter(name=q.name)
        self.assertGreater(len(created_queries), 0)
        self.assertFalse(created_queries[0].has_file_input())


    def test_query_created_with_current_user(self):
        response = create_query_from_example(self.client)

        query = Query()
        query.set_example_data()

        created_queries = Query.objects.filter(name=query.name)
        self.assertGreater(len(created_queries), 0)

        q1 = created_queries[0]
        self.assertEqual(q1.user, User.objects.get(username=self.username))

    def test_query_bad_parameters_not_created(self):
        example_query = Query()
        example_query.set_example_data()
        example_query.query = ''
        custom_form = CustomQueryModelForm(prefix='query', instance=example_query)

        context = flatten_to_dict(custom_form)

        context['submit_parameters'] = ''
        response = self.client.post(reverse('gmql:custom'), context)
        self.assertContains(response, 'This field is required')

    def test_query_no_parameters_not_created(self):
        example_query = Query()
        example_query.set_example_data()
        example_query.query = '123'
        custom_form = CustomQueryModelForm(prefix='query', instance=example_query)

        context = flatten_to_dict(custom_form)

        context['submit_parameters'] = ''
        response = self.client.post(reverse('gmql:custom'), context)
        self.assertContains(response, 'The query has no parameters')


class QueryListTests(LoginedClientTestCase):
    def test_no_queries(self):
        response = self.client.get(reverse('gmql:queries'))
        self.assertContains(response, 'No queries')

    def test_queries_sorted(self):
        q1 = Query()
        q1.set_example_data()
        q1.save()

        q2 = Query()
        q2.set_example_data()
        q2.save()

        response = self.client.get(reverse('gmql:queries'))
        mostly_used_query_list = response.context['mostly_used_query_list']
        self.assertIn(q1, mostly_used_query_list)
        self.assertIn(q2, mostly_used_query_list)

        bigger = q1
        smaller = q2
        if list(mostly_used_query_list).index(q1) < list(mostly_used_query_list).index(q2):
            bigger = q2
            smaller = q1

        bigger.increase_used()
        bigger.increase_used()
        bigger.increase_used()

        response = self.client.get(reverse('gmql:queries'))
        mostly_used_query_list = response.context['mostly_used_query_list']
        self.assertIn(smaller, mostly_used_query_list)
        self.assertIn(bigger, mostly_used_query_list)
        smaller_idx = list(mostly_used_query_list).index(smaller)
        bigger_idx = list(mostly_used_query_list).index(bigger)

        self.assertGreater(smaller_idx, bigger_idx)

    def test_queries_updated(self):
        response = self.client.get(reverse('gmql:queries'))
        mostly_used_query_list = response.context['mostly_used_query_list']
        len_before = len(mostly_used_query_list)

        q1 = Query()
        q1.set_example_data()
        q1.save()

        response = self.client.get(reverse('gmql:queries'))
        mostly_used_query_list = response.context['mostly_used_query_list']
        len_after = len(mostly_used_query_list)
        self.assertIn(q1, mostly_used_query_list)
        self.assertGreater(len_after, len_before)

    def test_clients_see_own(self):
        q1 = Query()
        q1.user = User.objects.get(username=self.username)
        q1.set_example_data()
        q1.save()

        response = self.client.get(reverse('gmql:queries'))
        custom_query_list = response.context['custom_query_list']
        self.assertIn(q1, custom_query_list)


    def test_clients_dont_see_each_other_queries(self):
        if not User.objects.filter(username='test1').exists():
            User.objects.create_user(username='test1', email='test1@test.com', password='test1')

        q1 = Query()
        q1.user = User.objects.get(username='test1')
        q1.name = 'n1'
        q1.description = 'd1'
        q1.query = "q1"
        q1.save()

        response = self.client.get(reverse('gmql:queries'))
        custom_query_list = response.context['custom_query_list']
        self.assertNotIn(q1, custom_query_list)


    def test_superuser_see_all_custom_queries(self):
        if not User.objects.filter(username='test1').exists():
            User.objects.create_user(username='test1', email='test1@test.com', password='test1')

        q1 = Query()
        q1.user = User.objects.get(username='test1')
        q1.name = 'n1'
        q1.description = 'd1'
        q1.query = "q1"
        q1.save()

        q2 = Query()
        q2.user = User.objects.get(username=self.username)
        q2.set_example_data()
        q2.save()

        if not User.objects.filter(username='super1').exists():
            User.objects.create_superuser(username='super1', email='super11@test.com', password='super1')
        self.client.login(username='super1', password='super1')

        response = self.client.get(reverse('gmql:queries'))
        custom_query_list = response.context['custom_query_list']
        self.assertIn(q1, custom_query_list)
        self.assertIn(q2, custom_query_list)


    def test_all_see_superuser_queries(self):
        if not User.objects.filter(username='super1').exists():
            User.objects.create_superuser(username='super1', email='super11@test.com', password='super1')
        self.client.login(username='super1', password='super1')

        q1 = Query()
        q1.user = User.objects.get(username='super1')
        q1.name = 'n1'
        q1.description = 'd1'
        q1.query = "q1"
        q1.save()

        response = self.client.get(reverse('gmql:queries'))
        mostly_used_query_list = response.context['mostly_used_query_list']
        custom_query_list = response.context['custom_query_list']
        self.assertIn(q1, mostly_used_query_list)
        self.assertNotIn(q1, custom_query_list)

        if not User.objects.filter(username='test1').exists():
            User.objects.create_user(username='test1', email='test1@test.com', password='test1')
        self.client.login(username='test1', password='test1')

        response = self.client.get(reverse('gmql:queries'))
        mostly_used_query_list = response.context['mostly_used_query_list']
        custom_query_list = response.context['custom_query_list']
        self.assertIn(q1, mostly_used_query_list)
        self.assertNotIn(q1, custom_query_list)


class PagesAccessibleTests(LoginedClientTestCase):
    def test_about_accessible(self):
        response = self.client.get(reverse('gmql:about'))
        self.assertContains(response, 'GMQL Service')

    def test_query_accessible(self):
        q1 = Query()
        q1.set_example_data()
        q1.save()
        response = self.client.get(reverse('gmql:query', args=(q1.id, )))
        self.assertContains(response, 'Description')


class QueriesPerformedTests(LoginedClientTestCase):
    def test_query_is_created(self):
        response = create_query_from_example(self.client)
        q1 = Query()
        q1.set_example_data()
        query = Query.objects.filter(name=q1.name)[0]
        response = self.client.get(reverse('gmql:query', args=(query.id, )))


class FormatGuessTests(TestCase):
    def test_is_bed(self):
        file_path = test_file_path("input/a.bed")
        guess = FormatGuess.FormatGuesser(open(file_path, "r"))
        self.assertEqual(guess.get_format(), "bed")

    def test_is_bed_1(self):
        file_path = test_file_path("input/valid.bed")
        guess = FormatGuess.FormatGuesser(open(file_path, "r"))
        self.assertEqual(guess.get_format(), "bed")


    def test_is_vcf(self):
        file_path = test_file_path("input/valid.vcf")
        guess = FormatGuess.FormatGuesser(open(file_path, "r"))
        self.assertEqual(guess.get_format(), "vcf")

    def test_is_vcf_1(self):
        file_path = test_file_path("input/valid1.vcf")
        guess = FormatGuess.FormatGuesser(open(file_path, "r"))
        self.assertEqual(guess.get_format(), "vcf")

    def test_is_not_vcf(self):
        file_path = test_file_path("input/a.bed")
        guess = FormatGuess.FormatGuesser(open(file_path, "r"))
        self.assertNotEqual(guess.get_format(), "vcf")

    def test_is_not_vcf_1(self):
        file_path = test_file_path("input/invalid.vcf")
        guess = FormatGuess.FormatGuesser(open(file_path, "r"))
        self.assertNotEqual(guess.get_format(), "vcf")

    def test_is_not_vcf_2(self):
        file_path = test_file_path("input/invalid1.vcf")
        guess = FormatGuess.FormatGuesser(open(file_path, "r"))
        self.assertNotEqual(guess.get_format(), "vcf")

    def test_is_not_bed_but_default(self):
        file_path = test_file_path("input/empty.bed")
        guess = FormatGuess.FormatGuesser(open(file_path, "r"))
        self.assertEqual(guess.get_format(), settings.DEFAULT_FORMAT)

    def test_is_not_bed_but_default_1(self):
        file_path = test_file_path("input/invalid.bed")
        guess = FormatGuess.FormatGuesser(open(file_path, "r"))
        self.assertEqual(guess.get_format(), settings.DEFAULT_FORMAT)




@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class AcceessTests(LoginedClientTestCase):
    def test_user_sees_his_queries(self):
        q1 = Query()
        q1.set_example_data()
        q1.user = self.get_user()
        q1.save()
        response = self.client.get(reverse('gmql:query', args=(q1.id, )))
        self.assertContains(response, 'Description')

    def test_user_sees_his_files(self):
        q = Query(name="q", description="d", query="v1={p1};v2={p2};")
        q.user = self.get_user()
        q.save()

        parameters = {'p1': '1', 'p2': '2'}

        task = Task(query_id=q.pk)
        task.user = self.get_user()
        task.set_parameters(parameters)
        task.status = Task.ERROR
        task.save()

        file_path = test_file_path("input/a.bed")
        file = open(file_path)
        djangofile = File(file)
        gmql_file = GMQLFile(task=task, file_type=GMQLFile.DOWNLOAD)
        gmql_file.save()
        gmql_file.file.save(file_path.split('/')[-1], djangofile)

        response = self.client.get(reverse('gmql:file_download', args=[task.pk, gmql_file.pk]))
        self.assertEqual(response.status_code, 200)

        gmql_file.delete()


    def test_user_sees_his_tasks(self):
        q = Query(name="q", description="d", query="p={v};")
        q.user = self.get_user()
        q.save()

        parameters = {'p1': '1', 'p2': '2'}

        task = Task(query_id=q.pk)
        task.user = self.get_user()
        task.set_parameters(parameters)
        task.save()

        response = self.client.get(reverse('gmql:task', args=[task.pk]))
        self.assertContains(response, "Running")
        self.assertContains(response, "p={v};")


    def test_admin_sees_all_queries(self):
        superuser = User.objects.create_superuser(username='test1', email='test1@test.com', password='test1')
        self.client.login(username='test1', password='test1')

        q1 = Query()
        q1.set_example_data()
        q1.user = self.get_user()
        q1.save()
        response = self.client.get(reverse('gmql:query', args=(q1.id, )))
        self.assertContains(response, 'Description')

    def test_user_sees_admin_queries(self):
        superuser = User.objects.create_superuser(username='test1', email='test1@test.com', password='test1')
        self.client.login(username='test1', password='test1')

        q1 = Query()
        q1.set_example_data()
        q1.user = self.get_user()
        q1.save()

        self.client.login(username='test', password='test')

        response = self.client.get(reverse('gmql:query', args=(q1.id, )))
        self.assertContains(response, 'Description')



    def test_admin_sees_all_files(self):
        superuser = User.objects.create_superuser(username='test1', email='test1@test.com', password='test1')
        self.client.login(username='test1', password='test1')

        q = Query(name="q", description="d", query="v1={p1};v2={p2};")
        q.user = self.get_user()
        q.save()

        parameters = {'p1': '1', 'p2': '2'}

        task = Task(query_id=q.pk)
        task.user = self.get_user()
        task.set_parameters(parameters)
        task.status = Task.ERROR
        task.save()

        file_path = test_file_path("input/a.bed")
        file = open(file_path)
        djangofile = File(file)
        gmql_file = GMQLFile(task=task, file_type=GMQLFile.DOWNLOAD)
        gmql_file.save()
        gmql_file.file.save(file_path.split('/')[-1], djangofile)

        response = self.client.get(reverse('gmql:file_download', args=[task.pk, gmql_file.pk]))
        self.assertEqual(response.status_code, 200)

        gmql_file.delete()


    def test_admin_sees_all_tasks(self):
        superuser = User.objects.create_superuser(username='test1', email='test1@test.com', password='test1')
        self.client.login(username='test1', password='test1')

        q = Query(name="q", description="d", query="p={v};")
        q.user = self.get_user()
        q.save()

        parameters = {'p1': '1', 'p2': '2'}

        task = Task(query_id=q.pk)
        task.user = self.get_user()
        task.set_parameters(parameters)
        task.save()

        response = self.client.get(reverse('gmql:task', args=[task.pk]))
        self.assertContains(response, "Running")
        self.assertContains(response, "p={v};")

    def test_user_doesnot_see_other_queries(self):
        user1 = User.objects.create_user(username='test1', email='test1@test.com', password='test1')
        self.client.login(username='test1', password='test1')

        q1 = Query()
        q1.set_example_data()
        q1.user = self.get_user()
        q1.save()
        response = self.client.get(reverse('gmql:query', args=(q1.id, )))
        self.assertEqual(response.status_code, 404)


    def test_user_doesnot_see_other_files(self):
        user1 = User.objects.create_user(username='test1', email='test1@test.com', password='test1')
        self.client.login(username='test1', password='test1')

        q = Query(name="q", description="d", query="v1={p1};v2={p2};")
        q.user = self.get_user()
        q.save()

        parameters = {'p1': '1', 'p2': '2'}

        task = Task(query_id=q.pk)
        task.user = self.get_user()
        task.set_parameters(parameters)
        task.status = Task.ERROR
        task.save()

        file_path = test_file_path("input/a.bed")
        file = open(file_path)
        djangofile = File(file)
        gmql_file = GMQLFile(task=task, file_type=GMQLFile.DOWNLOAD)
        gmql_file.save()
        gmql_file.file.save(file_path.split('/')[-1], djangofile)

        response = self.client.get(reverse('gmql:file_download', args=[task.pk, gmql_file.pk]))
        self.assertEqual(response.status_code, 404)

        gmql_file.delete()


    def test_user_doesnot_see_other_tasks(self):
        user1 = User.objects.create_user(username='test1', email='test1@test.com', password='test1')
        self.client.login(username='test1', password='test1')

        q = Query(name="q", description="d", query="p={v};")
        q.user = self.get_user()
        q.save()

        parameters = {'p1': '1', 'p2': '2'}

        task = Task(query_id=q.pk)
        task.user = self.get_user()
        task.set_parameters(parameters)
        task.save()

        response = self.client.get(reverse('gmql:task', args=[task.pk]))
        self.assertEqual(response.status_code, 404)



@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class FileUploadingTests(TestCase):
    def setUp(self):
        q1 = Query()
        q1.set_example_data()
        q1.save()
        task = Task(query_id=q1.pk)
        task.save()

        file_path = test_file_path("input/a.bed")
        file = open(file_path)
        djangofile = File(file)
        self.gmql_file = GMQLFile(task=task, file_type=GMQLFile.UPLOAD)
        self.gmql_file.save()
        self.gmql_file.file.save(file_path.split('/')[-1], djangofile)
        self.gmql_file.write_metadata_file()


    def test_uploaded(self):
        original_content = open(test_file_path("input/a.bed")).read(4096)
        saved_content = self.gmql_file.file.read(4096)

        self.assertEqual(original_content, saved_content)
        self.assertTrue(os.path.exists(self.gmql_file.meta_file.path))

    def tearDown(self):
        self.gmql_file.delete()


@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class FileDownloadingTests(TestCase):
    def setUp(self):
        q1 = Query()
        q1.set_example_data()
        q1.save()
        task = Task(query_id=q1.pk)
        task.save()

        file_path = test_file_path("input/a.bed")
        file = open(file_path)
        djangofile = File(file)
        self.gmql_file = GMQLFile(task=task, file_type=GMQLFile.DOWNLOAD)
        self.gmql_file.save()
        self.gmql_file.file.save(file_path.split('/')[-1], djangofile)
        self.gmql_file.write_metadata_file()


    def test_uploaded(self):
        original_content = open(test_file_path("input/a.bed")).read(4096)
        saved_content = self.gmql_file.file.read(4096)

        self.assertEqual(original_content, saved_content)
        self.assertTrue(os.path.exists(self.gmql_file.meta_file.path))

    def tearDown(self):
        self.gmql_file.delete()


class PrintTestForCoverage(TestCase):
    def test_prints(self):
        q1 = Query()
        q1.set_example_data()
        q1.save()

        parameters = gmql_parser.parse_gqml_parameters(q1.query)
        self.assertTrue(len(parameters) > 0)
        p1 = Parameter(query=q1, name=parameters[0], description="dd")
        p1.save()

        task = Task(query_id=q1.pk)
        task.save()

        file_path = test_file_path("input/a.bed")
        file = open(file_path)
        djangofile = File(file)
        gmql_file = GMQLFile(task=task, file_type=GMQLFile.UPLOAD)
        gmql_file.save()
        gmql_file.file.save(file_path.split('/')[-1], djangofile)

        print q1
        print p1
        print task
        print gmql_file

        gmql_file.delete()


class TaskMethodsTests(TestCase):
    def test_getname(self):
        q1 = Query()
        q1.set_example_data()
        q1.save()

        task = Task(query_id=q1.pk)
        task.save()

        self.assertEqual(task.get_name(), str(task.pk))

    def test_parametersdict(self):
        parameters = {'p1': '1', 'p2': '2'}

        task = Task(query_id=1)
        task.set_parameters(parameters)
        task_parameters = task.get_parameters()

        self.assertEqual(parameters, task_parameters)


    def test_get_script(self):
        script = "a={v1},b={v2}"
        parameters = {"v1": 'a', "v2": "b"}
        expected_script = script.format(**parameters)
        q1 = Query()
        q1.name = 'n'
        q1.description = 'd'
        q1.query = script
        q1.save()

        task = Task(query_id=q1.pk)
        task.set_parameters(parameters)
        task.save()

        self.assertEqual(expected_script, task.get_script_with_parameters())


def data_with_prefix(parameters):
    res = {}
    if not isinstance(parameters, list):
        parameters = [parameters]
    for p in parameters:
        prefix = p["name"]
        res_item = dict((prefix + "-" + k, v) for (k, v) in p.items())
        res.update(res_item)
    return res


@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class ParametersFormsTests(TestCase):
    def test_text_value(self):
        parameters = [{"name": "n", "description": "d", "parameter_type": Parameter.TEXT, "value": "v"}]
        expected_parameters = {'n': 'v'}
        data = data_with_prefix(parameters)
        forms = generate_forms_list(parameters, data)
        for form in forms:
            self.assertTrue(form.is_valid())
            self.assertEqual(form.get_parameters_dict(parameters), expected_parameters)

    def test_number_value(self):
        parameters = [{"name": "n", "description": "d", "parameter_type": Parameter.TEXT, "value": '1.5'}]
        expected_parameters = {'n': '1.5'}
        data = data_with_prefix(parameters)
        forms = generate_forms_list(parameters, data)
        for form in forms:
            self.assertTrue(form.is_valid())
            self.assertEqual(form.get_parameters_dict(parameters), expected_parameters)

    def test_multiple_parameters(self):
        parameters = [{"name": "n", "description": "d", "parameter_type": Parameter.TEXT, "value": '1.5'},
                      {"name": "n1", "description": "d1", "parameter_type": Parameter.TEXT, "value": '2'}]
        expected_parameters = {'n': '1.5', 'n1': '2'}
        data = data_with_prefix(parameters)
        forms = generate_forms_list(parameters, data)
        generated_data = {}
        for form in forms:
            self.assertTrue(form.is_valid())
            generated_data.update(form.get_parameters_dict(parameters))
        self.assertEqual(generated_data, expected_parameters)


    def test_file_value(self):
        parameters = [{"name": "n", "description": "d", "parameter_type": Parameter.FILE, "value": "f"}]
        expected_parameters = {"n": "a.bed"}
        data = data_with_prefix(parameters)
        upload_file = open(test_file_path('input/a.bed'), 'rb')
        files = {'n-value': SimpleUploadedFile(upload_file.name, upload_file.read())}
        forms = generate_forms_list(parameters, data, files)
        generated_data = {}
        for form in forms:
            self.assertTrue(form.is_valid())
            generated_data.update(form.get_parameters_dict(parameters))
        self.assertEqual(generated_data, expected_parameters)

    def test_multiple_files_value(self):
        parameters = [{"name": "n", "description": "d", "parameter_type": Parameter.FILE, "value": "f"}]
        expected_parameters = {"n": "a.bed,b.bed"}
        data = data_with_prefix(parameters)
        upload_file = open(test_file_path('input/a.bed'), 'rb')
        upload_file1 = open(test_file_path('input/b.bed'), 'rb')
        files = MultiValueDict({'n-value': [SimpleUploadedFile(upload_file.name, upload_file.read()),
                                            SimpleUploadedFile(upload_file1.name, upload_file1.read())]})
        forms = generate_forms_list(parameters, data, files)
        generated_data = {}
        for form in forms:
            self.assertTrue(form.is_valid())
            generated_data.update(form.get_parameters_dict(parameters))
        self.assertEqual(generated_data, expected_parameters)


    def test_multiple_files_values(self):
        parameters = [{"name": "n", "description": "d", "parameter_type": Parameter.FILE, "value": "f"},
                      {"name": "n1", "description": "d1", "parameter_type": Parameter.TEXT, "value": '1'}]
        expected_parameters = {"n": "a.bed,b.bed", "n1": '1'}
        data = data_with_prefix(parameters)
        upload_file = open(test_file_path('input/a.bed'), 'rb')
        upload_file1 = open(test_file_path('input/b.bed'), 'rb')
        files = MultiValueDict({'n-value': [SimpleUploadedFile(upload_file.name, upload_file.read()),
                                            SimpleUploadedFile(upload_file1.name, upload_file1.read())]})
        forms = generate_forms_list(parameters, data, files)
        generated_data = {}
        for form in forms:
            self.assertTrue(form.is_valid())
            generated_data.update(form.get_parameters_dict(parameters))
        self.assertEqual(generated_data, expected_parameters)


@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class QueryFormsTests(LoginedClientTestCase):
    def test_invalid_without_data(self):
        parameters = {"name": "n", "description": "d", "parameter_type": Parameter.TEXT}
        q = Query(name="q", description="d", query="p={v};")
        q.save()
        p = Parameter(query=q, name=parameters["name"], description=parameters["description"],
                      parameter_type=parameters["parameter_type"])
        p.save()

        context = data_with_prefix(parameters)
        context['run'] = 'run'
        response = self.client.post(reverse('gmql:query', args=[q.pk]), context)
        self.assertContains(response, "This field is required")

    def test_redirected(self):
        parameters = {"name": "n", "description": "d", "parameter_type": Parameter.TEXT, "value": "v"}
        q = Query(name="q", description="d", query="p={v};")
        q.save()
        p = Parameter(query=q, name=parameters["name"], description=parameters["description"],
                      parameter_type=parameters["parameter_type"])
        p.save()

        context = data_with_prefix(parameters)
        context['run'] = 'run'
        response = self.client.post(reverse('gmql:query', args=[q.pk]), context)
        taskset = Task.objects.filter(query=q.pk)
        self.assertTrue(len(taskset) > 0)
        taskid = taskset[0].pk
        self.assertRedirects(response, reverse("gmql:task", args=[taskid]), fetch_redirect_response=False)

    def test_deleted(self):
        parameters = {"name": "n", "description": "d", "parameter_type": Parameter.TEXT, "value": "v"}
        q = Query(name="q", description="d", query="p={v};")
        q.save()
        p = Parameter(query=q, name=parameters["name"], description=parameters["description"],
                      parameter_type=parameters["parameter_type"])
        p.save()

        context = data_with_prefix(parameters)
        response = self.client.get(reverse('gmql:delete_query', args=[q.pk]), context)
        queryset = Query.objects.filter(query=q.pk)
        self.assertTrue(len(queryset) == 0)
        self.assertRedirects(response, reverse("gmql:queries"), fetch_redirect_response=False)


@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class TaskViewTest(LoginedClientTestCase):
    def test_running_state(self):
        q = Query(name="q", description="d", query="p={v};")
        q.save()

        parameters = {'p1': '1', 'p2': '2'}

        task = Task(query_id=q.pk)
        task.set_parameters(parameters)
        task.save()

        response = self.client.get(reverse('gmql:task', args=[task.pk]))
        self.assertContains(response, "Running")
        self.assertContains(response, "p={v};")

    def test_parameters_substituted(self):
        q = Query(name="q", description="d", query="v1={p1};v2={p2};")
        q.save()

        parameters = {'p1': '1', 'p2': '2'}

        task = Task(query_id=q.pk)
        task.set_parameters(parameters)
        task.save()

        response = self.client.get(reverse('gmql:task', args=[task.pk]))
        self.assertContains(response, "v1=1;v2=2;")

    def test_state_changes(self):
        q = Query(name="q", description="d", query="v1={p1};v2={p2};")
        q.save()

        parameters = {'p1': '1', 'p2': '2'}

        task = Task(query_id=q.pk)
        task.set_parameters(parameters)
        task.status = Task.SUCCESS
        task.save()

        response = self.client.get(reverse('gmql:task', args=[task.pk]))
        self.assertContains(response, "Success")


    def test_has_files(self):
        q = Query(name="q", description="d", query="v1={p1};v2={p2};")
        q.save()

        parameters = {'p1': '1', 'p2': '2'}

        task = Task(query_id=q.pk)
        task.set_parameters(parameters)
        task.status = Task.SUCCESS
        task.save()

        file_path = test_file_path("input/a.bed")
        file = open(file_path)
        djangofile = File(file)
        gmql_file = GMQLFile(task=task, file_type=GMQLFile.DOWNLOAD)
        gmql_file.save()
        gmql_file.file.save(file_path.split('/')[-1], djangofile)

        response = self.client.get(reverse('gmql:task', args=[task.pk]))
        self.assertContains(response, "Success")
        #self.assertContains(response, "a.bed")

        gmql_file.delete()

    def test_has_no_files(self):
        q = Query(name="q", description="d", query="v1={p1};v2={p2};")
        q.save()

        parameters = {'p1': '1', 'p2': '2'}

        task = Task(query_id=q.pk)
        task.set_parameters(parameters)
        task.status = Task.ERROR
        task.save()

        file_path = test_file_path("input/a.bed")
        file = open(file_path)
        djangofile = File(file)
        gmql_file = GMQLFile(task=task, file_type=GMQLFile.DOWNLOAD)
        gmql_file.save()
        gmql_file.file.save(file_path.split('/')[-1], djangofile)

        response = self.client.get(reverse('gmql:task', args=[task.pk]))
        self.assertContains(response, "Error")
        self.assertNotContains(response, "a.bed")

        gmql_file.delete()

    def test_download_link(self):
        q = Query(name="q", description="d", query="v1={p1};v2={p2};")
        q.save()

        parameters = {'p1': '1', 'p2': '2'}

        task = Task(query_id=q.pk)
        task.set_parameters(parameters)
        task.status = Task.ERROR
        task.save()

        file_path = test_file_path("input/a.bed")
        file = open(file_path)
        djangofile = File(file)
        gmql_file = GMQLFile(task=task, file_type=GMQLFile.DOWNLOAD)
        gmql_file.save()
        gmql_file.file.save(file_path.split('/')[-1], djangofile)

        response = self.client.get(reverse('gmql:file_download', args=[task.pk, gmql_file.pk]))
        self.assertEqual(response.status_code, 200)

        gmql_file.delete()


@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class GMQLRequesterTests(TestCase):
    def test_if_gmql_available(self):
        try:
            res = GMQLRequester.is_gmql_availalbe()
            self.assertTrue(res)
        except Exception as er:
            self.fail(er)


    def test_if_dataset_uploadable(self):
        requester = GMQLRequester()
        try:
            requester.upload_dataset([test_file_path('input/1.bed'), test_file_path("input/1.bed.meta")],
                                     "test_dsname1")
            requester.upload_dataset([test_file_path('input/1.bed'), test_file_path("input/1.bed.meta")],
                                     "test_dsname1")
            requester.clear()
        except Exception as er:
            self.fail(er)

    def test_send_query(self):
        requester = GMQLRequester()
        try:
            requester.run_query("qq", "tes_query")
            requester.clear()
        except Exception as er:
            self.fail(er)

    def test_download_if_no_result(self):
        requester = GMQLRequester()
        try:
            self.assertEqual([], requester.download_result(test_file_path('tmp')))
        except Exception as er:
            self.fail(er)


@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class GMQLRequesterDownloadableTests(TestCase):
    def setUp(self):
        self.requester = GMQLRequester()

        q1 = Query()
        q1.set_example_data()
        q1.save()
        self.task = Task(query_id=q1.pk)
        self.task.save()


    def test_download_result(self):
        try:
            self.requester.upload_dataset([test_file_path('input/1.bed'), test_file_path("input/1.bed.meta")],
                                          "test_dsname2")
            script = "GENE = SELECT(annotation_type == 'gene') test_dsname2;MATERIALIZE GENE;"
            self.requester.run_query(script, "query_name2")
            self.requester.wait_finished(20)

            downloaded_paths = self.requester.download_result(os.path.join(test_file_path('tmp'), "test_download"))
            self.assertNotEqual([], downloaded_paths)
            for f in downloaded_paths:
                gmql_file = GMQLFile(task=self.task, file_type=GMQLFile.DOWNLOAD)
                gmql_file.file.name = f
                gmql_file.save()
                self.assertTrue(os.path.exists(gmql_file.file.path))

        except Exception as er:
            self.fail(er)

    def tearDown(self):
        all_files = GMQLFile.objects.filter(task=self.task)
        for file in all_files:
            file.delete()


@override_settings(CELERY_ALWAYS_EAGER=True)
@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class GMQLTaskTest(TestCase):
    def setUp(self):
        self.requester = GMQLRequester()

        q1 = Query(name="task_query", description="description",
                   query="GENE = SELECT(*) {test_dsname3};MATERIALIZE GENE;")
        q1.save()
        parameters = {"test_dsname3": "file"}
        self.task = Task(query_id=q1.pk)
        self.task.set_parameters(parameters)
        self.task.save()

    def test_run_task_and_download_result(self):
        try:
            upload_file = open(test_file_path('input/1.bed'), 'rb')
            files = MultiValueDict({'test_dsname3-value': [SimpleUploadedFile(upload_file.name, upload_file.read())]})
            result = process_task.delay(self.task, files)
            self.assertEqual(self.task.status, Task.SUCCESS)

            files = GMQLFile.objects.filter(task=self.task, file_type=GMQLFile.DOWNLOAD)
            self.assertTrue(len(files) > 0)
            for file in files:
                self.assertTrue(os.path.exists(file.file.path))

        except Exception as er:
            self.fail(er)


    def tearDown(self):
        all_files = GMQLFile.objects.filter(task=self.task)
        for file in all_files:
            file.delete()
        self.requester.clear()


@override_settings(MAX_TASK_STORAGE_TIME_DAYS=1)
@override_settings(CELERY_ALWAYS_EAGER=True)
@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class GMQLTaskCleanerTest(TestCase):
    def setUp(self):
        q = Query(name="q", description="d", query="v1={p1};v2={p2};")
        q.save()

        parameters = {'p1': '1', 'p2': '2'}

        self.task = Task(query_id=q.pk)
        self.task.set_parameters(parameters)
        self.task.status = Task.ERROR

        self.task.save()

        file_path = test_file_path("input/a.bed")
        file = open(file_path)
        djangofile = File(file)
        gmql_file = GMQLFile(task=self.task, file_type=GMQLFile.DOWNLOAD)
        gmql_file.save()
        gmql_file.file.save(file_path.split('/')[-1], djangofile)

    def test_tasks_are_not_cleaned(self):
        clean_task()
        self.assertTrue(len(list(Task.objects.all())) != 0)


    def test_tasks_are_cleaned_greater_than_now(self):
        self.task.date = self.task.date + timedelta(days=10)
        self.task.save()
        clean_task()
        self.assertTrue(len(list(Task.objects.all())) == 0)


    def test_tasks_are_cleaned_expired(self):
        self.task.date = datetime.now() + timedelta(days=1)
        self.task.save()
        clean_task()
        self.assertTrue(len(list(Task.objects.all())) == 0)

    def test_history_record_is_saved(self):
        self.task.date = datetime.now() + timedelta(days=1)
        self.task.save()
        task_query = '%s' % self.task.query
        user = self.task.user
        clean_task()
        self.assertTrue(len(list(Task.objects.all())) == 0)

        history = TaskHistoryRecord.objects.get(user=user)
        self.assertIn(task_query, str(history))


class SearchTest(TestCase):
    def test_tags_are_found(self):
        query = "map select encode"
        res = Tags.find_all_tags(query)
        self.assertEqual(res, ['select', 'map', 'ENCODE'])


    def test_tags_query_has_tags(self):
        q1 = Query()
        q1.set_example_data()
        q1.save()

        self.assertTrue(len(Query.objects.tags(['select', 'one_more_tag'])) > 0)
        self.assertTrue(len(Query.objects.tags(['first_tag', 'one_more_tag'])) == 0)


    def test_search(self):
        q1 = Query()
        q1.set_example_data()
        q1.save()

        self.assertTrue(len(Query.objects.search('select')) > 0)
        self.assertTrue(len(Query.objects.search('asdfasdfsdaf')) == 0)


    def test_search_number_terms(self):
        q1 = Query()
        q1.set_example_data()
        q1.save()

        self.assertTrue(len(Query.objects.search('encode asdfdasf')) > 0)

    def test_search_empty(self):
        q1 = Query()
        q1.set_example_data()
        q1.save()

        self.assertTrue(len(Query.objects.search('')) > 0)


class SpecialMetaTest(TestCase):
    def test_encode_parsed(self):
        res = encode_meta.has_term('antibody')
        self.assertTrue(res)


    def test_encode_has_values(self):
        res = encode_meta.values('antibody')
        self.assertIn('SRF', res)


    def test_encode_has_no_values(self):
        res = encode_meta.values('asdfasdfdsaf')
        self.assertFalse(res)


    def test_meta_has_values(self):
        res = SpecialMeta.special_values('antibody')
        self.assertIn('SRF', res)



class TestObserver:
    def __init__(self):
        self.open = []
        self.close = []
        self.data = []

    def file_open(self, file_name, meta_dict):
        self.open.append((file_name, meta_dict))

    def file_close(self, file_name):
        self.close.append(file_name)

    def data_line(self, line):
        self.data.append(line)

    def archive_open(self, archive_name):
        pass


    def archive_close(self, archive_name):
        pass

    def finish(self):
        pass


@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class GMQLArchiveTest(TestCase):
    def test_open_nonarchive(self):
        path = test_file_path("input/a.bed")
        archive = GMQLArchive(path=path)
        self.assertTrue(archive.has_error())

    def test_context_manager(self):
        path = test_file_path("input/a.bed")
        with GMQLArchive(path=path) as archive:
            self.assertTrue(archive.has_error())


    def test_open_archive(self):
        path = test_file_path("input/job_11_tomcat_20141202_144151_joinResult.zip")
        with GMQLArchive(path=path) as archive:
            self.assertFalse(archive.has_error())

    def test_process_archive(self):
        path = test_file_path("input/job_11_tomcat_20141202_144151_joinResult.zip")
        with GMQLArchive(path=path) as archive:
            test_observer = TestObserver()
            archive.register_observer(test_observer)
            self.assertFalse(archive.has_error())

            archive.process()

            self.assertTrue(len(test_observer.open) != 0)
            self.assertTrue(len(test_observer.open) == len(test_observer.close))
            self.assertTrue(len(test_observer.data) != 0)

    def test_process_igb(self):
        path = test_file_path("input/job_11_tomcat_20141202_144151_joinResult.zip")
        with GMQLArchive(path=path) as archive:
            outpath = test_file_path("output/archive_test/")
            archive.register_observer(GMQLArchiveIGBWriter(outpath))
            self.assertFalse(archive.has_error())

            archive.process()
            self.assertTrue(len(archive.outputs()) != 0)


    def test_write_result(self):
        path = test_file_path("input/job_11_tomcat_20141202_144151_joinResult.zip")
        with GMQLArchive(path=path) as archive:
            outpath = test_file_path("output/archive_test/")
            writer_observer = GMQLArchiveWriter(outpath)
            archive.register_observer(writer_observer)
            self.assertFalse(archive.has_error())

            archive.process()

            self.assertTrue(len(writer_observer.out_archives) != 0)

    def test_write_BED_result(self):
        path = test_file_path("input/job_11_tomcat_20141202_144151_joinResult.zip")
        with GMQLArchive(path=path) as archive:
            outpath = test_file_path("output/archive_test/")
            writer_observer = GMQLArchiveBEDWriter(outpath)
            archive.register_observer(writer_observer)
            self.assertFalse(archive.has_error())

            archive.process()

            self.assertTrue(len(writer_observer.out_archives) != 0)


    def test_write_CSV_result(self):
        path = test_file_path("input/job_11_tomcat_20141202_144151_joinResult.zip")
        with GMQLArchive(path=path) as archive:
            outpath = test_file_path("output/archive_test/")
            writer_observer = GMQLArchiveCSVWriter(outpath)
            archive.register_observer(writer_observer)
            self.assertFalse(archive.has_error())

            archive.process()

            self.assertTrue(len(writer_observer.out_archives) != 0)

    def test_convertor_task_function(self):
        path = test_file_path("input/job_11_tomcat_20141202_144151_joinResult.zip")
        outpath = test_file_path("output/archive_test/")
        res = process_file(path, outpath, 0, 'query')
        self.assertTrue(len(res) != 0)


    def tearDown(self):
        path = test_file_path("output/archive_test/")
        if os.path.exists(path):
            shutil.rmtree(path)


def create_genome_space(user):
    q = Query(name="q", description="d", query="map={p1};v2={p2};")
    q.user = user
    q.save()

    parameters = {'p1': '1', 'p2': '2'}

    task = Task(query_id=q.pk)
    task.user = user
    task.set_parameters(parameters)
    task.status = Task.ERROR
    task.save()

    space = GenomeSpace()
    sample = GenomeSpaceSample()
    sample2 = GenomeSpaceSample()
    sample.meta = {'meta1':'meta2'}
    sample2.meta = {'meta3':'meta4'}
    region1 = GenomeSpaceRegion('chr1', '2', '10', {'key' : 'value'})
    region2 = GenomeSpaceRegion('chr2', '10', '20', {'key1' : 'value2'})
    region3 = GenomeSpaceRegion('chr1', '2', '10', {'key' : 'value'})

    sample.add(region1)
    sample.add(region2)
    sample2.add(region3)

    space.insert('sample1', sample)
    space.insert('sample2', sample2)
    return space, task.pk, q.query


@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class GenomeSpaceTests(LoginedClientTestCase):
    def test_data_inserted(self):
        space = GenomeSpace()
        sample = GenomeSpaceSample()
        sample.meta = {'meta1':'meta2'}
        region1 = GenomeSpaceRegion('chr1', '2', '10', {'key' : 'value'})
        region11 = GenomeSpaceRegion('chr1', '1', '10', {'key' : 'value'})
        region2 = GenomeSpaceRegion('chr2', '10', '20', {'key1' : 'value2'})

        sample.add(region1)
        sample.add(region11)
        sample.add(region2)

        space.insert('sample1', sample)

        for s in space:
            self.assertEqual(space[s].meta, sample.meta)
            for r in space[s].regions(chr='chr1'):
                self.assertEqual(r.start, 1)
                break

            for r in space[s].regions():
                self.assertEqual(r.start, 10)
                break

    def test_basis_built(self):
        space = GenomeSpace()
        sample = GenomeSpaceSample()
        sample.meta = {'meta1':'meta2'}
        region1 = GenomeSpaceRegion('chr1', '1', '10', {'key' : 'value'})
        region11 = GenomeSpaceRegion('chr1', '1', '10', {'key' : 'value'})
        region111 = GenomeSpaceRegion('chr1', '1', '9', {'key' : 'value'})
        region1111 = GenomeSpaceRegion('chr1', '2', '10', {'key' : 'value'})
        region11111 = GenomeSpaceRegion('chr1', '2', '10', {'key' : 'value'})
        region2 = GenomeSpaceRegion('chr2', '10', '20', {'key1' : 'value2'})

        sample.add_unique(region1)
        sample.add_unique(region11)
        sample.add_unique(region111)
        sample.add_unique(region1111)
        sample.add_unique(region11111)
        sample.add_unique(region2)

        space.insert('sample1', sample)
        basis = space.basis_sample()

        counter = 0
        for r in basis.regions(chr='chr1'):
            counter += 1
        self.assertEqual(counter, 3)

        for r in basis.regions():
            self.assertEqual(r.start, 10)
            break


    def test_writer(self):
        path = test_file_path("input/job_11_tomcat_20141202_144151_joinResult.zip")
        with GMQLArchive(path=path) as archive:
            outpath = test_file_path("output/archive_test/")
            observer=GenomeSpaceWriter(outpath, 0, 'query')
            archive.register_observer(observer)
            self.assertFalse(archive.has_error())

            archive.process()

            self.assertEqual(len([s for s in observer.genome_spaces['job_11_tomcat_20141202_144151_joinResult.zip']]), 14)
            for r in observer.genome_spaces['job_11_tomcat_20141202_144151_joinResult.zip']['joinResult_-6357891149860971646.graph'].regions():
                self.assertEqual(r.start, 116384)
                self.assertEqual(r.stop, 116409)
                break


    def test_chromosome_calculator(self):
        space = GenomeSpace()
        sample = GenomeSpaceSample()
        sample2 = GenomeSpaceSample()
        sample.meta = {'meta1':'meta2'}
        region1 = GenomeSpaceRegion('chr1', '2', '10', {'key' : 'value'})
        region2 = GenomeSpaceRegion('chr2', '10', '20', {'key1' : 'value2'})
        region3 = GenomeSpaceRegion('chr1', '2', '10', {'key' : 'value'})

        sample.add(region1)
        sample.add(region2)
        sample2.add(region3)

        space.insert('sample1', sample)
        space.insert('sample2', sample2)
        out = {'series': [{'data': [1, 1], 'name': 'sample1'}, {'data': [1, 0], 'name': 'sample2'}], 'categories': ['chr1', 'chr2']}


        chart = ChartChromosomeDistribution()
        self.assertEqual(out,chart.run(space))


    def test_chromosome_calculator_sorting_by_chromosome(self):
        space = GenomeSpace()
        sample = GenomeSpaceSample()
        sample.meta = {'meta1':'meta2'}
        region1 = GenomeSpaceRegion('chr1', '2', '10', {'key' : 'value'})
        region2 = GenomeSpaceRegion('chr2', '10', '20', {'key1' : 'value2'})
        region3 = GenomeSpaceRegion('chr10', '2', '10', {'key' : 'value'})
        region4 = GenomeSpaceRegion('chrX', '2', '10', {'key' : 'value'})
        region5 = GenomeSpaceRegion('chrY', '2', '10', {'key' : 'value'})
        region6 = GenomeSpaceRegion('chrM', '2', '10', {'key' : 'value'})

        sample.add(region1)
        sample.add(region2)
        sample.add(region3)
        sample.add(region4)
        sample.add(region5)
        sample.add(region6)


        space.insert('sample1', sample)
        out = {'series': [{'data': [1, 1, 1, 1, 1, 1], 'name': 'sample1'}], 'categories': ['chr1', 'chr2', 'chr10', 'chrX', 'chrY', 'chrM']}


        chart = ChartChromosomeDistribution()
        actual_out = chart.run(space)
        self.assertEqual(out,actual_out)


    def test_create_chromosome_chart_data(self):
        space, taskpk, query = create_genome_space(self.get_user())
        out = {'series': [{'data': [1, 1], 'name': 'sample1'}, {'data': [1, 0], 'name': 'sample2'}], 'categories': ['chr1', 'chr2']}

        runner = ChartCalculatorRunner({'archive':space}, taskpk, query)
        runner.run()

        chart = ChartChromosomeDistribution()
        chart_object = Chart.objects.get(name=chart.name, archive_name='archive')
        self.assertEqual(out, chart_object.data)


    def test_create_heatmap_chart_data(self):
        space, taskpk, query = create_genome_space(self.get_user())
        out = {u'chr2': {u'series': [[0, 0, 1], [0, 1, 0]], u'ycategories': [u'sample1', u'sample2'], u'xcategories': [u'10-20']}, u'chr1': {u'series': [[0, 0, 1], [0, 1, 1]], u'ycategories': [u'sample1', u'sample2'], u'xcategories': [u'2-10']}}

        runner = ChartCalculatorRunner({'archive':space}, taskpk, query)
        runner.run()

        chart = ChartHeatmapMapOperation()
        chart_object = Chart.objects.get(name=chart.name, archive_name='archive')
        self.assertEqual(out, chart_object.data)


    def test_create_heatmap_chart_empty_data(self):
        space, taskpk, query = create_genome_space(self.get_user())
        space.name_sample = {}

        runner = ChartCalculatorRunner({'archive':space}, taskpk, query)
        runner.run()

        chart = ChartHeatmapMapOperation()
        self.assertEqual(0, Chart.objects.filter(name=chart.name, archive_name='archive').count())


    def test_meta_excluded(self):
        space, taskpk, query = create_genome_space(self.get_user())
        space['sample1'].meta['lab_description'] = 'aa'
        space['sample1'].meta['epi.lab_description'] = 'aa'
        out = {u'header': [u'sample1', u'sample2'], 'rows': [{u'data': [u'meta2', u'-'], u'name': u'meta1'}, {u'data': [u'-', u'meta4'], u'name': u'meta3'}]}

        runner = ChartCalculatorRunner({'archive':space}, taskpk, query)
        runner.run()

        chart = TableMetadataTerms()
        chart_object = Chart.objects.get(name=chart.name, archive_name='archive')
        self.assertEqual(out, chart_object.data)

"""
    def test_performance(self):
        space = GenomeSpace()
        sample = GenomeSpaceSample()
        startTime = time.time()
        d = {'key' : 'value'}
        for r in xrange(1000000):
            region1 = GenomeSpaceRegion('chr1', '2', '10', d)
            sample.add(region1)
        t = time.time() - startTime
        print "Insert time: %.3f" % t

        space.insert('sample1', sample)
        startTime = time.time()
        for s in space:
            for r in space[s].regions():
                pass
        t = time.time() - startTime
        print "Get time: %.3f" % t
"""


class TestTaskHistory(LoginedClientTestCase):
    def setUp(self):
        super(TestTaskHistory, self).setUp()
        q = Query(name="q", description="d", query="map={p1};v2={p2};")
        q.user = self.get_user()
        q.save()

        parameters = {'p1': '1', 'p2': '2'}

        task = Task(query_id=q.pk)
        task.user = self.get_user()
        task.set_parameters(parameters)
        task.status = Task.ERROR
        task.save()

        self.task = task

    def test_message_is_saved(self):
        history = TaskHistoryRecord(user=self.task.user)
        history.set_msg(self.task)
        history.save()
        self.assertIn('Date', repr(history))
        self.assertIn('Query', str(history))
        self.assertIn('Status', str(history))

    def test_has_error(self):
        history = TaskHistoryRecord(user=self.task.user)
        history.set_msg(self.task)
        history.save()
        self.assertIn('Error', repr(history))

@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class SelectionHintTests(LoginedClientTestCase):
    def test_statement_extracted(self):
        query = "GENES = SELECT( type == '{type}' ) MYANNOTATION; \n" \
                     "MAPPED = MAP(COUNT) GENES {MYEXPERIMENT}; \n" \
                     "MATERIALIZE MAPPED;"
        statements = SelectStatement.get_statements(query, {})
        self.assertEqual(len(statements), 1)
        self.assertEqual(statements[0].statement, "GENES = SELECT( type == '{type}' ) MYANNOTATION")

    def test_statement_extracted_difficult(self):
        query = "GENES = SELECT( type == '\n" \
                "{type}' ) MYANNOTATION; \n" \
                     "MAPPED = MAP(COUNT) GENES {MYEXPERIMENT}; \n" \
                     "MAPPED = SELECT(COUNT) GENES {MYEXPERIMENT}; \n" \
                     "MATERIALIZE MAPPED;"
        statements = SelectStatement.get_statements(query, {})
        self.assertEqual(len(statements), 1)
        self.assertEqual(statements[0].collection_name, 'MYANNOTATION')
        self.assertEqual(statements[0].statement, "GENES = SELECT( type == ' {type}' ) MYANNOTATION")


    def test_statement_not_extracted_difficult(self):
        query = "GENES = ELECT( type == '\n" \
                "{type}' ) MYANNOTATION; \n" \
                     "MAPPED = MAP(COUNT) GENES {MYEXPERIMENT}; \n" \
                     "MAPPED = SELECT(COUNT) GENES {MYEXPERIMENT}; \n" \
                     "MATERIALIZE MAPPED;"
        statements = SelectStatement.get_statements(query, {})
        self.assertEqual(len(statements), 0)


    def test_inner_statement_extracted(self):
        query = "GENES = SELECT( tt == '{tt}' AND a== 'b' or ( x =='y')) MYANNOTATION; \n" \

        statements = SelectStatement.get_statements(query, {'tt':'int'})
        self.assertEqual(len(statements), 1)
        self.assertEqual(statements[0].collection_name, 'MYANNOTATION')
        self.assertEqual(len(statements[0].terms), 3)
        self.assertEqual(statements[0].terms['tt'], ['int'])
        self.assertEqual(statements[0].terms['a'], ['b'])
        self.assertEqual(statements[0].terms['x'], ['y'])


    def test_terms_extracted(self):
        query = "GENES = SELECT( (tt=='{tt}') AND (a=='b' or " \
                "( x " \
                "=='y'))) MYANNOTATION; \n" \

        statements = SelectStatement.get_statements(query, {'tt':'int'})
        self.assertEqual(len(statements), 1)
        self.assertEqual(statements[0].collection_name, 'MYANNOTATION')
        self.assertEqual(len(statements[0].terms), 3)
        self.assertEqual(statements[0].terms['tt'], ['int'])
        self.assertEqual(statements[0].terms['a'], ['b'])
        self.assertEqual(statements[0].terms['x'], ['y'])

    def test_all_selection_skipped(self):
        query = "GENES = SELECT( *) MYANNOTATION; \n" \

        statements = SelectStatement.get_statements(query, {'tt':'int'})
        self.assertEqual(len(statements), 0)


    def test_terms_are_selected_1(self):
        query = "encode_broadpeaks=SELECT( dataType == 'ChipSeq' AND cell == '{cell}' AND antibody == '{antibody}' ) ENCODE_BROADPEAK_HG19;" \
                    "MATERIALIZE encode_broadpeaks;" \

        statements = SelectStatement.get_statements(query, {'cell':'K562', 'antibody':'CTCF'})
        self.assertEqual(len(statements), 1)
        self.assertEqual(statements[0].collection_name, 'ENCODE_BROADPEAK_HG19')
        self.assertEqual(len(statements[0].terms), 3)
        self.assertEqual(statements[0].terms['dataType'], ['ChipSeq'])
        self.assertEqual(statements[0].terms['cell'], ['K562'])
        self.assertEqual(statements[0].terms['antibody'], ['CTCF'])

    def test_terms_are_selected_2(self):
        query = "encode=SELECT( antibody == '{antibody}' AND cell_tissue == '{cell_tissue}' ) ENCODE_BROADPEAK_HG19;"\
                "dnaseseq=SELECT( ANATOMY == '{ANATOMY}' AND assay == 'DNase.hotspot.broad' ) EPIGENOME_BROADPEAK_HG19;"\
                "encode_in_dnaseseq=JOIN(distance < 0, right) dnaseseq encode;" \
                "MATERIALIZE encode_in_dnaseseq;" \


        statements = SelectStatement.get_statements(query, {'antibody':'CTCF', 'cell_tissue': 'breast', 'ANATOMY': 'BREAST'})
        self.assertEqual(len(statements), 2)

        self.assertEqual(statements[0].collection_name, 'ENCODE_BROADPEAK_HG19')
        self.assertEqual(len(statements[0].terms), 2)
        self.assertEqual(statements[0].terms['antibody'], ['CTCF'])
        self.assertEqual(statements[0].terms['cell_tissue'], ['breast'])
        self.assertEqual(len(statements[1].terms), 2)
        self.assertEqual(statements[1].terms['ANATOMY'], ['BREAST'])
        self.assertEqual(statements[1].terms['assay'], ['DNase.hotspot.broad'])


