from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.test.utils import override_settings
from django.utils.datastructures import MultiValueDict
from gmql.models import test_file_path

from gmql.tasks import *
from gmql.utils.SelectionHint import SelectStatement


@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class GMQLRequesterTests(TestCase):
    def test_if_gmql_available(self):
        try:
            res = GMQLRequester.is_gmql_availalbe()
            self.assertTrue(res)
        except Exception as er:
            self.fail(er)


    def test_if_dataset_uploadable(self):
        requester = GMQLRequester()
        try:
            requester.upload_dataset([test_file_path('input/1.bed'), test_file_path("input/1.bed.meta")],
                                     "test_dsname1")
            requester.upload_dataset([test_file_path('input/1.bed'), test_file_path("input/1.bed.meta")],
                                     "test_dsname1")
            requester.clear()
        except Exception as er:
            self.fail(er)

    def test_send_query(self):
        requester = GMQLRequester()
        try:
            requester.run_query("qq", "tes_query")
            requester.clear()
        except Exception as er:
            self.fail(er)

    def test_download_if_no_result(self):
        requester = GMQLRequester()
        try:
            self.assertEqual([], requester.download_result(test_file_path('tmp')))
        except Exception as er:
            self.fail(er)


@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class GMQLRequesterDownloadableTests(TestCase):
    def setUp(self):
        self.requester = GMQLRequester()

        q1 = Query()
        q1.set_example_data()
        q1.save()
        self.task = Task(query_id=q1.pk)
        self.task.save()


    def test_download_result(self):
        try:
            self.requester.upload_dataset([test_file_path('input/1.bed'), test_file_path("input/1.bed.meta")],
                                          "test_dsname2")
            script = "GENE = SELECT(annotation_type == 'gene') test_dsname2;MATERIALIZE GENE;"
            self.requester.run_query(script, "query_name2")
            self.requester.wait_finished(20)

            downloaded_paths = self.requester.download_result(os.path.join(test_file_path('tmp'), "test_download"))
            self.assertNotEqual([], downloaded_paths)
            for f in downloaded_paths:
                gmql_file = GMQLFile(task=self.task, file_type=GMQLFile.DOWNLOAD)
                gmql_file.file.name = f
                gmql_file.save()
                self.assertTrue(os.path.exists(gmql_file.file.path))

        except Exception as er:
            self.fail(er)

    def tearDown(self):
        all_files = GMQLFile.objects.filter(task=self.task)
        for file in all_files:
            file.delete()


@override_settings(CELERY_ALWAYS_EAGER=True)
@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class GMQLTaskTest(TestCase):
    def setUp(self):
        self.requester = GMQLRequester()

        q1 = Query(name="task_query", description="description",
                   query="GENE = SELECT(*) {test_dsname3};MATERIALIZE GENE;")
        q1.save()
        parameters = {"test_dsname3": "file"}
        self.task = Task(query_id=q1.pk)
        self.task.set_parameters(parameters)
        self.task.save()

    def test_run_task_and_download_result(self):
        try:
            upload_file = open(test_file_path('input/1.bed'), 'rb')
            files = MultiValueDict({'test_dsname3-value': [SimpleUploadedFile(upload_file.name, upload_file.read())]})
            result = process_task.delay(self.task, files)
            self.assertEqual(self.task.status, Task.SUCCESS)

            files = GMQLFile.objects.filter(task=self.task, file_type=GMQLFile.DOWNLOAD)
            self.assertTrue(len(files) > 0)
            for file in files:
                self.assertTrue(os.path.exists(file.file.path))

        except Exception as er:
            self.fail(er)



    def test_run_download_convert(self):
        try:
            upload_file = open(test_file_path('input/1.bed'), 'rb')
            files = MultiValueDict({'test_dsname3-value': [SimpleUploadedFile(upload_file.name, upload_file.read())]})
            result = process_task.delay(self.task, files)
            self.assertEqual(self.task.status, Task.SUCCESS)

            files = GMQLFile.objects.filter(task=self.task, file_type=GMQLFile.DOWNLOAD, label='BED')
            self.assertTrue(len(files) > 0)
            for file in files:
                self.assertTrue(os.path.exists(file.file.path))

        except Exception as er:
            self.fail(er)

    def tearDown(self):
        all_files = GMQLFile.objects.filter(task=self.task)
        for file in all_files:
            file.delete()
        self.requester.clear()


@override_settings(CELERY_ALWAYS_EAGER=True)
@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class GMQLTaskNoResiltTest(TestCase):
    def setUp(self):
        self.requester = GMQLRequester()

        q1 = Query(name="task_query", description="description",
                   query="GENE = SELECT(name=='aa') {test_dsname3};MATERIALIZE GENE;")
        q1.save()
        parameters = {"test_dsname3": "file"}
        self.task = Task(query_id=q1.pk)
        self.task.set_parameters(parameters)
        self.task.save()

    def test_run_task_with_no_result(self):
        try:
            upload_file = open(test_file_path('input/1.bed'), 'rb')
            files = MultiValueDict({'test_dsname3-value': [SimpleUploadedFile(upload_file.name, upload_file.read())]})
            result = process_task.delay(self.task, files)
            self.assertEqual(self.task.status, Task.SUCCESS)

            files = GMQLFile.objects.filter(task=self.task, file_type=GMQLFile.DOWNLOAD)
            self.assertTrue(len(files) == 0)

        except Exception as er:
            self.fail(er)

    def tearDown(self):
        all_files = GMQLFile.objects.filter(task=self.task)
        for file in all_files:
            file.delete()
        self.requester.clear()



@override_settings(CELERYD_TASK_SOFT_TIME_LIMIT=1)
@override_settings(CELERY_ALWAYS_EAGER=True)
@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class GMQLTaskTimeLimitTest(TestCase):
    def setUp(self):
        self.requester = GMQLRequester()

        q1 = Query(name="task_query", description="description",
                   query="GENE = SELECT(*) {test_dsname3};MATERIALIZE GENE;")
        q1.save()
        parameters = {"test_dsname3": "file"}
        self.task = Task(query_id=q1.pk)
        self.task.set_parameters(parameters)
        self.task.save()


    def test_run_time_limit_error(self):
        try:
            upload_file = open(test_file_path('input/1.bed'), 'rb')
            files = MultiValueDict({'test_dsname3-value': [SimpleUploadedFile(upload_file.name, upload_file.read())]})
            result = process_task.delay(self.task, files)
            self.assertEqual(self.task.status, Task.ERROR)

        except Exception as er:
            self.fail(er)


    def tearDown(self):
        all_files = GMQLFile.objects.filter(task=self.task)
        for file in all_files:
            file.delete()
        self.requester.clear()


@override_settings(MAX_PROCESS_FILE_SIZE=1)
@override_settings(CELERY_ALWAYS_EAGER=True)
@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class GMQLProcessFileSizeLimitTest(TestCase):
    def setUp(self):
        self.requester = GMQLRequester()

        q1 = Query(name="task_query", description="description",
                   query="GENE = SELECT(*) {test_dsname3};MATERIALIZE GENE;")
        q1.save()
        parameters = {"test_dsname3": "file"}
        self.task = Task(query_id=q1.pk)
        self.task.set_parameters(parameters)
        self.task.save()

    def test_run_download_convert(self):
        try:
            upload_file = open(test_file_path('input/1.bed'), 'rb')
            files = MultiValueDict({'test_dsname3-value': [SimpleUploadedFile(upload_file.name, upload_file.read())]})
            result = process_task.delay(self.task, files)
            self.assertEqual(self.task.status, Task.SUCCESS)

            files = GMQLFile.objects.filter(task=self.task, file_type=GMQLFile.DOWNLOAD, label='BED')
            self.assertTrue(len(files) == 0)
            for file in files:
                self.assertTrue(os.path.exists(file.file.path))

        except Exception as er:
            self.fail(er)

    def tearDown(self):
        all_files = GMQLFile.objects.filter(task=self.task)
        for file in all_files:
            file.delete()
        self.requester.clear()


@override_settings(CELERY_ALWAYS_EAGER=True)
@override_settings(MEDIA_ROOT=test_file_path('tmp'))
class GMQLMetadataTest(TestCase):
    def setUp(self):
        self.requester = GMQLRequester()

    def test_query_metadata(self):
        try:
            number = self.requester.filter_by_meta({'cell': ['K562']},'ENCODE_BROADPEAK_HG19')
            self.assertEqual(58, number)
        except Exception as er:
            self.fail(er)

    def test_query_metadata_1(self):
        try:
            number = self.requester.filter_by_meta({'antibody': ['CBP_(sc-369)']},'ENCODE_BROADPEAK_HG19')
            self.assertEqual(1, number)
        except Exception as er:
            self.fail(er)

    def test_query_metadata_2(self):
        try:
            number = self.requester.filter_by_meta({'antibody': ['CBX2'], 'cell': ['K562', '8988T']},'ENCODE_BROADPEAK_HG19')
            self.assertEqual(1, number)
        except Exception as er:
            self.fail(er)

    def test_query_metadata_none(self):
        try:
            number = self.requester.filter_by_meta({},'ENCODE_BROADPEAK_HG19')
            self.assertEqual(0, number)
        except Exception as er:
            self.fail(er)


    def test_query_metadata_none_1(self):
        try:
            number = self.requester.filter_by_meta({'asdfdsaf': ['asdfasdf']},'ENCODE_BROADPEAK_HG19')
            self.assertEqual(0, number)
        except Exception as er:
            self.fail(er)

    def test_query_metadata_none_2(self):
        try:
            number = self.requester.filter_by_meta({'antibody': ['CBX2'], 'cell': ['K562', '8988T']}, 'ENCODE_BROADPEAK_HG191')
            self.assertTrue(False)
        except Exception as er:
            self.assertTrue(True)

    def test_query_metadata_encode_1(self):
        try:
            number = self.requester.filter_by_meta({'dataType': ['ChipSeq'], 'cell': ['K562'], 'antibody': ['CBX2']},'ENCODE_BROADPEAK_HG19')
            self.assertEqual(1, number)
        except Exception as er:
            self.fail(er)

    def test_query_metadata_encode_2(self):
        try:
            number = self.requester.filter_by_meta({'antibody': ['CTCF'], 'cell_tissue': ['breast']},'ENCODE_BROADPEAK_HG19')
            self.assertEqual(3, number)

            number1 = self.requester.filter_by_meta({'ANATOMY': ['BREAST'], 'assay': ['DNase.hotspot.broad']},'EPIGENOME_BROADPEAK_HG19')
            self.assertEqual(1, number1)
        except Exception as er:
            self.fail(er)

    def test_query_metadata_encode_3(self):
        try:
            number = self.requester.filter_by_meta({'dataType': ['ChipSeq'], 'antibody_target': ['CTCF']},'ENCODE_BROADPEAK_HG19')
            self.assertEqual(81, number)
        except Exception as er:
            self.fail(er)

    def test_query_integration_1(self):
        query = "encode_broadpeaks=SELECT( dataType == 'ChipSeq' AND cell == '{cell}' AND antibody == '{antibody}' ) ENCODE_BROADPEAK_HG19;" \
                    "MATERIALIZE encode_broadpeaks;"
        q = Query(name='test1', description='d1', query=query)
        q.save()
        self.assertEqual(True, q.has_selection())
        statements = q.extract_selection_statements({'cell':'K562', 'antibody':'CTCF'})
        self.assertEqual(1, len(statements))
        self.assertEqual(0, statements[0].count)

    def test_query_integration_2(self):
        query = "encode_broadpeaks=SELECT( dataType == 'ChipSeq' AND cell == '{cell}' AND antibody == '{antibody}' ) ENCODE_BROADPEAK_HG19;" \
                    "MATERIALIZE encode_broadpeaks;"
        q = Query(name='test1', description='d1', query=query)
        q.save()
        self.assertEqual(True, q.has_selection())
        statements = q.extract_selection_statements({'cell':'K562', 'antibody':'CBX2'})
        self.assertEqual(1, len(statements))
        self.assertEqual(1, statements[0].count)

    def test_query_integration_3(self):
        query = "encode=SELECT( antibody == '{antibody}' AND cell_tissue == '{cell_tissue}' ) ENCODE_BROADPEAK_HG19;"\
                "dnaseseq=SELECT( ANATOMY == '{ANATOMY}' AND assay == 'DNase.hotspot.broad' ) EPIGENOME_BROADPEAK_HG19;"\
                "encode_in_dnaseseq=JOIN(distance < 0, right) dnaseseq encode;" \
                "MATERIALIZE encode_in_dnaseseq;" \

        q = Query(name='test1', description='d1', query=query)
        q.save()
        self.assertEqual(True, q.has_selection())
        statements = q.extract_selection_statements({'antibody':'CTCF', 'cell_tissue': 'breast', 'ANATOMY': 'BREAST'})
        self.assertEqual(2, len(statements))
        self.assertEqual(3, statements[0].count)
        self.assertEqual(1, statements[1].count)

    def tearDown(self):
        self.requester.clear()
