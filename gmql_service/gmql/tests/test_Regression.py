from unittest import TestCase
from gmql.tests.test_Queries import GMQLQueryBaseTest
from gmql.tests.testutils import QueryTester
from gmql.utils import gmql_parser


class Regression1(GMQLQueryBaseTest):
    def test_run_task(self):
        self.tester = QueryTester(query='GENE=SELECT(*) {test_dsname4};MATERIALIZE GENE;',
                         parameters_dict = {"test_dsname4": "file"},
                         files_dict={"test_dsname4": ["input/1.bed"]})

        super(Regression1, self).run_query()


class CustomQuery(TestCase):
    def test_double_bracket(self):
        query = 'a={b}}'
        parameters = gmql_parser.parse_gqml_parameters(query)
        self.assertTrue(len(parameters) == 0)