from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.urlresolvers import reverse
from django.test import Client, TestCase
from django.utils.datastructures import MultiValueDict
from gmql.models import test_file_path
from rebar.testing import flatten_to_dict
import zipfile

from gmql.tasks import *

class LoginedClientTestCase(TestCase):
    def setUp(self):
        self.username = 'test'
        self.client = Client()
        if not User.objects.filter(username='test').exists():
            User.objects.create_user(username='test', email='test@test.com', password='test')
        self.client.login(username=self.username, password='test')

    def get_user(self):
        return User.objects.get(username=self.username)


def fileiterator(zipf):
    with zipfile.ZipFile(zipf, "r", zipfile.ZIP_STORED) as openzip:
        filelist = openzip.infolist()
        for f in filelist:
            yield(f.filename, openzip.read(f))


class QueryTester:
    def __init__(self, query, parameters_dict, files_dict=None, query_name=None, clear_files=True):
        if not files_dict: files_dict = {}
        self.clear_files = clear_files

        description = str(hash(query))
        name = query_name if query_name else description

        self.query = Query(name=name, description=description, query=query)
        self.query.save()

        parameters = parameters_dict

        self.task = Task(query_id=self.query.pk)
        self.task.set_parameters(parameters)
        self.task.save()

        self.files = MultiValueDict()
        for parameter in files_dict:
            file_list = []
            for file in files_dict[parameter]:
                upload_file = open(test_file_path(file), 'rb')
                file_list.append(SimpleUploadedFile(upload_file.name, upload_file.read()))
            self.files.setlist('%s-value' % parameter, file_list)

        self.requester = GMQLRequester()


    def result_files(self):
        return GMQLFile.objects.filter(task=self.task, file_type=GMQLFile.DOWNLOAD)

    def clear(self):
        if self.clear_files:
            all_files = GMQLFile.objects.filter(task=self.task)
            for file in all_files:
                file.delete()
        self.requester.clear()



def data_with_prefix(parameters):
    res = {}
    if not isinstance(parameters, list):
        parameters = [parameters]
    for p in parameters:
        prefix = p["name"]
        res_item = dict((prefix + "-" + k, v) for (k, v) in p.items())
        res.update(res_item)
    return res


def get_example_response(client):
    data = {'submit_example': ''}
    response = client.post(reverse('gmql:custom'), data)
    context = flatten_to_dict(response.context['custom_form'])

    context['submit_parameters'] = ''
    response = client.post(reverse('gmql:custom'), context)
    return response


def create_query_from_example(client, default=False):
    response = get_example_response(client)

    data = {}
    parameters_form = response.context['parameters_form']
    custom_form = response.context['custom_form']
    params = flatten_to_dict(parameters_form)
    if default:
        for k in params:
            if 'default_value' in k:
                params[k] = "default"
    data.update(params)
    data.update(flatten_to_dict(custom_form))
    data['submit_create'] = ''

    response = client.post(reverse('gmql:custom'), data)
    return response

