from collections import OrderedDict
from django.core.urlresolvers import reverse as django_reverse
from gmql.models import Query, Task, GMQLFile
from rest_framework import serializers
from rest_framework import generics
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse as rest_reverse
from gmql.tasks import process_task


class ParameterSerializer(serializers.Serializer):
    description = serializers.CharField()
    parameter_type = serializers.CharField()
    name = serializers.CharField()
    value = serializers.CharField(allow_blank=True)

    def to_representation(self, instance):
        """
        Object instance -> Dict of primitive datatypes.
        """
        ret = OrderedDict()
        fields = [field for field in self.fields.values() if not field.write_only]

        for field in fields:
            try:
                attribute = field.get_attribute(instance)
            except Exception:
                continue

            if attribute is None:
                # We skip `to_representation` for `None` values so that
                # fields do not have to explicitly deal with that case.
                ret[field.field_name] = None
            else:
                ret[field.field_name] = field.to_representation(attribute)

        return ret


class QuerySerializer(serializers.HyperlinkedModelSerializer):
    parameter_set = ParameterSerializer(many=True)
    class Meta:
        model = Query
        fields = ('url', 'name', 'description', 'query', 'parameter_set')

    def save(self, **kwargs):
        request = self.context.get('request', None)
        assert request is not None, (
            "`%s` requires the request in the serializer"
            " context. Add `context={'request': request}` when instantiating "
            "the serializer." % self.__class__.__name__
        )
        parameters_dict = {par['name'] : par['value'] for par in self.validated_data['parameter_set']}

        task = Task(query_id=self.instance.pk, user_id=request.user.id)
        task.set_parameters(parameters_dict)
        task.save()
        #TODO add file support
        result = process_task.delay(task, {})


    def update(self, instance, validated_data):
        self.save()

class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = GMQLFile
        fields = ('file', 'file_type')



class FileDownloadField(serializers.RelatedField):
    def to_representation(self, value):
        request = self.context.get('request', None)
        assert request is not None, (
            "`%s` requires the request in the serializer"
            " context. Add `context={'request': request}` when instantiating "
            "the serializer." % self.__class__.__name__
        )
        return request.build_absolute_uri(django_reverse('gmql:file_download', args=[value.task_id, value.pk]))


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    gmqlfile_set = FileDownloadField(many=True, queryset=GMQLFile.objects.filter(file_type=GMQLFile.DOWNLOAD))
    class Meta:
        model = Task
        fields = ('url', 'query', 'date', 'status', 'last_error', 'gmqlfile_set')


class QueryList(generics.ListAPIView):
    """
    Returns the list of all available queries.

    Note that the list includes public and your custom queries (if any).
    """
    serializer_class = QuerySerializer

    def get_queryset(self):
        user = None
        if self.request.user.is_authenticated():
            user = self.request.user
        return Query.objects.globals_and_customs(user)


class QueryDetails(generics.RetrieveUpdateAPIView):
    """
    Returns information and parameters of a query.

    Options to run a query:
    1. PUT request. With a PUT request you need to send the whole JSON object of the query (returned by GET) + 'value' key and a value for each parameter.
    e.g. {"url":"/rest/queries/1/","name":"Example name","description":"Example description","query":"q1={q1}; q2={q2}","parameter_set":[{"name":"q1","description":"descrq1","parameter_type":"Text", "value":"1"},{"name":"q2","description":"descrq2","parameter_type":"Text", "value":"2"}]}
    2. PATCH request. To run a query by a PATCH request you need to add/send only the values like so:
    {"parameter_set":[{"name":"q1","value":"1"},{"name":"q2", "value":"2"}]}

    When you run a query a new task will be created in the task list. You can track its execution.

    """
    queryset = Query.objects.all()
    serializer_class = QuerySerializer
    permission_classes = (permissions.IsAuthenticated, )



class TaskList(generics.ListAPIView):
    """
    Returns the list of all available tasks for your account.

    """
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = (permissions.IsAuthenticated, )


class TaskDetails(generics.RetrieveAPIView):
    """
    Returns a specific task that you invoked.

    A task has three state: error, running, success. When a task has the success state a list of output files appears (if any).
    You can download those files using the links.
    """
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = (permissions.IsAuthenticated, )


@api_view(('GET',))
def gmql_api(request, format=None):
    return Response({
        'queries': rest_reverse('query-list', request=request, format=format),
        'tasks': rest_reverse('task-list', request=request, format=format)
    })
