from itertools import islice
from django.conf import settings

def is_bed(header):
    res = False
    for line in header:
        if line.startswith("#") or line.startswith("browser") or line.startswith("track") or line.startswith(
                "visibility"):
            continue
        if len(line.strip()) == 0:
            continue
        if line[0].isspace():
            continue
        columns = line.split('\t')
        if len(columns) < 3:
            res = False
            break
        if not columns[1].isdigit() or not columns[2].isdigit():
            res = False
            break
        res = True

    if res:
        return "bed"
    else:
        return None


def is_vcf(header):
    res = False
    for line in header:
        if line.startswith("##fileformat=VCFv"):
            res = True
            break
        if line.startswith("#"):
            continue
        if len(line.strip()) == 0:
            continue
        if line[0].isspace():
            continue
        columns = line.split('\t')
        if len(columns) < 5:
            res = False
            break
        if columns[2].isdigit():
            res = False
            break
        if columns[2].startswith('rs') or columns[2].startswith('.'):
            res = True
    if res:
        return "vcf"
    else:
        return None


class FormatGuesser:
    def __init__(self, f):
        self.header = list(islice(f, 40))
        self.check_functions = [is_bed, is_vcf]

    def get_format(self):
        for f in self.check_functions:
            res = f(self.header)
            if res is not None:
                return res
        return settings.DEFAULT_FORMAT
