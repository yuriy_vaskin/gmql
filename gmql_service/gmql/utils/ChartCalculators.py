from collections import defaultdict
import json

sortdict = defaultdict(int)
sortdict['chr1'] = 1
sortdict['chr2'] = 2
sortdict['chr3'] = 3
sortdict['chr4'] = 4
sortdict['chr5'] = 5
sortdict['chr6'] = 6
sortdict['chr7'] = 7
sortdict['chr8'] = 8
sortdict['chr9'] = 9
sortdict['chr10'] = 10
sortdict['chr11'] = 11
sortdict['chr12'] = 12
sortdict['chr13'] = 13
sortdict['chr14'] = 14
sortdict['chr15'] = 15
sortdict['chr16'] = 16
sortdict['chr17'] = 17
sortdict['chr18'] = 18
sortdict['chr19'] = 19
sortdict['chr20'] = 20
sortdict['chr21'] = 21
sortdict['chr22'] = 22
sortdict['chrX'] = 23
sortdict['chrY'] = 24
sortdict['chrM'] = 25

def compare_chromosomes(chr1, chr2):
        return sortdict[chr1] - sortdict[chr2]


class ChartChromosomeDistribution(object):
    def __init__(self):
        self.name = 'ChromosomeDistribution'
        self.table = False
        self.has_chromosomes = False

    def test(self, query):
        return True

    def run(self, genome_space):
        data = {
            'categories': [],
            'series': []
        }
        for sample_name in genome_space:
            for chromosome in genome_space[sample_name].chr_regions:
                if chromosome not in data['categories']:
                    data['categories'].append(chromosome)
        data['categories'] = sorted(data['categories'], cmp=compare_chromosomes)
        for sample_name in genome_space:
            data_by_chromosome = []
            for chromosome in data['categories']:
                data_by_chromosome.append(len(genome_space[sample_name].chr_regions[chromosome]))

            data['series'].append({'name': sample_name, 'data': data_by_chromosome})
        return data

    def html(self, data, chromosome=None):
        res = "chart: { \
            type: 'bar' \
        }, \
        title: {\
            text: 'Region Distribution by Chromosomes'\
        },\
        xAxis: {\
            categories: %s\
        },\
        yAxis: {\
            title: {\
                text: 'Number of regions in each chromosome'\
            }\
        }, \
        series: %s" % (json.dumps(data['categories']), json.dumps(data['series']))
        return str(res)

class ChartRegionDistribution(object):
    def __init__(self):
        self.name = 'RegionDistribution'
        self.table = False
        self.has_chromosomes = False

    def test(self, query):
        return True

    def run(self, genome_space):
        data = {
            'categories': [],
            'series': []
        }
        for sample_name in genome_space:
            data['categories'].append(genome_space[sample_name].display_name(sample_name))

        data_by_sample = []
        for sample_name in genome_space:
            data_by_sample.append(sum([len(genome_space[sample_name].chr_regions[c]) for c in genome_space[sample_name].chr_regions]))
        data['series'].append({'name': 'Number of regions', 'data': data_by_sample})
        return data

    def html(self, data, chromosome=None):
        res = "chart: { \
            type: 'column' \
        }, \
        title: {\
            text: 'Global Region Distribution by Samples'\
        },\
        plotOptions: {\
            column: {\
                dataLabels: {\
                    enabled: true\
                }\
            }\
        },\
        xAxis: {\
            categories: %s\
        },\
        yAxis: {\
            title: {\
                text: 'Number of regions in each sample'\
            }\
        }, \
        series: %s" % (json.dumps(data['categories']), json.dumps(data['series']))
        return str(res)


exclusion_set = [
    'origAssembly', 'subId', 'lab_description', 'lab_labPi', 'dataType_tag', 'seqPlatform_label',
    'antibody_vendorName', 'grant_grantInst', 'view_description', 'view_tag', 'lab_labPiFull',
    'cell_organism', 'cell_vendorId','dccAccession', 'control_label',
    'view','antibody_targetUrl', 'control_type', 'dataVersion', 'dataType_type', 'tableName',
    'treatment_tag', 'control_description', 'dateSubmitted', 'cell_type', 'seqPlatform_description',
    'treatment_type', 'antibody_vendorId', 'view_label', 'size', 'lab_organism', 'cell_tier',
    'grant_projectName', 'antibody_validation', 'lab_labInst', 'control_tag', 'antibody_type',
    'cell_color', 'seqPlatform_tag', 'cell_vendorName', 'lab_type', 'treatment_description',
    'lab_grantPi', 'treatment_label', 'cell_lineage', 'setType', 'grant_type', 'controlId',
    'dateUnrestricted', 'lab_tag','seqPlatform_geoPlatformName', 'composite', 'cell_orderUrl',
    'dataType_dataGroup', 'seqPlatform', 'dataType_description', 'cell_protocol', 'cell_sex',
    'project', 'softwareVersion', 'control','antibody_lab', 'cell_termUrl', 'cell_termId',
    'antibody_orderUrl', 'grant_tag', 'seqPlatform_type', 'view_type', 'antibody_targetDescription',
    'antibody_targetId', 'lab', 'ID', 'antibody_lots', 'lab_label', 'grant_description',
    'url', 'grant', 'md5sum', 'dataType_label', 'cell_tag', 'antibody_antibodyDescription',
    'AGE', 'DONOR_/_SAMPLE_ALIAS', 'Epigenome_name_(from_EDACC_Release_9_directory)',
    'ORDER', 'LAB', 'TYPE', 'SOLID_/_LIQUID', 'SEX', 'ETHNICITY', 'CLASS', 'Epigenome_Mnemonic',
    'Single_Donor_(SD)_/Composite_(C)', 'GROUP', 'grant_label', 'cell_lots', 'labVersion', 'labExpId',
]

exclusion_chart_addional_set = [
    'treatment','antibody_label', 'geoSampleAccession', 'dataType_label', 'cell_tag', 'antibody_antibodyDescription',
    'antibody_tag', 'type', 'md5sum', 'cell_description', 'dataType', 'cell_karyotype', 'Epigenome_ID_(EID)', 'Standardized_Epigenome_name',
    ]

class TableMetadataTerms(object):
    def __init__(self):
        self.name = 'MetadataTerms'
        self.table = True
        self.has_chromosomes = False

    def test(self, query):
        return True

    def run(self, genome_space):
        data = {
            'header': [],
            'rows': []
        }
        metaterms = set()
        for sample_name in genome_space:
            data['header'].append(sample_name)
            metaterms.update(genome_space[sample_name].meta_dict.keys())

        metaterms.difference_update(exclusion_set)
        #for key.term
        metaterms = {term for term in metaterms if all((not term.endswith('.%s' % e) for e in exclusion_set))}


        for term in metaterms:
            data_by_sample = []
            for sample_name in genome_space:
                if term not in genome_space[sample_name].meta_dict:
                    data_by_sample.append('-')
                else:
                    data_by_sample.append(genome_space[sample_name].meta_dict[term])

            data['rows'].append({'name': term, 'data': data_by_sample})

        return data

    def html(self, data, chromosome=None):
        res = ['<thead>', '<tr>']
        res.append('<td></td>')
        for sample_name in data['header']:
            res.append('<td><b>%s</b></td>'%sample_name)
        res.append('</tr>')
        res.append('</thead>')
        res.append('<tbody>')

        for data_row in data['rows']:
            res.append('<tr>')
            res.append('<td class="active">%s</td>' % data_row['name'])
            for data_item in data_row['data']:
                res.append('<td>%s</td>' % data_item)
            res.append('</tr>')

        res.append('</tbody>')
        return '\n'.join(res)


class ChartMetadataTermsDistribution(object):
    def __init__(self):
        self.name = 'MetadataTermsDistribution'
        self.table = False
        self.has_chromosomes = False

    def test(self, query):
        return True

    def run(self, genome_space):
        data = {
            'categories': [],
            'series': []
        }

        metaterms = set()
        for sample_name in genome_space:
            metaterms.update(genome_space[sample_name].meta_dict.keys())

        metaterms.difference_update(exclusion_set)
        metaterms = {term for term in metaterms if all((not term.endswith('.%s' % e) for e in exclusion_set))}
        metaterms.difference_update(exclusion_chart_addional_set)
        metaterms = {term for term in metaterms if all((not term.endswith('.%s' % e) for e in exclusion_chart_addional_set))}

        metavalue_dict = {}
        for metaterm in metaterms:
            metavalue_dict[metaterm] = set()
            for sample_name in genome_space:
                if metaterm in genome_space[sample_name].meta:
                    metavalue_dict[metaterm].add(genome_space[sample_name].meta[metaterm])


        data_by_metaterm = []
        for metaterm in metavalue_dict:
            for metaval in metavalue_dict[metaterm]:
                data['categories'].append(metaterm+'_'+metaval)
                metaterm_count = 0
                for sample_name in genome_space:
                    if metaterm in genome_space[sample_name].meta and metaval == genome_space[sample_name].meta[metaterm]:
                        metaterm_count += 1
                data_by_metaterm.append(metaterm_count)
        data['series'].append({'name': 'Number of metadata terms', 'data': data_by_metaterm})


        return data

    def html(self, data, chromosome=None):
        res = "chart: { \
            type: 'bar' \
        }, \
        title: {\
            text: 'Sample Distribution by Metadata Terms'\
        },\
        plotOptions: {\
            bar: {\
                dataLabels: {\
                    enabled: true\
                }\
            }\
        },\
        xAxis: {\
            allowDecimals: false,\
            categories: %s\
        },\
        yAxis: {\
            allowDecimals: false,\
            title: {\
                text: 'Number of samples'\
            }\
        }, \
        series: %s" % (json.dumps(data['categories']), json.dumps(data['series']))
        return str(res)



class ChartHeatmapMapOperation(object):
    def __init__(self):
        self.name = 'ChartHeatmapMapOperation'
        self.table = False
        self.has_chromosomes = True

    def test(self, query):
        return 'map' in query.lower() or 'join' in query.lower()


    def run(self, genome_space):
        data = {}

        basis = genome_space.basis_sample()
        chromosomes = sorted([c for c in basis.chr_regions], cmp=compare_chromosomes)
        for c in chromosomes:
            data[c] = {}
            data[c]['xcategories'] = []
            data[c]['ycategories'] = []
            data[c]['series'] = []
            for r in basis.regions(c):
                data[c]['xcategories'].append('%d-%d' % (r.start, r.stop))

            for sample_name in genome_space:
                data[c]['ycategories'].append(genome_space[sample_name].display_name(sample_name))


            for sample_name in enumerate(genome_space):
                for r in enumerate(basis.regions(c)):
                    val = 0 if r[1] in genome_space[sample_name[1]] else 1
                    data[c]['series'].append([r[0], sample_name[0], val])

        return data

    def html(self, data, chromosome=None):
        if chromosome not in data:
            return ''

        res = "chart: { \
            type: 'heatmap' \
        }, \
        title: {\
            text: 'Distribution of regions across samples'\
        },\
        xAxis: {\
            categories: %s\
        },\
        colorAxis: {\
            min: 0,\
            max: 1,\
        },\
        yAxis: {\
            categories: %s,\
            title: {\
                 title: null\
            }\
        }, \
        series:[{ \
                    'data': %s,\
                    tooltip: {\
                        useHTML: true,\
                        headerFormat: '{point.key}',\
                        pointFormat: 'Sample: {point.y} Region: {point.x} Has region: {point.value}',\
                     },\
            },\
        ]" % (json.dumps(data[chromosome]['xcategories']), json.dumps(data[chromosome]['ycategories']), json.dumps(data[chromosome]['series']))

        return str(res)



#hitmap with region distribution (rows - samples, columns - regions, p or q values?)

