__author__ = 'yvaskin'


class Tags:
    tag_categories = []

    def __init__(self, category, tags):
        self.category = category
        self.tags = tags
        Tags.tag_categories.append(self)


    @staticmethod
    def tags_dict():
        return {t.category : t.tags for t in Tags.tag_categories}

    @staticmethod
    def find_all_tags(query, **kwargs):
        pre_result = [tag.find_tags(query, **kwargs) for tag in Tags.tag_categories]
        return [res for sublist in pre_result for res in sublist]

    def find_tags(self, query, **kwargs):
        return [tag for tag in self.tags if tag.lower() in query.lower()]



gmql_tags = Tags("GMQL Operations", ["select", "project", "map", "cover", "join", "union"])
data_tags = Tags("Data", ["ENCODE", "EPIGENOME"])
genome_tags = Tags("Genome", ["hg19", "mm9"])
