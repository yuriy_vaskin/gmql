from collections import defaultdict
from gmql.utils import gmql_parser
from gmql.utils.gmql_integration import GMQLRequester
import re

class SelectionTerm(object):
    """
    A term for selection
    """
    def __init__(self):
        self.name = ''
        self.values = []

class SelectStatement(object):
    def __init__(self, statement, parameters):
        self.parameters = parameters
        self.statement = statement
        self.statement_with_parameters = gmql_parser.set_gqml_parameters(self.statement, self.parameters)
        self.collection_name = ''
        self.terms = defaultdict(list)
        self.count = 0

        self.parse()

    @staticmethod
    def filter_statement(statement):
        statement = re.sub('\s+', '', statement)
        return '=SELECT'.lower() in statement.lower() and not statement.endswith('}') and '=SELECT(*)'.lower() not in statement.lower()

    @staticmethod
    def get_statements(query, parameters):
        statements = re.sub('\s+', ' ', query).split(';')
        return [SelectStatement(stat, parameters) for stat in statements if SelectStatement.filter_statement(stat)]

    def parse(self):
        nospaces_statement = re.sub('\s+', '', self.statement)
        idx = nospaces_statement.rfind(')')
        if idx == -1:
            return
        self.collection_name = nospaces_statement[idx+1: ]

        inner_select_re = re.compile("SELECT\s*\(\s*(.*)\s*\)")
        inner_selects = inner_select_re.findall(self.statement)
        if len(inner_selects) != 1:
            return

        inner_select = inner_selects[0]

        self._parse_inner_select(inner_select)

    def _parse_inner_select(self, inner_select):
        inner_select_re = re.compile("([\w_\-\d]+)\s*==\s*([\w\d\-_\'\{\}\.\(\)]+)")
        equails = inner_select_re.findall(inner_select)
        for k,v in equails:
            if not v:
                continue
            v = gmql_parser.set_gqml_parameters(v, self.parameters)
            v = re.sub("^\'", '', v)
            v = re.sub("\)+$", '', v)
            v = re.sub("\'$", '', v)

            if v not in self.terms[k]:
                self.terms[k].append(v)

    def retrieve_count(self):
        self.count = 0
        try:
            requester = GMQLRequester()
            self.count = requester.filter_by_meta(self.terms, self.collection_name)
            requester.clear()
        except RuntimeError:
            self.count = 0

