import shutil
import zipfile
from django.conf import settings
from gmql.models import GMQLFile
import re
import os


def error_decorator(archive_func):
        def inner(self, *args, **kwargs):
            try:
                return archive_func(self, *args, **kwargs)
            except Exception as e:
                self.error_msg = e.message
                if settings.DEBUG:
                    raise

        return inner


class GMQLArchive(object):
    def __init__(self, path):
        self.error_msg = ''

        self._init_zip(path)

        self.observers = []

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()


    def register_observer(self, observer):
        if observer not in self.observers:
            self.observers.append(observer)

    def outputs(self):
        return [out for observer in self.observers for out in observer.output()]

    @error_decorator
    def check_file_size(self, path):
        """
        Checks file size
        :param path: input path
        :return:
        """
        statinfo = os.stat(path)
        if settings.MAX_PROCESS_FILE_SIZE < statinfo.st_size:
            self.error_msg = 'Too big file to process'
            return False
        return True


    @error_decorator
    def _init_zip(self, path):
        """
        Initializes a zip file
        :param path: input path
        :return:
        """
        if self.check_file_size(path):
            self.zipfile = zipfile.ZipFile(path, 'r')
            self.archive_name = path.split('/')[-1]

    def has_error(self):
        return len(self.error_msg) != 0

    def error(self):
        return self.error_msg

    @error_decorator
    def close(self):
        self.zipfile.close()
        self._finish()



    @error_decorator
    def process(self):
        if not self.has_error():
            self._archive_open(self.archive_name)

            for file_name, meta_name in self._file_names():
                with self.zipfile.open(file_name) as gtf_file:
                    meta_dict = self._meta_dict(meta_name)
                    self._file_open(file_name.split()[-1], meta_dict)

                    for line in gtf_file:
                        self._data_line(line)

                    self._file_close(file_name.split()[-1])

            self._archive_close(self.archive_name)

    def _file_names(self):
        res = []
        name_list = self.zipfile.namelist()
        for file_name in name_list:
            if file_name.endswith('.gtf'):
                meta_name = file_name + '.meta'
                if meta_name in name_list:
                    res.append((file_name, meta_name))
        return res


    def _meta_dict(self, meta_name):
        res = {}
        with self.zipfile.open(meta_name) as meta_file:
            for line in meta_file:
                columns = line.strip().split('\t')
                if len(columns) == 2:
                    res[columns[0]] = columns[1]
        return res


    #observer methods
    def _archive_open(self, archive_name):
        [observer.archive_open(archive_name) for observer in self.observers]

    def _archive_close(self, archive_name):
        [observer.archive_close(archive_name) for observer in self.observers]

    def _finish(self):
        [observer.finish() for observer in self.observers]

    def _file_open(self, file_name, meta_dict):
        [observer.file_open(file_name, meta_dict) for observer in self.observers]

    def _file_close(self, file_name):
        [observer.file_close(file_name) for observer in self.observers]

    def _data_line(self, d):
        [observer.data_line(d) for observer in self.observers]




class GMQLArchiveWriter(object):
    def __init__(self, outpath):
        self.label = 'default'
        self.extention = 'GTF'
        self.outpath = outpath
        self.out_archives = []


    @error_decorator
    def output(self):
        return [(GMQLFile.DOWNLOAD, self.label, out_archive) for out_archive in self.out_archives]


    @error_decorator
    def archive_open(self, archive_name):
        arch_path = os.path.join(self.outpath, self.label, archive_name)
        d = os.path.dirname(arch_path)
        if not os.path.exists(d):
            os.makedirs(d)
        self.archive_file = zipfile.ZipFile(arch_path, 'w')


    @error_decorator
    def archive_close(self, archive_name):
        self.out_archives.append(os.path.join(self.outpath, self.label, archive_name))
        self.archive_file.close()

    @error_decorator
    def file_open(self, file_name, meta_dict):
        self.meta_dict = meta_dict
        self.file = open(os.path.join(self.outpath, self.label, file_name.split('/')[-1]), 'w')

    @error_decorator
    def file_close(self, file_name):
        self.file.close()
        name_in_archive = self.file.name.split('/')[-1]
        name_in_archive = name_in_archive[:-len('gtf')] + self.extention
        self.archive_file.write(os.path.abspath(self.file.name), arcname=name_in_archive)
        os.remove(os.path.abspath(self.file.name))

    @error_decorator
    def data_line(self, d):
        self.file.write(d)

    def split_gtf_line(self, line):
        columns = line.split('\t')
        content = columns[0:-1]
        reg = r'\s+(\w+)\s+\"(.*?)\";'
        terms = {key : value for (key, value) in re.findall(reg, columns[-1])}
        return (content, terms)

    @error_decorator
    def finish(self):
        for f in os.listdir(os.path.join(self.outpath, self.label)):
            if os.path.isfile(f):
                if f not in self.archive_file:
                    os.remove(f)
            #else:
            #    shutil.rmtree(f)


class GMQLArchiveBEDWriter(GMQLArchiveWriter):
    def __init__(self, outpath):
        super(GMQLArchiveBEDWriter, self).__init__(outpath)
        self.label = 'BED'
        self.extention = 'bed'

    @error_decorator
    def data_line(self, d):
        content, terms = super(GMQLArchiveBEDWriter, self).split_gtf_line(d)
        line = [content[0], str(int(content[3]) + 1), content[4], '\n']
        self.file.write('\t'.join(line))


class GMQLArchiveCSVWriter(GMQLArchiveWriter):
    def __init__(self, outpath):
        super(GMQLArchiveCSVWriter, self).__init__(outpath)
        self.label = 'CSV'
        self.extention = 'csv'
        self.header = []
        self.header_terms = []

    @error_decorator
    def file_open(self, file_name, meta_dict):
        super(GMQLArchiveCSVWriter, self).file_open(file_name, meta_dict)
        self.header = []
        self.header_terms = []

    @error_decorator
    def data_line(self, d):

        content, terms = super(GMQLArchiveCSVWriter, self).split_gtf_line(d)

        if len(self.header) == 0:
            #first line
            self.header_terms = terms.keys()
            self.header = ['chr', 'start', 'stop']
            [self.header.append(term) for term in terms]
            [self.header.append(term) for term in self.meta_dict]
            self.header.append('\n')
            line = '#' + '\t'.join(self.header)
            self.file.write(line)

        line = [content[0], str(int(content[3]) + 1), content[4]]

        for term in self.header_terms:
            if term not in terms:
                val = '.'
            else:
                val = re.sub(r"\s+", " ", terms[term])
            line.append(val)

        [line.append(re.sub(r"\s+", " ", self.meta_dict[term])) for term in self.meta_dict]
        line.append('\n')

        line = '\t'.join(line)

        self.file.write(line)


class GMQLArchiveIGBWriter(GMQLArchiveWriter):
    def __init__(self, outpath):
        super(GMQLArchiveIGBWriter, self).__init__(outpath)
        self.label = 'IGB'
        self.extention = 'gtf'
        self.files = []
        self.script_path = ''


    @error_decorator
    def output(self):
        res = [(GMQLFile.BROWSE, '', f) for f in self.files]
        if self.script_path:
            res.append((GMQLFile.BROWSE, self.label, self.script_path))
        return res


    @error_decorator
    def data_line(self, d):
        line = d.strip() + ' ' + ' '.join(['%s "%s";' % (key, self.meta_dict[key]) for key in self.meta_dict]) + '\n'
        self.file.write(line)


    @error_decorator
    def archive_open(self, archive_name):
        arch_path = os.path.join(self.outpath, self.label, archive_name)
        d = os.path.dirname(arch_path)
        if not os.path.exists(d):
            os.makedirs(d)


    #TODO support other genomes
    @error_decorator
    def archive_close(self, archive_name):
        if not self.files:
            return

        fname = archive_name.split('/')[-1]
        fname = fname[:-len('zip')] + 'igb'
        script_path = os.path.join(self.outpath, self.label, fname)
        script = open(script_path, 'w')
        to_write = ['genome H_sapiens_Feb_2009']
        for f in self.files:
            to_write.append('load %s' % (settings.SITE_URL + settings.MEDIA_URL + GMQLFile.file_download_path(f)))

        to_write.append('refresh')
        script.write('\n'.join(to_write))
        self.script_path = script.name


    @error_decorator
    def file_open(self, file_name, meta_dict):
        self.meta_dict = meta_dict
        fname = file_name.split('/')[-1]
        fname = fname[:-len('gtf')] + self.extention
        self.file = open(os.path.join(self.outpath, self.label, fname), 'w')


    @error_decorator
    def file_close(self, file_name):
        self.files.append(self.file.name)
        self.file.close()


    @error_decorator
    def finish(self):
        pass
