import bisect
from collections import defaultdict
from gmql.models import ChartCalculatorRunner
from gmql.utils.GMQLArchive import GMQLArchiveWriter, error_decorator


class GenomeSpaceRegion(object):
    def __init__(self, chr, start, stop, meta=None):
        self.chr = chr
        self.start = int(start)
        self.stop = int(stop)
        if not meta:
            self.meta = {}
        else:
            self.meta = meta

    def __str__(self):
        return "%s %d %d" % (self.chr, self.start, self.stop)

    def __lt__(self, other):
        return self.start < other.start

    def __eq__(self, other):
        return self.start == other.start and self.stop == other.stop and self.chr == other.chr

    def __ne__(self, other):
        return not self == other


class GenomeSpaceSample(object):
    def __init__(self):
        self.chr_regions = defaultdict(list)
        self.meta_dict = {}

    @property
    def meta(self):
        return self.meta_dict

    @meta.setter
    def meta(self, value):
        self.meta_dict = value

    def add(self, region):
        bisect.insort_left(self.chr_regions[region.chr], region)

    def _insert_pos(self, region):
        target_list = self.chr_regions[region.chr]
        hi = len(target_list)
        pos = bisect.bisect_left(target_list, region, hi=hi)
        if pos == hi or target_list[pos] != region:
            return pos
        return -1

    def add_unique(self, region):
        pos = self._insert_pos(region)
        if pos != -1:
            self.chr_regions[region.chr].insert(pos, region)

    def regions(self, chr=None):
        if chr:
            for r in self.chr_regions[chr]:
                yield r
        else:
            for c in self.chr_regions:
                for r in self.chr_regions[c]:
                    yield r


    def __contains__(self, item):
        pos = self._insert_pos(item)
        return pos != -1


    def _normilize_name(self, sample_name):
        res = sample_name[:-len('.graph')] if sample_name.endswith('.graph') else sample_name[:]
        try :
            res = int(res.split('_')[-1])
        except ValueError:
            pass
        return res

    def display_name(self, sample_name):
        names_order_keys = [('EPIGENOME', ['Epigenome_ID_(EID)', 'assay']), ('ENCODE', ['dccAccession']), ('CUSTOM', ['name'])]
        names = defaultdict(list)
        for key in self.meta_dict:
            for name_key, name_list in names_order_keys:
                for name_val in name_list:
                    if key.endswith(name_val):
                        names[name_key].append(self.meta_dict[key])

        name = ''
        for name_key, name_val in names_order_keys:
            if name_key in names:
                name = name_key + '_' + '_'.join(names[name_key])
                break
        if not name:
            name = self._normilize_name(sample_name)
        return name


class GenomeSpace(object):
    def __init__(self, name=None):
        self.name = ''
        if name: self.name = name

        self.name_sample = {}

    def insert(self, name, sample):
        self.name_sample[name] = sample

    def __iter__(self):
        return self.name_sample.__iter__()

    def __getitem__(self, item):
        return self.name_sample[item]


    def basis_sample(self):
        sample = GenomeSpaceSample()
        for name in self.name_sample:
            for region in self.name_sample[name].regions():
                sample.add_unique(region)
        return sample



class GenomeSpaceWriter(GMQLArchiveWriter):
    def __init__(self, outpath, task_id, query):
        super(GenomeSpaceWriter, self).__init__(outpath)
        self.label = 'graph'
        self.extention = 'graph'
        self.outpath = outpath

        self.genome_spaces = {}

        self.cur_space = None
        self.cur_sample = None
        self.task_id = task_id
        self.query = query


    @error_decorator
    def output(self):
        return []


    @error_decorator
    def archive_open(self, archive_name):
        self.cur_space = GenomeSpace(archive_name)


    @error_decorator
    def archive_close(self, archive_name):
        if self.cur_space:
            self.genome_spaces[archive_name] = self.cur_space


    @error_decorator
    def file_open(self, file_name, meta_dict):
        self.cur_sample = GenomeSpaceSample()
        self.cur_sample.meta = meta_dict


    @error_decorator
    def file_close(self, file_name):
        if self.cur_sample and self.cur_space:
            name_in_archive = file_name.split('/')[-1]
            name_in_archive = name_in_archive[:-len('gtf')] + self.extention
            self.cur_space.insert(name_in_archive, self.cur_sample)


    @error_decorator
    def data_line(self, d):
        (content, terms) = super(GenomeSpaceWriter, self).split_gtf_line(d)
        terms['source'] = content[1]
        terms['feature'] = content[2]
        terms['score'] = content[5]
        terms['strand'] = content[6]
        terms['frame'] = content[7]
        region = GenomeSpaceRegion(content[0], content[3], content[4], terms)
        if self.cur_sample:
            self.cur_sample.add(region)

    @error_decorator
    def finish(self):
        runner = ChartCalculatorRunner(self.genome_spaces, self.task_id, self.query)
        runner.run()

