from string import Formatter


def parse_gqml_parameters(query):
    unique_list = []
    try:
        l = [i[1] for i in Formatter().parse(query) if (i[1] is not None and len(i[1]) != 0)]
    except ValueError:
        return []
    for p in l:
        if p not in unique_list:
            unique_list.append(p)
    return unique_list


def set_gqml_parameters(query, parameters):
    res = query
    try:
        res = query.format(**parameters)
    except KeyError:
        pass
    return res
