import os
import time

from django.conf import settings
from celery.utils.log import get_task_logger
import requests
import xmltodict

from gmql.utils import FormatGuess


DATASETS = 'datasets/'
UPLOAD_SAMPLE = 'uploadSamples/'
CREATE_DATASET = 'createDataSet/'
DELETE_DATASET = 'delete/'
PREPARE_DATASET = 'prepare/'
ZIP_DATASET = 'zip/'
LISTALL_DATASET = 'listAll/'

QUERY = 'query/'
SAVE_QUERY = 'save/'
RUN_QUERY = 'run/'
DELETE_QUERY = 'delete/'
TRACE_QUERY = 'trace/'
LOCAL_RUN = 'local'
MAPREDUCE_RUN = 'mapreduce'
METAKEYS = "repo/browse/meta"
FILTER_BY_META = "browse-c/meta/filtermany"

STEP_TIMEOUT = 5
NO_FILE_TRIES = 5


def _upload_sample_address(dataset_name):
    return os.path.join(settings.BASE_ADDRESS, DATASETS, UPLOAD_SAMPLE, dataset_name)


def _create_dataset_address(dataset_name):
    return os.path.join(settings.BASE_ADDRESS, DATASETS, CREATE_DATASET, dataset_name)


def _delete_dataset_address(dataset_name):
    return os.path.join(settings.BASE_ADDRESS, DATASETS, DELETE_DATASET, dataset_name)


def _download_zipped_dataset_address(dataset_id):
    return os.path.join(settings.BASE_ADDRESS, DATASETS, ZIP_DATASET, dataset_id)


def _listall_dataset_address():
    return os.path.join(settings.BASE_ADDRESS, DATASETS, LISTALL_DATASET)


def _prepare_zipped_dataset_address(dataset_id):
    return os.path.join(settings.BASE_ADDRESS, DATASETS, PREPARE_DATASET, dataset_id, 'true')


def _save_query_address():
    return os.path.join(settings.BASE_ADDRESS, QUERY, SAVE_QUERY)


def _delete_query_address(query_id):
    return os.path.join(settings.BASE_ADDRESS, QUERY, DELETE_QUERY, query_id)


def _run_query_address(key, mode):
    return os.path.join(settings.BASE_ADDRESS, QUERY, RUN_QUERY, key, mode)


def _trace_query_address(jobid):
    return os.path.join(settings.BASE_ADDRESS, QUERY, TRACE_QUERY, jobid)

def _meta_files_address():
    return os.path.join(settings.BASE_ADDRESS, METAKEYS)

def _filter_meta_files_address():
    return os.path.join(settings.BASE_ADDRESS, FILTER_BY_META)

def _get_proxies():
    proxies = None
    if settings.USE_HTTP_PROXY:
        proxies = {'http' : settings.BASE_ADDRESS }
    return proxies


# ##################################################
# #GMQLRequester
# #Requester for a single run only
class GMQLRequester:
    def __init__(self):
        self.uploaded_ds = ""
        self.dataset_names = []
        self.query_id = ""

        # HACK 3. Sometimes result of the query is SUCCESS, but there is now output dataset. In that case try NO_FILE_TRIES and than raise exceptions
        self.no_file_counter = 0


    # HACK1 use _ in datasetname
    def upload_dataset(self, files, name):
        if len(files) == 0:
            return
        # HACH2
        try:
            self.delete_dataset(name)
        except:
            pass
        first = True

        scheme = settings.DEFAULT_FORMAT

        for file_name in files:
            upload_files = {'file': (file_name.split('/')[-1], open(file_name, 'rb'), "application/octet-stream")}
            url = _upload_sample_address(name)
            if first:
                guess = FormatGuess.FormatGuesser(open(file_name, 'r'))
                scheme = guess.get_format()
                url = os.path.join(url, 'first')
                first = False
            else:
                url = os.path.join(url, 'middle')

            r = requests.post(url, files=upload_files, auth=settings.AUTH, proxies=_get_proxies())
            if not r.ok:
                raise RuntimeError("Cannot upload file %s.\nFull trace:%s" % (file_name.split('/')[-1], r.text))

        url = _create_dataset_address(name)
        data = {'schema': scheme}
        r = requests.post(url, auth=settings.AUTH, data=data, proxies=_get_proxies())

        if not r.ok:
            raise RuntimeError("Cannot create GMQL dataset %s.\nFull trace:%s" % (name, r.text))
        self.uploaded_ds = name


    @staticmethod
    def delete_dataset(name):
        url = _delete_dataset_address(name)
        r = requests.post(url, auth=settings.AUTH, proxies=_get_proxies())
        if not r.ok:
            raise RuntimeError("Cannot delete GMQL dataset %s.\nFull trace:%s" % (name, r.text))

    def run_query(self, query, name):
        url = _save_query_address()
        files = {'filekey': "",
                 'filename': name,
                 'query': query,
        }
        r = requests.post(url, auth=settings.AUTH, files=files, proxies=_get_proxies())
        if not r.ok:
            raise RuntimeError("Cannot save GMQL query %s.\nFull trace: %s" % (query, r.text))

        url = _run_query_address(r.text, settings.RUN_MODE)
        r = requests.get(url, auth=settings.AUTH, proxies=_get_proxies())
        if not r.ok:
            raise RuntimeError("Cannot run GMQL query %s.\nFull trace: %s" % (query, r.text))

        self.query_id = r.text

    def download_result(self, download_path):
        result = []
        if self.is_finished():
            for dataset in self.dataset_names:
                url = _prepare_zipped_dataset_address(dataset)
                r = requests.post(url, auth=settings.AUTH, proxies=_get_proxies())
                if not r.ok:
                    raise RuntimeError("Cannot prepare GMQL dataset %s.\nFull trace: %s" % (dataset, r.text))
                url = _download_zipped_dataset_address(dataset)
                r = requests.get(url, auth=settings.AUTH, stream=True, proxies=_get_proxies())
                if not r.ok:
                    raise RuntimeError("Cannot download GMQL dataset %s.\nFull trace: %s" % (dataset, r.text))
                if not os.path.exists(download_path):
                    os.makedirs(download_path)
                path = os.path.join(download_path, dataset + '.zip')
                with open(path, 'wb') as f:
                    for chunk in r.iter_content():
                        f.write(chunk)
                result.append(path)
        return result


    def delete_query(self):
        if len(self.query_id) > 0:
            url = _delete_query_address(self.query_id)
            r = requests.post(url, auth=settings.AUTH, proxies=_get_proxies())

    def _find_key(self, json_data, name):
        if name in json_data['filename']:
            return json_data['filekey']

        res = None
        if 'gmqlFile' in json_data:
            iter_data = json_data['gmqlFile']
            if not isinstance(iter_data, list):
                iter_data = [iter_data]
            for data in iter_data:
                res = self._find_key(data, name)
                if res:
                    break
        return res

    def _load_metakey(self, name):
        headers = {'accept': 'application/json'}
        url = _meta_files_address()
        r = requests.get(url, auth=settings.AUTH, proxies=_get_proxies(), headers=headers)
        if not r.ok:
            raise RuntimeError("Cannot load metadata.\nFull trace: %s" % (r.text))
        key = self._find_key(r.json(), name)
        return key

    def filter_by_meta(self, terms, collection_name):
        filekey = self._load_metakey(collection_name)
        if not filekey:
            raise RuntimeError("Cannot load metadata for dataset %s.\n" % (collection_name))

        headers = {'accept': 'application/json'}
        params = {'filekey': filekey,
                  'attributes': terms.keys(),
                  'values': terms.values(),
                  'valuesEachAttr': [len(terms[k]) for k in terms.keys()]}
        url = _filter_meta_files_address()
        r = requests.get(url, auth=settings.AUTH, proxies=_get_proxies(), headers=headers, params=params)
        if r.status_code == requests.codes.no_content:
            return 0
        if not r.ok:
            raise RuntimeError("Cannot load metadata.\nFull trace: %s" % (r.text))
        return int(r.json()['@count'])

    def clear(self):
        for dataset in self.dataset_names:
            # HACH4
            try:
                self.delete_dataset(dataset)
            except:
                pass

        self.delete_query()
        if len(self.uploaded_ds) > 0:
            # HACH5
            try:
                self.delete_dataset(self.uploaded_ds)
            except:
                pass

    def is_finished(self):
        if len(self.query_id) == 0:
            return True

        url = _trace_query_address(self.query_id)
        r = requests.get(url, auth=settings.AUTH, proxies=_get_proxies())
        if not r.ok:
            raise RuntimeError("Cannot trace GMQL query %s.\nFull trace: %s" % (self.query_id, r.text))
        responce_dict = xmltodict.parse(r.text)
        status = responce_dict['gmqlJobStatusXML']["status"]
        logger = get_task_logger(__name__)
        logger.info('Query status %s' % status)
        if status == "SUCCESS":
            print responce_dict
            if responce_dict['gmqlJobStatusXML']["datasetNames"] is not None:
                print responce_dict['gmqlJobStatusXML']["datasetNames"]
                self.dataset_names = responce_dict['gmqlJobStatusXML']["datasetNames"].split(',')
                print self.dataset_names
                return True
            else:
                if self.no_file_counter < NO_FILE_TRIES:
                    print self.no_file_counter
                    self.no_file_counter += 1
                    return False
                else:
                    raise RuntimeError("No result %s." % (self.query_id))

        elif status in ["ERROR", "TRANSLATION FAILED", "EXECUTION FAILED"]:
            if status == "EXECUTION FAILED" and "No result" in responce_dict['gmqlJobStatusXML']["message"]:
                #raise RuntimeError("No result %s.\nFull trace: %s" % (self.query_id, responce_dict['gmqlJobStatusXML']["message"]))
                return True
            elif status == "EXECUTION FAILED" and "Please Check Your Select Statements..." in responce_dict['gmqlJobStatusXML']["message"]:
                return True
            else:
                raise RuntimeError("Cannot perform GMQL query %s.\nFull trace: %s" % (
                self.query_id, responce_dict['gmqlJobStatusXML']["message"]))

        elif status in ["PENDING", "TRANSLATING", "TRANSLATED", "RUNNING"]:
            return False
        # unknown status
        return True


    def wait_finished(self, timeout_sec=600):
        timeout_sec = max(settings.CELERYD_TASK_SOFT_TIME_LIMIT, timeout_sec)
        timeout = min(timeout_sec, STEP_TIMEOUT)
        steps = timeout_sec / timeout
        for i in range(0, steps):
            if self.is_finished():
                return
            time.sleep(timeout)
        raise RuntimeError("Timeout of execution exceeded %s.\nFull trace: %s" % (self.query_id, timeout_sec))

    @staticmethod
    def is_gmql_availalbe():
        url = _listall_dataset_address()
        r = requests.get(url, auth=settings.AUTH, proxies=_get_proxies())
        if r.ok:
            return True
        if r.status_code == requests.codes.unauthorized:
            raise RuntimeError("No login-pass pair %s." % (settings.AUTH))
        if r.status_code == requests.codes.not_found:
            raise RuntimeError("Server is not available.")

        r.raise_for_status()

        return False
