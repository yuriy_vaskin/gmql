from collections import defaultdict
from django.conf import settings
import os

__author__ = 'yvaskin'


def _default_parse(f):
    return {}


def _encode_parse(f):
    if not os.path.isfile(f):
        return {}
    terms = defaultdict(list)
    term = ''
    val = ''
    with open(f, "r") as meta:
        for line in meta:
            if not line.strip(): continue

            if not line.startswith('\t'):
                term = line.strip()
                val = ''
            else:
                val = line.strip()
            if term and val:
                terms[term].append(val)

    return terms


class SpecialMeta:
    special_metas = []

    def __init__(self, name, file_path, file_name, genome=None, parse=None):
        if not parse: parse = _default_parse
        self.file_name = file_name
        self.name = name
        self.terms = parse(os.path.join(file_path, file_name))
        self.genome = genome
        SpecialMeta.special_metas.append(self)


    @staticmethod
    def guess_metas(query):
        res = []
        for sm in SpecialMeta.special_metas:
            if not sm.genome or sm.genome.lower() in query.lower():
                res.append(sm)
        return res


    @staticmethod
    def has_special_value(term):
        return any(meta.has_term(term) for meta in SpecialMeta.special_metas)


    @staticmethod
    def special_values(term):
        res = []
        for meta in SpecialMeta.special_metas:
            res = meta.values(term)
            if res: break
        return res


    def has_term(self, term):
        return term in self.terms


    def values(self, term):
        if self.has_term(term):
            return self.terms[term]
        return []


    def keys(self):
        return self.terms.keys()


encode_meta = SpecialMeta("ENCODE_BROADPEAK_HG19", os.path.join(settings.MEDIA_ROOT, 'dicts'), 'encode_hg19.txt', "hg19", _encode_parse)
encode_meta_mm9 = SpecialMeta("ENCODE_BROADPEAK_MM9", os.path.join(settings.MEDIA_ROOT, 'dicts'), 'encode_mm9.txt', "mm9", _encode_parse)
epigenome_meta_hg19 = SpecialMeta("EPIGENOME_BROADPEAK_HG19", os.path.join(settings.MEDIA_ROOT, 'dicts'), 'epigenome_hg19.txt', "hg19", _encode_parse)