"""
Django settings for gmql_service project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
from datetime import timedelta
import os
from django.core.urlresolvers import reverse
import djcelery
from requests.auth import HTTPBasicAuth

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ')*g(45z%hfa(kx&d9nkpm&-ua+@u$e08_#6cpdn@&+ep!1yqxw'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['127.0.0.1', 'localhost', 'blade-b1-p1.iit.ieo.eu']
SITE_URL = 'http://localhost:8000'


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'gmql',
    'djcelery',
    'kombu.transport.django',
    'bootstrapform',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.google',
    'django_cleanup',
    'selectable',
    'rest_framework',
    'pybb',
    'codemirror',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'pybb.middleware.PybbMiddleware',
)

SITE_ID = 3

ROOT_URLCONF = 'gmql_service.urls'

WSGI_APPLICATION = 'gmql_service.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }

}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_ROOT = '/home/hadoop/bioinfo_service/gmql/static/'
STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

AUTHENTICATION_BACKENDS = (
     # Needed to login by username in Django admin, regardless of `allauth`
    "django.contrib.auth.backends.ModelBackend",

    # `allauth` specific authentication methods, such as login by e-mail
    "allauth.account.auth_backends.AuthenticationBackend",
)

TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

TEMPLATE_CONTEXT_PROCESSORS = (
    #'django.core.context_processors.auth',
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.contrib.messages.context_processors.messages",

    'django.core.context_processors.request',
    'allauth.account.context_processors.account',
    'allauth.socialaccount.context_processors.socialaccount',
    'pybb.context_processors.processor',

)


djcelery.setup_loader()


# auth and allauth settings
SOCIALACCOUNT_QUERY_EMAIL = True
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_VERIFICATION = 'none'
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_PASSWORD_MIN_LENGTH = 4
SOCIALACCOUNT_PROVIDERS = {
    'facebook': {
        'SCOPE': ['email'],
        'METHOD': 'js_sdk',  # instead of 'oauth2'
    },

     'google': {
         'SCOPE': ['profile', 'email'],
         'AUTH_PARAMS': { 'access_type': 'online' }
     }

}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

#gmlq specific settings
DEFAULT_FORMAT = "bed"

MAX_UPLOAD_FILES = 10
MAX_UPLOAD_FILE_SIZE = 100 * 1024 * 1024 #bytes
MAX_TASK_STORAGE_TIME_DAYS = 1
CELERYD_TASK_SOFT_TIME_LIMIT = 120*60 #seconds
MAX_PROCESS_FILE_SIZE = 30 * 1024 * 1024 #bytes


CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
BROKER_BACKEND = "djkombu.transport.DatabaseTransport"
BROKER_URL = 'django://'
CELERYBEAT_SCHEDULE = {
    'clean_task': {
        'task': 'gmql.tasks.clean_task',
        'schedule': timedelta(days=MAX_TASK_STORAGE_TIME_DAYS),
    },
}


BASE_ADDRESS = 'http://blade-b1-p1.iit.ieo.eu:8082/gmql-services/rest/'
#BASE_ADDRESS = 'http://blade-b1-p1.iit.ieo.eu:8082/gmql-services-webapp1.6.10/rest/'
RUN_MODE = 'local'
AUTH = HTTPBasicAuth('tomcat', 'tomcat')

USE_HTTP_PROXY = True

ACCOUNT_ADAPTER = 'gmql.views.AccountAdapter'
DEFAULT_FILE_STORAGE = 'gmql.views.FileStorage'

PYBB_ENABLE_ANONYMOUS_POST=True
PYBB_DEFAULT_AUTOSUBSCRIBE=False
PYBB_DISABLE_SUBSCRIPTIONS=True
PYBB_DISABLE_NOTIFICATIONS=True


CODEMIRROR_PATH= 'gmql/codemirror'


try:
    from production_settings import *
except ImportError as e:
    pass
