from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.core.urlresolvers import reverse_lazy
from django.views import defaults
import gmql
from gmql.rest import QueryList, QueryDetails, TaskList, TaskDetails, gmql_api


urlpatterns = patterns('',
    url(r'^', include('gmql.urls', namespace='gmql')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^accounts/', include('allauth.urls')),


    # prevent the extra are-you-sure-you-want-to-logout step on logout
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': reverse_lazy('gmql:index')}),

    url(r'^accounts/profile/', gmql.views.profile, name='profile'),

    # block pages
    url(r"^inactive/$", defaults.page_not_found, name="account_inactive"),
    url(r"^email/$", defaults.page_not_found, name="account_email"),
    url(r"^confirm-email/$", defaults.page_not_found,
        name="account_email_verification_sent"),
    url(r"^confirm-email/(?P<key>\w+)/$", defaults.page_not_found,
        name="account_confirm_email"),
    # Handle old redirects
    url(r"^confirm_email/(?P<key>\w+)/$",
        defaults.page_not_found),

    (r'^selectable/', include('selectable.urls')),

    url(r'^rest/$', gmql_api, name='rest'),
    url(r'^rest/queries/$', QueryList.as_view(), name='query-list'),
    url(r'^rest/queries/(?P<pk>[0-9]+)/$', QueryDetails.as_view(), name='query-detail'),
    url(r'^rest/tasks/$', TaskList.as_view(), name='task-list'),
    url(r'^rest/tasks/(?P<pk>[0-9]+)/$', TaskDetails.as_view(), name='task-detail'),
    url(r'^rest/api-auth/', include('rest_framework.urls', namespace='rest_framework')),


    url(r'^forum/', include('pybb.urls', namespace='pybb')),
)
