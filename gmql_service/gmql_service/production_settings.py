SECRET_KEY = ')*g(45z%hfa(kx&d9nkpm&-ua+@u$e08_#6cpdn@&+ep!1yqxw'

DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['cru.genomics.iit.it/GMQL', 'cru.genomics.iit.it', 'http://cru.genomics.iit.it/GMQL', 'localhost', 'blade-b1-p1.iit.ieo.eu', '127.0.0.1']



SITE_ID = 4
STATIC_URL = '/GMQL/static/' 
STATIC_ROOT = '/home/hadoop/bioinfo_service/gmql/static/'
SITE_URL = 'http://cru.genomics.iit.it/GMQL'
FORCE_SCRIPT_NAME = '/GMQL'
LOGIN_URL = '/GMQL/accounts/login/'
LOGOUT_REDIRECT_URL = '/GMQL/'
USE_X_FORWARDED_HOST = True
