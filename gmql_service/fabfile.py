from fabric.api import lcd, local


def prepare_deployment(branch_name):
    local('python manage.py test gmql_service')
    local('git add -p && git commit')


def deploy():
    with lcd('/Users/yvaskin/Documents/project/bioinfo_service/'):
        # With git...
        local('git pull /Users/yvaskin/Documents/project/bioinfo_service/')

        # With both
        local('python manage.py migrate gmql_service')
        local('python manage.py test gmql_service')
        # local('/my/command/to/restart/webserver')