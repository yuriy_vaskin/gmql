from fabric.api import *
from contextlib import contextmanager as _contextmanager

# the user to use for the remote commands
env.user = 'hadoop'
# the servers where the commands are executed
env.hosts = ['blade-b1-p1.iit.ieo.eu']

env.directory = '/home/hadoop/bioinfo_service/gmql'
env.activate = 'source /home/hadoop/tools/python-packages/bioinfo_service/bin/activate'

@_contextmanager
def virtualenv():
    with cd(env.directory):
        with prefix(env.activate):
            yield

def deploy():
    with virtualenv():
        run('git pull')
        run('pip install -r requirements.txt')
        run('python gmql_service/manage.py makemigrations')
        run('python gmql_service/manage.py migrate')
        run('python gmql_service/manage.py collectstatic')
