#!/bin/bash
# Set the directory path for java jdk 
# If it is not otherwise set, then the default will be /usr/lib/jvm/java-7-oracle
export JAVA_HOME=/usr/lib/jvm/java-7-oracle


# Setting Hadoop home directory, if there is no hadoop installed then do not set this variable
# If it is not otherwise set, then the default will be /usr/local/hadoop
# If you are using Hadoop, Make sure to set GMQL_EXEC to MAPREDUCE mode.
export HADOOP_HOME=/usr/local/hadoop

export HADOOP_COMMON_HOME=$HADOOP_HOME
export HADOOP_COMMON_LIB_NATIVE_DIR=${HADOOP_COMMON_HOME}/lib/native
export HADOOP_CONF_DIR=$HADOOP_HOME/conf


# Setting Pig Apache home directory, This is where Pig Apache will be installed.
# If it is not otherwise set, then the default will be ~/pig
export PIG_HOME=~/pig

export PIG_CONF_DIR=$PIG_HOME/conf
export PIG_CLASSPATH=$PIG_HOME/pig-0.13.0-h1.jar:$HADOOP_COMMON_LIB_NATIVE_DIR/lib/*:$PIG_CLASSPATH:$HADOOP_CONF_DIR:$PIG_CONF_DIR


# Set the GMQL Home. It is the location where all the local data of the GMQL repository will be saved.
# All control and configuration data will be stored in this directory.
# If it is not otherwise set, then the default will be /home/gmql_repository
export GMQL_HOME=~/gmql_repository

# Set the GMQL_DFS_HOME. It is the home directory on hadoop distributed file system (HDFS) that will be the GMQL home on HDFS. 
# If it is not otherwise set, then the default will be /user/.
export GMQL_DFS_HOME=/user/

# Set the GMQL_EXE=[LOCAL|MAPREDUCE]. It is the execution mode for GMQL,
# LOCAL when there is no Hadoop on the system. While MAPREDUCE is used when there is Hadoop installed.
# If it is not otherwise set, then the default will be LOCAL.
export GMQL_EXEC=LOCAL


# Adding the variables to the system path.
export PATH=$PATH:$JAVA_HOME/bin:$HADOOP_HOME/bin:$PIG_HOME/bin:$GMQL_HOME/bin
