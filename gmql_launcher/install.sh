#!/bin/bash
# author: Abdulrahman Kaitoua
# PhD candidate at Politecnico Di Milano, Department of Electrical, Information, and Bioinformatic Engineering. 
# email: abdulrahman dot kaitoua at polimi dot it


function clean_GMQL
{
	echo "          GMQL Package is removed from your system. "
	echo "          Do not forget to delete the Global variables from your { .bashrc or .bash_profile } file."

 	rm -r $GMQL_HOME
#	rm /etc/profile.d/GMQL-env.sh
 	rm -r $PIG_HOME
# 	rm -r /usr/local/GMQL
}

check_racket() {
    if hash racket 2>/dev/null; then
        echo " INFO: racket is already installed.."
    else
        echo " WARN: { racket } not installed use { sudo apt-get install racket } to install it in ubuntu"
    fi
}

function install_GMQL
{
echo " Your chose to install GMQL...."
 	if [ "$GMQL_HOME" = '' ]; then
 		echo " ERROR: GMQL_HOME is not set .. Please set the Environment variables and copy them to .bash_profile"
 	else
 		echo " INFO: GMQL_Home is : $GMQL_HOME"
	
 		if [ -e $GMQL_HOME ]; then
 			echo " WARN: The GMQL HOME Directory already exists .."
 	
 		else  
 			mkdir $GMQL_HOME 
 			if [ -e $GMQL_HOME ]; then 
 				echo " INFO: Creating the Repository Directory ...................... DONE"
 				echo " INFO: Copy Bin files ......................................... DONE"
 				mkdir $GMQL_HOME/bin
 				cp -R ./bin/* $GMQL_HOME/bin/
 				echo " INFO: Copy configuration files ............................... DONE"
 				mkdir $GMQL_HOME/conf
 				cp -R ./conf/* $GMQL_HOME/conf/
 				echo " INFO: Copy Libraries {Lucene, GMQL, Orchestrator, Pig} jars .. DONE"
 				mkdir $GMQL_HOME/utils
 				cp -R ./utils/* $GMQL_HOME/utils/
				mkdir $GMQL_HOME/GMQL
 				#mkdir $GMQL_HOME/utils/lib
 				mkdir $GMQL_HOME/tmp
 				mkdir $GMQL_HOME/tmp/$USER
				mkdir $GMQL_HOME/data
 			else
 				echo " The Repository Dir {$GMQL_HOME} can not be created.. Might be permission problem."
 				exit 0	
 			fi
 		fi
 	fi	
 	
 	echo ""
 	PIG_VER="0.13.0"
 	if [ -e ./pig-$PIG_VER.tar.gz ]; then
 		if [ -e pig-$PIG_VER ]; then
 			echo " INFO: The pig tar already untared .. "
 		else 
 			echo  " INFO: Untaring the PIG folder .."
 			tar -xzf ./pig-$PIG_VER.tar.gz
 		fi
 	else
 		echo " ERROR: Apache PIG tar file is not found .."
 		exit 0
 	fi
 
 	if [ "$PIG_HOME" != "" ]; then
 		echo " INFO: Copying Apache PIG package to $PIG_HOME"
 		mv ./pig-$PIG_VER $PIG_HOME
 	else
 		echo " ERROR: The PIG_HOME Variable is not set."
 		exit 0
 	fi
 
 	if [ -e $PIG_HOME ]; then
 		echo " INFO: DONE" 
 	else
 		echo " ERROR: Copying did not work .." 
 		exit 0
 	fi
 	
 	echo ""
 
 	GMQL_TRANSLATOR=$GMQL_HOME"/GMQL/gmqlc"
 
 	cp -R ./GMQL/* $GMQL_HOME/GMQL/
 
 	if [ -e $GMQL_TRANSLATOR ]; then
 		echo " INFO: GMQL Translator directory are copied.. $GMQL_TRANSLATOR"  
 		chmod -R 755 $GMQL_TRANSLATOR
 	else	
 		echo " ERROR: Translator directory could not been copied .."
 		exit 0
	fi
 
 
 	echo ""
 
 	$GMQL_HOME/bin/repositoryManagerV1 registeruser public   
 	echo " INFO: User { public } is registered to the repository. "

 	chmod -R 750 $GMQL_HOME
 	
	#cp ./conf/GMQL-env.sh /etc/profile.d/ 
 	#echo " INFO: The global variables are registered in /etc/profile.d/GMQL-env.sh"
	#echo " INFO: If you want to change any env variable then modify /etc/profile.d/GMQL-env.sh"

	echo " INFO: Checking racket instalation .. "
	check_racket	
}

choice=4

function usage
{
	echo "    - Choose  1 for installing GMQL .."
	echo "    - Choose  2 for removing GMQL .."
	echo "    - Choose  3 for exit .."
	echo ""
}

function run
{
        case $choice in
                1 ) 
                        echo ""
			echo "    INFO: Your chose to install GMQL .."
                        echo ""
                        install_GMQL;
                        ;;
                2 ) 
                        echo ""
			echo "    INFO: Your chose to remove GMQL .."
                        clean_GMQL;
                        ;;
                3 ) 
                        echo ""
			echo "    INFO: Your chose to exit GMQL .."
                        echo ""
                        exit 0
                        ;;
                * ) 
                        echo ""
			echo "    INFO: Your selection is not permitted, try again .."
                        echo ""
                        usage;
                        echo -n "    -> Enter your choice : "
                        echo ""
                        read choice
			run;
                        ;;
        esac
}

echo ""
echo "  ........................................................"
echo "  ........................................................"
echo "  ......Installing GMQL Package to your System............"
echo "  ........................................................"
echo "  ........................................................"
echo ""

#if [ $(id -u) != "0" ]; then
#        echo " WARN: The program will not proceed with out SUDO user.."
#        echo " WARN: Use the following command ( sudo ./install ) "
#        exit 0
#fi

source ./conf/GMQL-env.sh
usage;

echo -n "    -> Enter your choice : "
read choice

run;
echo ""

