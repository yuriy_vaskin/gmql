#lang racket

(provide init-optimizer
	 is-indexed?
	 loading-code
	 emit-lucene-load-stm)

(require xml)
(require racket/match)
(require (only-in lazy [or lazy-or]))

;in the list there are triplets (name dir index)
(define list-indexed-variables (list))

(define indexed-identifier-factory 'null)

;list-indexed-variables accessor
(define (get-dir variable)
  (second (car (filter (lambda (x) (equal? (first x) variable)) list-indexed-variables))))
(define (get-index variable)
  (third (car (filter (lambda (x) (equal? (first x) variable)) list-indexed-variables))))

(define (make-indexed-identifier-factory)
    (let ((count 0))
      (lambda ()
	(begin 
	  (set! count (+ 1 count))
	  (string-append "INDEXED" (number->string count))))))


;remove from a list all the strings that are made of only [:space:] characters
(define (trim-list x)
  (if (null? x)
      '()
      (match (car x)
        [(regexp #px"[[:space:]]+") (trim-list (cdr x))]
        [_ (cons (car x) (trim-list (cdr x)))])))


(define (attributes->tuple a)
  (list
   (cadar (filter (lambda (x) (equal? (car x) 'name)) a))
   (cadar (filter (lambda (x) (equal? (car x) 'dir)) a))
   (cadar (filter (lambda (x) (equal? (car x) 'index)) a))))

(define (xml->var x)
  (match x
    [(list 'indexedList attributes r ...) (map xml->var (trim-list r))]
    [(list 'variable attributes r ...) (attributes->tuple attributes)]
    ))

(define (init-optimizer input-directories-list)
  (display input-directories-list))
;(define (init-optimizer path-to-indexed-list-xml-file)
;  (let
;    ([indexed-variables-xexpr (xml->xexpr (document-element (read-xml (open-input-file path-to-indexed-list-xml-file))))])
;    (begin
;      (set! indexed-identifier-factory (make-indexed-identifier-factory))
;      (set! list-indexed-variables (xml->var indexed-variables-xexpr)))))

;this returns a cons (identifier . load-preamble)
(define (loading-code variable condition)
  (let ((new-identifier (indexed-identifier-factory)))
    (cons
      new-identifier
      (string-append
	new-identifier "\t"
	condition "\t"
	(get-index variable) "\t"
	(get-dir variable) ))))

(define (is-indexed? var)
    (apply lazy-or (map (lambda (x) (equal? var (first x))) list-indexed-variables)))

(define (emit-lucene-load-stm identifier)
  (string-append "load '$" identifier "' AS (id:long, region:(chr:int, left:int, right:int, strand:chararray), value:())"))

